#ifndef _BUILDVERSION_H_
#define _BUILDVERSION_H_

#define _PRODUCTVERSION			1,0,0,14
#define _PRODUCTVERSION_STR 	"1.0.0.14"

#define _COMPANYNAME_STR_EN		"http://tnlab.ru"
#define _COMPANYNAME_STR_RU		"http://tnlab.ru"

#define _LEGALCOPYRIGHT_STR_EN	"� 2007-2016 TNLab"
#define _LEGALCOPYRIGHT_STR_RU	"� 2007-2016 TNLab"

#define _PRODUCTNAME_STR_EN		"TNLab"
#define _PRODUCTNAME_STR_RU		"TNLab"


#endif
