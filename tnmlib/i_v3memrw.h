#pragma once

BOOL
_TND_ReadRecordsTableSnapshot
(
  LPTNMLIB_DEVICE_INSTANCE                pDI,
  LPTNMLIB_DEVICE_RECORDS_TABLE_SNAPSHOT  pRTS
);


BOOL
_TND_WriteRecordsTableSnapshot
(
  LPTNMLIB_DEVICE_INSTANCE                pDI,
  LPTNMLIB_DEVICE_RECORDS_TABLE_SNAPSHOT  pRTS
);
