#include "stdafx.h"

#include "tnmdefs.h"
#include "i_devman.h"

#include "i_ovlio.h"

BOOL static
_SubmitIOBuffer(
  DWORD dwIOFlags,
  HANDLE hFileHandle, 
  LPTNMLIB_EIO_BUFFER pIOB
  )
{
  BOOL fSuccess;

  _ASSERTE(pIOB->dwBufferSize >= pIOB->dwNumberOfBytesToTransfer);

  if (TNMLIB_EIO_IsRead(dwIOFlags))
  {
    fSuccess = ReadFile(
      hFileHandle, 
      pIOB->lpBuffer, 
      pIOB->dwNumberOfBytesToTransfer, 
      &pIOB->dwNumberOfBytesTransferred, 
      &pIOB->OVL);
  }
  else
  {
    fSuccess = WriteFile(
      hFileHandle, 
      pIOB->lpBuffer, 
      pIOB->dwNumberOfBytesToTransfer, 
      &pIOB->dwNumberOfBytesTransferred, 
      &pIOB->OVL);
  }

  if (fSuccess) 
  {
    pIOB->dwStatus = ERROR_SUCCESS;
  }
  else 
  {
    pIOB->dwStatus = GetLastError();
    if (pIOB->dwStatus == ERROR_IO_PENDING) 
      fSuccess = TRUE;
  }

  return fSuccess;
}

DWORD static
_WaitIOBufferForCompletion(
  HANDLE hFileHandle,
  LPTNMLIB_EIO_BUFFER pIOB,
  DWORD dwTimeoutMilliseconds, HANDLE hBreakEvent, DWORD dwBreakCode
  )
{
  BOOL  fSuccess = FALSE;
  DWORD  dwWaitResult;

  DWORD  dwStatus;

  DWORD  dwEventCount;
  HANDLE  lphEvents[2]; 

  do {
    dwStatus = pIOB->dwStatus;

    if (dwStatus != ERROR_IO_PENDING)
      break;

    lphEvents[0] = pIOB->OVL.hEvent;
    lphEvents[1] = hBreakEvent;

    dwEventCount = (hBreakEvent == NULL) ? 1 : 2;
    dwWaitResult = WaitForMultipleObjects(dwEventCount, lphEvents, 
      FALSE, dwTimeoutMilliseconds);

    switch (dwWaitResult)
    {
    case WAIT_TIMEOUT:
      dwStatus = WAIT_TIMEOUT;
      break;

    case WAIT_OBJECT_0:
      fSuccess = GetOverlappedResult(
        hFileHandle, 
        &pIOB->OVL, 
        &pIOB->dwNumberOfBytesTransferred, 
        FALSE);

      if (fSuccess)
        pIOB->dwStatus = ERROR_SUCCESS;
      else
        pIOB->dwStatus = GetLastError();
      break;

    case (WAIT_OBJECT_0 + 1):
      break;
    }

  } while (FALSE);

  return dwWaitResult;
}

BOOL
_InitializeIOBuffer(LPTNMLIB_EIO_BUFFER pIOB)
{
  BOOL fSuccess = TRUE;

  _ASSERTE(_CrtIsValidPointer(pIOB, sizeof(TNMLIB_EIO_BUFFER), TRUE));

  do 
  {
    ZeroMemory(&pIOB->OVL, sizeof(OVERLAPPED));
    pIOB->OVL.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

    if (pIOB->OVL.hEvent == NULL)
    {
      fSuccess = FALSE;
      break;
    }
#ifdef _DEBUG
    pIOB->lpBuffer = NULL;
    pIOB->dwBufferSize = 0;

    pIOB->dwNumberOfBytesToTransfer = 0;
    pIOB->dwNumberOfBytesTransferred = 0;
    pIOB->dwStatus = ERROR_SUCCESS;
#endif

  } while (FALSE);

  return fSuccess;
}

BOOL
_DeleteIOBuffer(LPTNMLIB_EIO_BUFFER pIOB)
{
  BOOL fSuccess = TRUE;

  _ASSERTE(_CrtIsValidPointer(pIOB, sizeof(TNMLIB_EIO_BUFFER), TRUE));

  if (pIOB->OVL.hEvent != NULL)
    fSuccess &= CloseHandle(pIOB->OVL.hEvent);

#ifdef _DEBUG
  pIOB->lpBuffer = NULL;
  pIOB->dwBufferSize = 0;

  pIOB->dwNumberOfBytesToTransfer = 0;
  pIOB->dwNumberOfBytesTransferred = 0;

  pIOB->dwStatus = ERROR_SUCCESS;
  ZeroMemory(&pIOB->OVL, sizeof(OVERLAPPED));
#endif

  return fSuccess;
}


LPTNMLIB_EIO_BUFFERS_POOL
_EIO_CreateIOBuffersPool(
  DWORD dwFlags,
  DWORD dwNumberOfBuffers,
  DWORD dwBufferSize,
  DWORD dwAlignment
  )
{
  LPTNMLIB_EIO_BUFFERS_POOL pBP = NULL;
  LPVOID  pBufMemory = NULL;

  BOOL fSuccess;

  _ASSERTE(dwNumberOfBuffers != 0);
  do 
  {
    size_t i, nInitializedBuffers;
    DWORD dwAlignedSize;
    LPBYTE p;

    size_t iSize;
    iSize = 
      sizeof(TNMLIB_EIO_BUFFERS_POOL) + 
      sizeof(TNMLIB_EIO_BUFFER) * (dwNumberOfBuffers - 1);

    pBP = (LPTNMLIB_EIO_BUFFERS_POOL)malloc(iSize);
    if (pBP == NULL)
    {
      fSuccess = FALSE;
      break;
    }

    pBP->dwFlags = dwFlags;
    pBP->dwNumberOfBuffers = dwNumberOfBuffers;

    if (dwFlags & TNMLIB_EIO_FLAG_ALLOCATE_BUFFERS)
    {

      dwAlignedSize = dwBufferSize;
      
      if (dwAlignment != 0)
      {
        DWORD dwAlignTail = dwBufferSize % dwAlignment;
        if (dwAlignTail != 0)
          dwAlignedSize += dwAlignment - dwAlignTail;
      }

      pBufMemory = (LPBYTE)malloc(dwNumberOfBuffers * dwAlignedSize);
      if (pBufMemory == NULL)
        break;
    }
    else
    {
      dwAlignedSize = 0;
    }

    fSuccess = TRUE;
    nInitializedBuffers = 0;
    p = (LPBYTE)pBufMemory;

    for (i = 0; i < dwNumberOfBuffers; i ++)
    {
      fSuccess = _InitializeIOBuffer(&pBP->pIOB[i]);
      if (!fSuccess)
        break;

      pBP->pIOB[i].dwBufferSize = dwBufferSize;
      pBP->pIOB[i].lpBuffer     = p;

      p += dwAlignedSize;
      nInitializedBuffers ++;
    }

    if (!fSuccess)
    {
      DWORD dwLastError = GetLastError();

      for (i = 0; i < nInitializedBuffers; i ++)
        _DeleteIOBuffer(&pBP->pIOB[i]);

      if (pBufMemory != NULL)
        free(pBufMemory);

      if (pBP != NULL)
        free(pBP);

      pBP = NULL;

      SetLastError(dwLastError);
    }

  } while (FALSE);

  return pBP;
}

BOOL
_EIO_CancelIOBuffersPool(
  HANDLE hFileHandle,
  LPTNMLIB_EIO_BUFFERS_POOL pBP,
  DWORD dwTimeoutMilliseconds, HANDLE hBreakEvent, DWORD dwBreakCode
  )
{
  BOOL fSuccess = TRUE;
  size_t i;

  fSuccess = CancelIo(hFileHandle);

  for (i = 0; i < pBP->dwNumberOfBuffers; i ++)
  {
    DWORD dwWaitStatus;

    if (pBP->pIOB[i].dwStatus == ERROR_IO_PENDING)
    {
      dwWaitStatus = _WaitIOBufferForCompletion(
              hFileHandle, 
              &pBP->pIOB[i], 
              dwTimeoutMilliseconds, hBreakEvent, dwBreakCode
              );
      if (hBreakEvent != NULL && dwWaitStatus == dwBreakCode)
        break;

      if (dwWaitStatus == WAIT_ABANDONED_0 + 1)
        _ASSERTE(FALSE);

      if (dwWaitStatus == WAIT_ABANDONED_0 + 0)
        _ASSERTE(FALSE);
    }
  }

  return fSuccess;
}

BOOL
_EIO_DeleteIOBuffersPool(
  LPTNMLIB_EIO_BUFFERS_POOL pBP
  )
{
  BOOL fSuccess = TRUE;
  size_t i;

  if (pBP->dwFlags & TNMLIB_EIO_FLAG_ALLOCATE_BUFFERS)
    free(pBP->pIOB[0].lpBuffer);

  for (i = 0; i < pBP->dwNumberOfBuffers; i ++)
    fSuccess &= _DeleteIOBuffer(&pBP->pIOB[i]);

  free(pBP);

  return fSuccess;
}

BOOL
_EIO_IOQuery(
  DWORD  dwIOFlags,
  HANDLE  hFileHandle,
  LPTNMLIB_EIO_BUFFERS_POOL pBP,
  DWORD64 ldwNumberOfBytesToTransfer,
  DWORD dwTimeoutMilliseconds, HANDLE hBreakEvent, DWORD dwBreakCode,
  LPTNMLIB_EIO_BUFFERPROC lpDataProc, LPVOID lpParam
  )
{
  BOOL    fSuccess = FALSE;

  LPTNMLIB_EIO_BUFFER pEndOfIOB;
  LPTNMLIB_EIO_BUFFER  pIOB;
  DWORD        dwSubmittedBuffers;

  pIOB        = pBP->pIOB;
  pEndOfIOB      = pBP->pIOB + pBP->dwNumberOfBuffers;
  dwSubmittedBuffers  = 0;

  do 
  {
    // Submit all buffers if there is enough
    // bytes to transfer
    while (pIOB != pEndOfIOB && ldwNumberOfBytesToTransfer)
    {
      if (lpDataProc != NULL)
      {
        fSuccess = lpDataProc(
          dwIOFlags | TNMLIB_EIO_BUFFERSUBMIT, 
          ERROR_SUCCESS,
          pIOB, lpParam);

        if (!fSuccess)
          break;
      }

      _ASSERTE(pIOB->lpBuffer != NULL);
      _ASSERTE(pIOB->dwBufferSize != 0);

      pIOB->dwNumberOfBytesToTransfer = (DWORD)
        min(pIOB->dwBufferSize, ldwNumberOfBytesToTransfer);

      fSuccess = _SubmitIOBuffer(dwIOFlags, hFileHandle, pIOB);
      if (!fSuccess)
        break;

      ++ dwSubmittedBuffers;

      if (ldwNumberOfBytesToTransfer != -1)
        ldwNumberOfBytesToTransfer -= pIOB->dwNumberOfBytesToTransfer;

      ++ pIOB;
    }

    if (!fSuccess)
      break;

    pIOB = pBP->pIOB;

    while (dwSubmittedBuffers)
    {
      DWORD dwWaitResult;
      DWORD dwStatus;

      dwWaitResult = _WaitIOBufferForCompletion(
        hFileHandle, 
        pIOB, 
        dwTimeoutMilliseconds,
        hBreakEvent, dwBreakCode);

      _ASSERTE(pIOB->dwNumberOfBytesTransferred <= pIOB->dwNumberOfBytesToTransfer);

      fSuccess = (dwWaitResult == (WAIT_OBJECT_0 + 0) && pIOB->dwStatus == ERROR_SUCCESS);

      if (dwWaitResult == WAIT_TIMEOUT)
        dwStatus = WAIT_TIMEOUT;

      if (dwWaitResult == (WAIT_OBJECT_0 + 0))
        dwStatus = pIOB->dwStatus;

      if (dwWaitResult == (WAIT_OBJECT_0 + 1))
        dwStatus = dwBreakCode;

      if (lpDataProc != NULL)
      {
        fSuccess = lpDataProc(
          dwIOFlags | TNMLIB_EIO_BUFFERCOMPLETE, 
          dwStatus,
          pIOB, lpParam);
      }
      else
        SetLastError(dwStatus);

      if (!fSuccess)
        break;

      -- dwSubmittedBuffers;

      if (ldwNumberOfBytesToTransfer)
      {
        if (lpDataProc != NULL)
        {
          fSuccess = lpDataProc(
            dwIOFlags | TNMLIB_EIO_BUFFERSUBMIT, 
            ERROR_SUCCESS,
            pIOB, lpParam);

          if (!fSuccess)
            break;
        }

        pIOB->dwNumberOfBytesToTransfer = (DWORD)
          min(pIOB->dwBufferSize, ldwNumberOfBytesToTransfer);

        fSuccess = _SubmitIOBuffer(dwIOFlags, hFileHandle, pIOB);
        if (!fSuccess)
          break;

        ++ dwSubmittedBuffers;

        if (ldwNumberOfBytesToTransfer != - 1)
          ldwNumberOfBytesToTransfer -= pIOB->dwNumberOfBytesToTransfer;
      }

      ++ pIOB;

      if (pIOB == pEndOfIOB)
        pIOB = pBP->pIOB;
    }

  } while (FALSE);

  return fSuccess;
}

BOOL
_EIO_IOQuerySync(
  DWORD  dwIOFlags,
  HANDLE  hFileHandle,
  LPVOID  lpBuffer, DWORD dwNumberOfBytesToTransfer, 
  DWORD  dwTimeoutMilliseconds, HANDLE hBreakEvent, DWORD dwBreakCode,

  LPDWORD lpdwNumberOfBytesTransferred
  )
{
  BOOL fSuccess = FALSE;

  TNMLIB_EIO_BUFFERS_POOL BP;

  DWORD      dwStatus;

  do 
  {
    if (lpdwNumberOfBytesTransferred != NULL)
      *lpdwNumberOfBytesTransferred = 0;

    BP.dwFlags = 0;
    BP.dwNumberOfBuffers = 1;

    fSuccess = _InitializeIOBuffer(&BP.pIOB[0]);
    if (!fSuccess)
      break;

    BP.pIOB[0].lpBuffer     = lpBuffer;
    BP.pIOB[0].dwBufferSize = dwNumberOfBytesToTransfer;

    fSuccess = _EIO_IOQuery(
      dwIOFlags,
      hFileHandle,
      &BP, 
      dwNumberOfBytesToTransfer,
      dwTimeoutMilliseconds, hBreakEvent, dwBreakCode,
      NULL, NULL
      );

    dwStatus = GetLastError();

    if (!fSuccess)
    {
      _EIO_CancelIOBuffersPool(
        hFileHandle, &BP, 
        INFINITE, NULL, 0);
    }        

    if (lpdwNumberOfBytesTransferred != NULL)
      *lpdwNumberOfBytesTransferred += BP.pIOB[0].dwNumberOfBytesTransferred;

    _DeleteIOBuffer(&BP.pIOB[0]);

    SetLastError(dwStatus);
  } while (FALSE);

  return fSuccess;
}
