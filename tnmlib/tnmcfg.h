#ifndef _tnmcfg_header_
#define _tnmcfg_header_

BOOL TNMLIB_API
TNMLIB_SetTimeout(
  TNMLIB_HANDLE hTND, 
  DWORD dwTimeout
  );

BOOL TNMLIB_API
TNMLIB_GetTimeout(
  TNMLIB_HANDLE hTND, 
  LPDWORD lpdwTimeout
  );


#endif // _tnmcfg_header_