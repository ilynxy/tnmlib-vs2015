#ifndef _tnmerrs_header_
#define _tnmerrs_header_

DWORD TNMLIB_API 
TNMLIB_GetErrorString(
  DWORD dwErrorCode, 
  BOOL fAllocate, 
  LPTSTR lpBuffer, 
  DWORD dwSize
  );

BOOL TNMLIB_API 
TNMLIB_FreeErrorString(
  LPTSTR lpBuffer
  );

#endif // _tnmerrs_header_
