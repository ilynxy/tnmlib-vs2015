#ifndef _tnmlib_i_header_
#define _tnmlib_i_header_

#ifdef __cplusplus 
extern "C" {
#endif

#include "tnmdefs.h"

#include "tnmmsg.h"

#include "tnmlib.h"
#include "tnmerrs.h"
#include "tnmlock.h"
#include "tnminfo.h"
#include "tnmrw.h"
#include "tnmrec.h"
#include "tnmcfg.h"

#ifdef __cplusplus 
}
#endif

#endif // _tnmlib_i_header_
