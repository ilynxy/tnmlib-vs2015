#ifndef _TNM_I_OVLIO_HEADER_
#define _TNM_I_OVLIO_HEADER_

#define TNMLIB_EIO_READREQUEST    0x00000000L
#define TNMLIB_EIO_WRITEREQUEST   0x00000001L

#define TNMLIB_EIO_REQUEST_MASK   0x00000001L

#define TNMLIB_EIO_BUFFERSUBMIT   0x00000000L
#define TNMLIB_EIO_BUFFERCOMPLETE 0x00000002L

#define TNMLIB_EIO_BUFFER_MASK    0x00000002L

#define TNMLIB_EIO_FLAG_ALLOCATE_BUFFERS  0x00000001L

#define TNMLIB_EIO_IsRead(x)    (((x) & TNMLIB_EIO_REQUEST_MASK) == TNMLIB_EIO_READREQUEST)
#define TNMLIB_EIO_IsWrite(x)   (((x) & TNMLIB_EIO_REQUEST_MASK) == TNMLIB_EIO_WRITEREQUEST)

#define TNMLIB_EIO_IsBufferQuery(x)     (((x) & TNMLIB_EIO_BUFFER_MASK) == TNMLIB_EIO_BUFFERSUBMIT)
#define TNMLIB_EIO_IsBufferComplete(x)  (((x) & TNMLIB_EIO_BUFFER_MASK) == TNMLIB_EIO_BUFFERCOMPLETE)

typedef struct _TNMLIB_EIO_BUFFER
{
  DWORD       dwBufferSize;
  LPVOID      lpBuffer;
  OVERLAPPED  OVL;

  DWORD       dwStatus;
  DWORD       dwNumberOfBytesToTransfer;
  DWORD       dwNumberOfBytesTransferred;

} TNMLIB_EIO_BUFFER, *LPTNMLIB_EIO_BUFFER;

typedef struct _TNMLIB_EIO_BUFFERS_POOL 
{
  DWORD        dwFlags;
  DWORD        dwNumberOfBuffers;
  TNMLIB_EIO_BUFFER  pIOB[1];

} TNMLIB_EIO_BUFFERS_POOL, *LPTNMLIB_EIO_BUFFERS_POOL;

typedef BOOL (CALLBACK *LPTNMLIB_EIO_BUFFERPROC)(
  DWORD        dwIOFlags,
  DWORD        dwStatus, 
  LPTNMLIB_EIO_BUFFER pIOB, 
  LPVOID        pParam
  );

LPTNMLIB_EIO_BUFFERS_POOL
_EIO_CreateIOBuffersPool(
  DWORD dwFlags,
  DWORD dwNumberOfBuffers,
  DWORD dwBufferSize,
  DWORD dwAlignment
  );

BOOL
_EIO_CancelIOBuffersPool(
  HANDLE hFileHandle,
  LPTNMLIB_EIO_BUFFERS_POOL pBP,
  DWORD dwTimeoutMilliseconds, HANDLE hBreakEvent, DWORD dwBreakCode
  );

BOOL
_EIO_DeleteIOBuffersPool(
  LPTNMLIB_EIO_BUFFERS_POOL pBP
  );

BOOL
_EIO_IOQuery(
  DWORD  dwIOFlags,
  HANDLE  hFileHandle,
  LPTNMLIB_EIO_BUFFERS_POOL pBP,
  DWORD64 ldwNumberOfBytesToTransfer,
  DWORD dwTimeoutMilliseconds, HANDLE hBreakEvent, DWORD dwBreakCode,
  LPTNMLIB_EIO_BUFFERPROC lpDataProc, LPVOID lpParam
  );

BOOL
_EIO_IOQuerySync(
  DWORD  dwIOFlags,
  HANDLE  hFileHandle,
  LPVOID  lpBuffer, DWORD dwNumberOfBytesToTransfer, 
  DWORD  dwTimeoutMilliseconds, HANDLE hBreakEvent, DWORD dwBreakCode,

  LPDWORD lpdwNumberOfBytesTransferred
  );


#endif // _TNM_I_OVLIO_HEADER_
