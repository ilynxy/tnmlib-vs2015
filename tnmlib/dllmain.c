#include "stdafx.h"

#include "i_debug.h"
#include "i_hminit.h"

HANDLE  g_hModule;

BOOL APIENTRY 
DllMain(
  HMODULE hModule, 
  DWORD  ul_reason_for_call, 
  LPVOID lpReserved
  )
{
  BOOL fSuccess = TRUE;

  UNREFERENCED_PARAMETER(lpReserved);

  switch (ul_reason_for_call)
  {
  case DLL_PROCESS_ATTACH:

    g_hModule = hModule;

    fSuccess = DisableThreadLibraryCalls(hModule);
    if (!fSuccess)
      break;

    _DbgInitialize();

    _HMInitialize();

    break;

  case DLL_PROCESS_DETACH:

    _HMFinalize();

    _DbgFinalize();

    break;

  case DLL_THREAD_ATTACH:
    break;

  case DLL_THREAD_DETACH:
    break;
  }

  return fSuccess;
}
