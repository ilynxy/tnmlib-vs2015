#pragma once

// #define _DEBUG_CALLS

#ifdef _DEBUG_CALLS

void
_DbgInitialize();

void
_DbgFinalize();

void
_DbgPrint(LPCTSTR lpszFormat, ...);

void
_DbgFunctionEntry(LPCSTR pszFunction);

void
_DbgFunctionLeave(LPCSTR pszFunction, void* lpParam);

void
_DbgFunctionOutputString(LPCTSTR pszString);

#define DBG_ENTRY     _DbgFunctionEntry(__FUNCTION__)
#define DBG_LEAVE(x)  _DbgFunctionLeave(__FUNCTION__, ((void *)(x)))

#else // _DEBUG

__inline static void
_DbgInitialize()
{
}

__inline static void
_DbgFinalize()
{
}

__inline static void
_DbgPrint(LPCTSTR lpszFormat, ...)
{
  UNREFERENCED_PARAMETER(lpszFormat);
}

__inline static void
_DbgFunctionOutputString(LPCTSTR pszString)
{
  UNREFERENCED_PARAMETER(pszString);
}

#define DBG_ENTRY
#define DBG_LEAVE(x)

#endif // _DEBUG_CALLS
