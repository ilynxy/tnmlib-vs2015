#ifndef _tnmdefs_header_
#define _tnmdefs_header_

#define TNMLIB_API __stdcall

typedef void* TNMLIB_HANDLE;
#define TNMLIB_INVALID_HANDLE  ((TNMLIB_HANDLE)0)

#define _TNMLIB_MAX_DEVICES    128

typedef BOOL (CALLBACK *LPTNMLIB_DATAPROCESSPROC)(LPVOID lpBuffer, DWORD dwBufferSize, LPVOID lpParam);
typedef BOOL (CALLBACK *LPTNMLIB_PROGRESSPROC)(LPVOID lpParam);

typedef BOOL (CALLBACK *LPTNMLIB_STAGEDREADPROC)(LPVOID lpStageBuffer, DWORD dwStageSize, LPVOID lpParam);
typedef BOOL (CALLBACK *LPTNMLIB_STAGEDWRITEPROC)(LPVOID* lpStageBuffer, DWORD dwStageSize, LPVOID lpParam);

#define TNMLIB_DEVICE_MEMTYPE_RAWFLASH    0
#define TNMLIB_DEVICE_MEMTYPE_FLASH       1
#define TNMLIB_DEVICE_MEMTYPE_TASKS       2
#define TNMLIB_DEVICE_MEMTYPE_BADBLOCKS   3
#define TNMLIB_DEVICE_MEMTYPE_RECORDS     4
#define TNMLIB_DEVICE_MEMTYPE_BOOT        5
#define TNMLIB_DEVICE_MEMTYPE_FAKETASK    6

#define TNMLIB_DEVICE_MEMTYPES_COUNT      7

#define TNMLIB_DEVICE_MEMORY_EXINFO_READ  0
#define TNMLIB_DEVICE_MEMORY_EXINFO_WRITE 1

#define TNMLIB_DEVICE_RECORDS_TABLE_ENTRIES     256
#define TNMLIB_DEVICE_RECORDS_TABLE_COPIES      4

#define TNMLIB_USE_NAMELESS_STRUCT_UNION

#ifdef TNMLIB_USE_NAMELESS_STRUCT_UNION
// Supress warning C4201: nonstandard extension used : nameless struct/union
#pragma warning(push)
#pragma warning(disable : 4201)
#endif

#include <PshPack1.h>

typedef struct _TNMLIB_DEVICE_DATE {

#ifdef TNMLIB_USE_NAMELESS_STRUCT_UNION
  union
  {
    struct
    {
#endif

      WORD  wYear;
      BYTE  bMonth;
      BYTE  bDay;

#ifdef TNMLIB_USE_NAMELESS_STRUCT_UNION
    };
    DWORD   dwUDate;
  };
#endif
} TNMLIB_DEVICE_DATE, *LPTNMLIB_DEVICE_DATE;

typedef struct _TNMLIB_DEVICE_TIME {
#ifdef TNMLIB_USE_NAMELESS_STRUCT_UNION
  union
  {
    struct
    {
#endif
      BYTE  bHour;
      BYTE  bMinute;
      BYTE  bSecond;
      BYTE  bMillisecond;
#ifdef TNMLIB_USE_NAMELESS_STRUCT_UNION
    };
    DWORD   dwUTime;
  };
#endif

} TNMLIB_DEVICE_TIME, *LPTNMLIB_DEVICE_TIME;


typedef struct _TNMLIB_DEVICE_VERSION_INFO 
{
  WORD  wTNVersion;
  WORD  wTNSerialNumber;
} TNMLIB_DEVICE_VERSION_INFO, *LPTNMLIB_DEVICE_VERSION_INFO;

typedef struct _TNMLIB_DEVICE_MEMORY_INFO {
  DWORD64 ldwTotalMainMemorySize;
  DWORD64 ldwInstalledMainMemorySize;
  DWORD64 ldwFreeMainMemorySize;
  DWORD   dwRecordsMemorySize;
  DWORD   dwTasksMemorySize;
  DWORD   dwBadBlocksMemorySize;
  DWORD   dwBlockSize;
  DWORD   dwNumberOfMainMemoryChips;
  DWORD   dwNumberOfRecords;
  DWORD   dwNumberOfTasks;
  DWORD   dwNumberOfBadBlocks;
  DWORD   dwNumberOfMemoryTypes;
  BYTE    Reserved[4];
} TNMLIB_DEVICE_MEMORY_INFO, *LPTNMLIB_DEVICE_MEMORY_INFO;

typedef struct _TNMLIB_DEVICE_RECORD_INFO {
  DWORD64             ldwSize;
  TNMLIB_DEVICE_DATE  rdDate;
  TNMLIB_DEVICE_TIME  rtStart;
  TNMLIB_DEVICE_TIME  rtStop;
  WORD                wTaskNumber;
  WORD                wFlyNumber;
  WORD                wDataUnitSerial;
  WORD                wDataUnitVersion;
  BYTE                Reserved[4];
} TNMLIB_DEVICE_RECORD_INFO, *LPTNMLIB_DEVICE_RECORD_INFO;

typedef struct _TNMLIB_DEVICE_RECORDS_TABLE_SNAPSHOT
{
  TNMLIB_DEVICE_RECORD_INFO RI[TNMLIB_DEVICE_RECORDS_TABLE_COPIES][TNMLIB_DEVICE_RECORDS_TABLE_ENTRIES];

} TNMLIB_DEVICE_RECORDS_TABLE_SNAPSHOT, *LPTNMLIB_DEVICE_RECORDS_TABLE_SNAPSHOT;

typedef struct _TNMLIB_RECORD_MODULE_INFO
{
  DWORD       dwModuleID;
#ifdef TNMLIB_USE_NAMELESS_STRUCT_UNION
  union
  {
    struct
    {
#endif
      WORD    wVersion;
      WORD    wSerialNumber;
#ifdef TNMLIB_USE_NAMELESS_STRUCT_UNION
    };
    DWORD     dwVersionSerial;
  };
#endif
  LONG32      iMIBAddress;
  DWORD       uiActive;
} TNMLIB_RECORD_MODULE_INFO, *LPTNMLIB_RECORD_MODULE_INFO;

#define TNMLIB_CFG_MAX_MODULES_ENTRIES  16

typedef struct _TNM_RECORD_MODULES_MAP
{
  TNMLIB_RECORD_MODULE_INFO MI[TNMLIB_CFG_MAX_MODULES_ENTRIES];

} TNMLIB_RECORD_MODULES_MAP, *LPTNMLIB_RECORD_MODULES_MAP;


typedef struct _TNMLIB_DEVICE_TASK_INFO {
  DWORD   dwOffset;
  DWORD   dwSize;
  DWORD   dwTag;
  WORD    wMinVersion;
  WORD    Reserved;
} TNMLIB_DEVICE_TASK_INFO, *LPTNMLIB_DEVICE_TASK_INFO;

typedef struct _TNMLIB_DEVICE_MEMORY_RWINFO
{
  DWORD   dwTotalPages;
  DWORD   dwPageSize;
} TNMLIB_DEVICE_MEMORY_RWINFO, *LPTNMLIB_DEVICE_MEMORY_RWINFO;

typedef struct _TNMLIB_DEVICE_MEMORY_EXINFO 
{
  TNMLIB_DEVICE_MEMORY_RWINFO  RW[2];
} TNMLIB_DEVICE_MEMORY_EXINFO, *LPTNMLIB_DEVICE_MEMORY_EXINFO;

typedef struct _TNMLIB_DEVICE_RECORD_PARAM
{
  TNMLIB_DEVICE_TIME  rsTime;
  TNMLIB_DEVICE_DATE  rsDate;
#ifdef TNMLIB_USE_NAMELESS_STRUCT_UNION
  union
  {
    struct
    {
#endif
      WORD            wTaskNumber;
      WORD            wFlyNumber;
#ifdef TNMLIB_USE_NAMELESS_STRUCT_UNION
    };
    DWORD             dwUTaskFly;
  };
#endif

#ifdef TNMLIB_USE_NAMELESS_STRUCT_UNION
  union
  {
    struct
    {
#endif
      WORD            wDataUnitSerial;
      WORD            wDataUnitVersion;
#ifdef TNMLIB_USE_NAMELESS_STRUCT_UNION
    };
    DWORD             dwUSerialVersion;
  };
#endif

#ifdef TNMLIB_USE_NAMELESS_STRUCT_UNION
  union
  {
#endif
    BYTE                Reserved[4];
#ifdef TNMLIB_USE_NAMELESS_STRUCT_UNION
    DWORD               dwUReserved;
  };
#endif

} TNMLIB_DEVICE_RECORD_PARAM, *LPTNMLIB_DEVICE_RECORD_PARAM;

#include <PopPack.h>

#ifdef TNMLIB_USE_NAMELESS_STRUCT_UNION
// Restore possible supressed warning C4201: nonstandard extension used : nameless struct/union
#pragma warning(pop)

#endif

#endif // _tnmdefs_header_
