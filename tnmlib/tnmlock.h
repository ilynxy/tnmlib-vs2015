#ifndef _tnmlock_header_
#define _tnmlock_header_

BOOL TNMLIB_API
TNMLIB_LockDevice(TNMLIB_HANDLE hTND);

BOOL TNMLIB_API
TNMLIB_UnLockDevice(TNMLIB_HANDLE hTND);

#endif
