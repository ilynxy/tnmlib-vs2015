#include "stdafx.h"

#include "tnmdefs.h"
#include "tnmlib.h"
#include "tnmmsg.h"

#include "i_devman.h"
#include "i_devrw.h"

#include "i_tnminfo.h"
#include "i_memrw.h"

#include "i_devcmd.h"

#include "i_v3memrw.h"

BOOL
_TND_ReadRecordsTableSnapshot
(
  LPTNMLIB_DEVICE_INSTANCE                pDI,
  LPTNMLIB_DEVICE_RECORDS_TABLE_SNAPSHOT  pRTS
)
{
  BOOL fSuccess = FALSE;
  DBG_ENTRY;

  {
    const DWORD snapshot_size = sizeof(TNMLIB_DEVICE_RECORDS_TABLE_SNAPSHOT);
    
    WORD wVersion;

    fSuccess = _TND_GetVersionInfo(pDI, NULL);
    if (!fSuccess)
      goto _cleanup;

    wVersion = pDI->cache.VI.wTNVersion;

    if (wVersion < 0x300)
    {
      pDI->cbw.wOperation = TNMLIB_V2DEVICE_COMMAND_READRECORDMEMORY;
      pDI->cbw.dwArgs[0]  = 0; // Offset
      pDI->cbw.dwArgs[1]  = snapshot_size; // Size

      fSuccess = _DeviceCBWAndDataStage(
        pDI, 
        _CBW_DATASTAGE_READ,
        pRTS, 
        snapshot_size,
        1);
    }
    else if (wVersion >= 0x300 && wVersion < 0x400)
    {
      const DWORD page_size   = 32;
      const DWORD page_count  = snapshot_size / page_size;
      const DWORD page_offset = 0;

      pDI->cbw.wOperation = TNMLIB_DEVICE_COMMAND_EXREAD_MEMORY;
      pDI->cbw.dwArgs[0]  = TNMLIB_DEVICE_MEMTYPE_RECORDS;
      pDI->cbw.dwArgs[1]  = page_offset; // Offset
      pDI->cbw.dwArgs[2]  = page_count; // Size

      fSuccess = _DeviceCBWAndDataStage(
        pDI, 
        _CBW_DATASTAGE_READ,
        pRTS, 
        page_count,
        page_size);
    }
    else
    {
      fSuccess = _TND_ReadMemory(pDI, TNMLIB_DEVICE_MEMTYPE_RECORDS, 
        0, snapshot_size,
        pRTS);
    }
  }

_cleanup:

  DBG_LEAVE(fSuccess);
  return fSuccess;
}

BOOL
_TND_WriteRecordsTableSnapshot
(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  LPTNMLIB_DEVICE_RECORDS_TABLE_SNAPSHOT  pRTS
)
{
  BOOL fSuccess = FALSE;
  DBG_ENTRY;

  {
    const DWORD snapshot_size = sizeof(TNMLIB_DEVICE_RECORDS_TABLE_SNAPSHOT);

    WORD wVersion;

    fSuccess = _TND_GetVersionInfo(pDI, NULL);
    if (!fSuccess)
      goto _cleanup;

    wVersion = pDI->cache.VI.wTNVersion;

    if (wVersion < 0x300)
    {
      pDI->cbw.wOperation = TNMLIB_V2DEVICE_COMMAND_WRITERECORDMEMORY;
      pDI->cbw.dwArgs[0]  = 0; // Offset
      pDI->cbw.dwArgs[1]  = snapshot_size; // Size

      _MarkCacheAsInvalid(&pDI->cache, _CACHE_MEMORY_INFO | _CACHE_RECORDS_INFO);

      fSuccess = _DeviceCBWAndDataStage(
        pDI, 
        _CBW_DATASTAGE_WRITE,
        pRTS, 
        snapshot_size,
        1);
    }
    else if (wVersion >= 0x300 && wVersion < 0x400)
    {
      const DWORD page_size   = 32;
      const DWORD page_count  = snapshot_size / page_size;
      const DWORD page_offset = 0;

      pDI->cbw.wOperation = TNMLIB_DEVICE_COMMAND_EXWRITE_MEMORY | TNMLIB_DEVICE_COMMAND_ADD_SYNC_STAGE;
      pDI->cbw.dwArgs[0]  = TNMLIB_DEVICE_MEMTYPE_RECORDS;
      pDI->cbw.dwArgs[1]  = page_offset; // Offset
      pDI->cbw.dwArgs[2]  = page_count; // Size

      _MarkCacheAsInvalid(&pDI->cache, _CACHE_MEMORY_INFO | _CACHE_RECORDS_INFO);

      fSuccess = _DeviceCBWAndDataStage(
        pDI, 
        _CBW_DATASTAGE_WRITE,
        pRTS, 
        page_count,
        page_size);
    }
    else
    {
      fSuccess = _TND_WriteMemory(pDI, TNMLIB_DEVICE_MEMTYPE_RECORDS, 
        0, snapshot_size,
        pRTS);
    }

  } 

_cleanup:
  DBG_LEAVE(fSuccess);
  return fSuccess;
}
