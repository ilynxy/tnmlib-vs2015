#include "stdafx.h"

#include "tnmdefs.h"
#include "tnmlib.h"
#include "tnmerrs.h"

#include "i_gvars.h"

/*

*/
DWORD TNMLIB_API 
TNMLIB_GetErrorString(
  DWORD dwErrorCode, 
  BOOL fAllocate, 
  LPTSTR lpBuffer, 
  DWORD dwSize
  )
{
  DWORD dwLength;
  DWORD dwFlags;
  LPVOID lpSource;

  if (dwErrorCode & 0x20000000) 
  {
    dwFlags = FORMAT_MESSAGE_FROM_HMODULE;
    lpSource = g_hModule;
  } 
  else 
  {
    dwFlags = FORMAT_MESSAGE_FROM_SYSTEM;
    lpSource = NULL;
  }

  if (fAllocate) 
    dwFlags |= FORMAT_MESSAGE_ALLOCATE_BUFFER;

  dwLength = FormatMessage(dwFlags, lpSource, dwErrorCode, 0, lpBuffer, dwSize, NULL);

  if (dwLength == 0 && GetLastError() == ERROR_MR_MID_NOT_FOUND) {
    TCHAR lpszHexCode[9];

    LPCTSTR lpszSource = _T("DLL");

    DWORD_PTR vl[2] = 
    { 
      (DWORD_PTR)lpszHexCode, 
      (DWORD_PTR)lpszSource 
    };

#ifdef TNMLIB_USE_SECURE_CRT
    _stprintf_s(lpszHexCode, sizeof(lpszHexCode) / sizeof(lpszHexCode[0]),
      _T("%08X"), dwErrorCode);
#else
    _stprintf(lpszHexCode, _T("%08X"), dwErrorCode);
#endif
    dwErrorCode = GetLastError();

    dwFlags = (dwFlags & ~FORMAT_MESSAGE_FROM_HMODULE) | 
      FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ARGUMENT_ARRAY;

    lpSource = NULL;

    dwLength = FormatMessage(dwFlags, lpSource, 
      ERROR_MR_MID_NOT_FOUND, 0, lpBuffer, dwSize, (va_list *)vl);
  }

  // Remove trailing <CR><LF>
  if (dwLength != 0) 
  {
    LPTSTR p = dwFlags & FORMAT_MESSAGE_ALLOCATE_BUFFER ?
      *((LPTSTR *)lpBuffer) : lpBuffer;

    p += dwLength - 1;

    while ((*p == _T('\r') || *p == _T('\n')) && (dwLength != 0))
    {
      *p -- = _T('\0');
      -- dwLength;
    }
  }

  _ASSERTE(dwLength != 0);
  return dwLength;
}

/*

*/
BOOL TNMLIB_API 
  TNMLIB_FreeErrorString(
  LPTSTR lpBuffer
  )
{
  return (LocalFree((HLOCAL)lpBuffer) == NULL);
}
