// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers

#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>

#include <tchar.h>

#include <windows.h>
#include <winioctl.h>

#include <stdlib.h>
#include <malloc.h>

#include <stdint.h>

#define TNMLIB_USE_SECURE_CRT

#include "i_debug.h"
