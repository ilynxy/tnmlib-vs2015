#pragma once

BOOL
_PnP_RegisterHandleNotification(HANDLE hHandle);

BOOL 
 _PnP_GetDeviceInterfacePath(
  LPCGUID lpGUID, 
  DWORD dwInterfaceNumber, 
  LPTSTR lpszDevicePath, 
  DWORD dwcchDevicePathLength, 
  LPDWORD lpcchRequiredDevicePathLen);

