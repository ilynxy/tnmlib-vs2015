#pragma once

BOOL
_TND_GetVersionInfo(
  LPTNMLIB_DEVICE_INSTANCE pDI, 
  LPTNMLIB_DEVICE_VERSION_INFO pDVI
  );

BOOL
_TND_GetMemoryInfo(
  LPTNMLIB_DEVICE_INSTANCE pDI, 
  LPTNMLIB_DEVICE_MEMORY_INFO pDMI
  );

BOOL
_TND_GetMemoryExInfoArray(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  DWORD dwStartMemoryTypeIndex,
  DWORD dwNumberOfMemoryTypes,
  LPTNMLIB_DEVICE_MEMORY_EXINFO pDMEI
  );

BOOL
_TND_GetRecordsInfoArray(
  LPTNMLIB_DEVICE_INSTANCE pDI, 
  DWORD dwStartRecordIndex,
  DWORD dwNumberOfRecords,
  LPTNMLIB_DEVICE_RECORD_INFO pRI);

BOOL
_TND_NC_GetRecordsModulesMapArray(
  LPTNMLIB_DEVICE_INSTANCE pDI, 
  DWORD dwStartRecordIndex,
  DWORD dwNumberOfRecords,
  LPTNMLIB_RECORD_MODULES_MAP pRMM);

BOOL
_TND_GetTasksInfoArray(
  LPTNMLIB_DEVICE_INSTANCE pDI, 
  DWORD dwStartTaskIndex,
  DWORD dwNumberOfTasks,
  LPTNMLIB_DEVICE_TASK_INFO pTI);

BOOL
_TND_GetUTInfoObjectSize(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  DWORD   dwObjectIndex,
  DWORD*  pdwObjectSize
  );

BOOL
_TND_GetUTInfoObject(
  LPTNMLIB_DEVICE_INSTANCE pDI, 
  DWORD   dwObjectIndex,
  void*   pObject,
  DWORD   dwObjectSize
  );
