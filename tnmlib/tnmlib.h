#ifndef _tnmlib_header_
#define _tnmlib_header_

TNMLIB_HANDLE TNMLIB_API
TNMLIB_OpenDevice(
  LPCGUID lpDeviceGUID, 
  DWORD dwNumberOfInterface
  );

BOOL TNMLIB_API
TNMLIB_CloseDevice(TNMLIB_HANDLE hTND);

BOOL TNMLIB_API
TNMLIB_CancelDeviceRequest(TNMLIB_HANDLE hTND);

BOOL TNMLIB_API
TNMLIB_AsyncResetDevice(TNMLIB_HANDLE hTND);

#endif _tnmlib_header_
