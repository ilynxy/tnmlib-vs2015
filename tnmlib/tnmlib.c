#include "stdafx.h"

#include "tnmdefs.h"
#include "tnmlib.h"

#include "i_devman.h"

#include "i_devrw.h"

#include "i_pnpman.h"

#include "tnmmsg.h"

#include "i_gvars.h"
#include "i_extpep.h"

TNMLIB_HANDLE TNMLIB_API
TNMLIB_OpenDevice(
  LPCGUID lpDeviceGUID, 
  DWORD dwNumberOfInterface
  )
{
  TNMLIB_HANDLE h = TNMLIB_INVALID_HANDLE;
  LPTNMLIB_DEVICE_INSTANCE pDI = NULL;
  LPTSTR lpszDevicePath = NULL;
  DWORD dwDevicePathLength = 0;
  BOOL fSuccess = FALSE; 
  TNMLIB_BACKEND be = TNMLIB_USBIO_BACKEND;

  const GUID* lpGUID;

  /*
  const GUID g_TNGUID = 
  {
    0x325DDF96, 0x938C, 0x11D3, 
    {0x9E, 0x34, 0x00, 0x80, 0xC8, 0x27, 0x27, 0xF4}
  };
  */

  static const GUID g_TNGUID  = 
  {
    0xB5A39724, 0x2BD6, 0x4218, 
    {0x94, 0x03, 0xA5, 0x4E, 0x26, 0x29, 0x56, 0x1D}
  };

  static const GUID g_WINUSB_GUID =
  {
    0x44444444, 0x2BD6, 0x4218,
    { 0x94, 0x03, 0xA5, 0x4E, 0x26, 0x29, 0x56, 0x1D }
  };

  DBG_ENTRY;

  if (lpDeviceGUID == NULL)
    lpGUID = &g_TNGUID;
  else
    lpGUID = lpDeviceGUID;

  fSuccess = _PnP_GetDeviceInterfacePath(
    lpGUID, dwNumberOfInterface, 
    NULL, 0, 
    &dwDevicePathLength);
  if (!fSuccess && (GetLastError() != ERROR_INSUFFICIENT_BUFFER) && lpDeviceGUID == NULL)
  {
    lpGUID = &g_WINUSB_GUID;
    be = TNMLIB_WINUSB_BACKEND;

    fSuccess = _PnP_GetDeviceInterfacePath(
      lpGUID, dwNumberOfInterface,
      NULL, 0,
      &dwDevicePathLength);
  }

  if (!fSuccess && (GetLastError() != ERROR_INSUFFICIENT_BUFFER))
    goto _cleanup;

  lpszDevicePath = malloc(dwDevicePathLength * sizeof(TCHAR));
  if (lpszDevicePath == NULL)
    goto _cleanup;

  fSuccess = _PnP_GetDeviceInterfacePath(
    lpGUID, dwNumberOfInterface, 
    lpszDevicePath, dwDevicePathLength, 
    NULL);
  if (!fSuccess)
    goto _cleanup;


  pDI = _CreateDeviceInstanceByPath(lpszDevicePath, be);
  if (pDI == NULL)
    goto _cleanup;

  EnterCriticalSection(&g_HM_CS);

  h = _HMCreateHandle(pDI);

  LeaveCriticalSection(&g_HM_CS);

  if (h == TNMLIB_INVALID_HANDLE)
  {
    DWORD dwLastError = GetLastError();
    _DeleteDeviceInstance(pDI);
    SetLastError(dwLastError);
  }

_cleanup:

  if (lpszDevicePath != NULL)
    free(lpszDevicePath);

  DBG_LEAVE(h);
  return h;
}

BOOL TNMLIB_API
TNMLIB_CloseDevice(TNMLIB_HANDLE hTND)
{
  BOOL fSuccess = FALSE;
  LPTNMLIB_DEVICE_INSTANCE pDI;

  DBG_ENTRY;

  pDI = _HMDeleteHandle(hTND);
  if (pDI == NULL)
    goto _cleanup;

  fSuccess = _LockDeviceInstance(pDI);
  if (!fSuccess)
  {
    DWORD dwLastError = GetLastError();

    if (dwLastError == TNMLIB_ERR_REQUEST_PENDING)
    {
      fSuccess = _CancelDeviceOperation(pDI);
      _ASSERTE(fSuccess);
    }

    if (dwLastError == TNMLIB_ERR_CANCEL_PENDING)
    {
      EnterCriticalSection(&pDI->csBreakLock);
      LeaveCriticalSection(&pDI->csBreakLock);
      fSuccess = TRUE;
    }
  }
  _ASSERTE(fSuccess);

  fSuccess = _DeleteDeviceInstance(pDI);
  _ASSERTE(fSuccess);

_cleanup:

  DBG_LEAVE(fSuccess);
  return fSuccess;
}

BOOL TNMLIB_API
TNMLIB_CancelDeviceRequest(TNMLIB_HANDLE hTND)
{
  DWORD dwWaitStatus;

  BOOL fSuccess = FALSE;
  LPTNMLIB_DEVICE_INSTANCE pDI;
  DBG_ENTRY;

  EnterCriticalSection(&g_HM_CS);

  pDI = _HMGetInstanceByHandle(hTND, NULL);

  if (pDI != NULL)
  {
    BOOL fSuccesSetBreakEvent;
    EnterCriticalSection(&pDI->csBreakLock);
    fSuccesSetBreakEvent = SetEvent(pDI->hBreakEvent);
    _ASSERTE(fSuccesSetBreakEvent);
    fSuccess = TRUE;
  }

  LeaveCriticalSection(&g_HM_CS);

  if (fSuccess)
  {
    dwWaitStatus = WaitForSingleObject(pDI->hIdleEvent, pDI->dwTimeOut);
    _ASSERTE(dwWaitStatus == WAIT_OBJECT_0);

    // TODO: May be there we need to reset device?
    // if (1)
    {
      pDI->bIOIgnoreBreakEvent = TRUE;

      fSuccess = _AsyncResetDevice(pDI);

      pDI->bIOIgnoreBreakEvent = FALSE;
    }

    fSuccess = ResetEvent(pDI->hBreakEvent);
    _ASSERTE(fSuccess);
    LeaveCriticalSection(&pDI->csBreakLock);
  }

  DBG_LEAVE(fSuccess);
  return fSuccess;
}

BOOL TNMLIB_API
TNMLIB_AsyncResetDevice(TNMLIB_HANDLE hTND)
{
  _EXT_ENTRY

    fSuccess = _AsyncResetDevice(pDI);

    _MarkAllCacheAsInvalid(&pDI->cache);

  _EXT_LEAVE
}
