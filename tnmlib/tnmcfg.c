#include "stdafx.h"

#include "tnmdefs.h"
#include "tnmlib.h"

#include "i_devman.h"
#include "i_devrw.h"
#include "i_extpep.h"

#include "tnmcfg.h"

BOOL TNMLIB_API
TNMLIB_SetTimeout(TNMLIB_HANDLE hTND, DWORD dwTimeout)
{
  _EXT_ENTRY

    pDI->dwTimeOut = dwTimeout;
    fSuccess = TRUE;

  _EXT_LEAVE
}

BOOL TNMLIB_API
TNMLIB_GetTimeout(TNMLIB_HANDLE hTND, LPDWORD lpdwTimeout)
{
  _EXT_ENTRY

    *lpdwTimeout = pDI->dwTimeOut;
    fSuccess = TRUE;

  _EXT_LEAVE
}
