#include "stdafx.h"

#include "tnmdefs.h"

#include "i_cache.h"
#include "i_devman.h"

void
_InitializeCache(LPTNMLIB_DEVICE_CACHE pDC)
{
  size_t i;
  DBG_ENTRY;

  pDC->bmValid = 0;

  for (i = 0; i < _CACHE_VAR_COUNTS; i ++)
  {
    pDC->pVSC[i].uiSize = 0;
    pDC->pVSC[i].pData  = NULL;
  }

  DBG_LEAVE(_CACHE_VAR_COUNTS);
}

void
_DeleteCache(LPTNMLIB_DEVICE_CACHE pDC)
{
  size_t i;
  DBG_ENTRY;

  for (i = 0; i < _CACHE_VAR_COUNTS; i ++)
  {
    // if (pDC->pVSC[i].pData  != NULL)
    
    free(pDC->pVSC[i].pData);
  }

  DBG_LEAVE(_CACHE_VAR_COUNTS);
}

LPVOID
_AllocVarCache(LPTNMLIB_DEVICE_CACHE pDC, size_t uiIdx, size_t uiSize)
{
  LPTNMLIB_VARSIZE_CACHE pVSC;

  DBG_ENTRY;

  _ASSERTE(uiIdx < _CACHE_VAR_COUNTS);

  pVSC = &pDC->pVSC[uiIdx];

  if (pDC->pVSC[uiIdx].uiSize < uiSize)
  {
    LPVOID pNewData;
    pNewData = realloc(pVSC->pData, uiSize);
    if (pNewData == NULL)
    {
      free(pVSC->pData);
      pVSC->pData = NULL;
    }
    else
      pVSC->pData = pNewData;
  }

  DBG_LEAVE(pVSC->pData);
  return pVSC->pData;
}

LPVOID
_GetVarCache(LPTNMLIB_DEVICE_CACHE pDC, size_t uiIdx)
{
  DBG_ENTRY;
  _ASSERTE(uiIdx < _CACHE_VAR_COUNTS);

  DBG_LEAVE(pDC->pVSC[uiIdx].pData);
  return pDC->pVSC[uiIdx].pData;
}
