#ifndef _FILEVERSION_H_
#define _FILEVERSION_H_

#define _FILEVERSION			0,4,1,00
#define _FILEVERSION_STR 		"0.4.1.00"

#define _ORIGINALNAME_STR_EN	"tnmlib.dll"
#define _ORIGINALNAME_STR_RU	"tnmlib.dll"

#define _INTERNALNAME_STR_EN	"tnmlib"
#define _INTERNALNAME_STR_RU	"tnmlib"

#define _FILEDESCRIPTION_STR_EN	"TN USB Management library"
#define _FILEDESCRIPTION_STR_RU	"���������� ���������� �� USB"


#endif
