#include "stdafx.h"

#include "tnmdefs.h"
#include "i_devman.h"
#include "i_ovlio.h"
#include "i_devrw.h"

#include "tnmmsg.h"
#include "i_gvars.h"

BOOL
_CancelDeviceOperation(
  LPTNMLIB_DEVICE_INSTANCE pDI
  )
{
  BOOL fSuccess = FALSE;

  EnterCriticalSection(&pDI->csBreakLock);

  do
  {
    
    DWORD dwWaitStatus;

    /*
    dwWaitStatus = WaitForSingleObject(pDI->hIdleEvent, 0);
    if (dwWaitStatus == WAIT_OBJECT_0)
    {
      fSuccess = TRUE;
      break;
    }
    */

    fSuccess = SetEvent(pDI->hBreakEvent);
    _ASSERTE(fSuccess);

    dwWaitStatus = WaitForSingleObject(pDI->hIdleEvent, pDI->dwTimeOut);
    _ASSERTE(dwWaitStatus == WAIT_OBJECT_0);

    fSuccess = ResetEvent(pDI->hBreakEvent);
    _ASSERTE(fSuccess);

  } while (FALSE);

  LeaveCriticalSection(&pDI->csBreakLock);
  return fSuccess;
}

BOOL
_LockDeviceInstance(LPTNMLIB_DEVICE_INSTANCE pDI)
{
  BOOL fSuccess = FALSE;
  DWORD dwWaitStatus;

  do 
  {
    dwWaitStatus = WaitForSingleObject(pDI->hBreakEvent, 0);
    if (dwWaitStatus == WAIT_OBJECT_0)
    {
      SetLastError(TNMLIB_ERR_CANCEL_PENDING);
      break;
    }

    dwWaitStatus = WaitForSingleObject(pDI->hIdleEvent, 0);
    if (dwWaitStatus == WAIT_TIMEOUT)
    {
      SetLastError(TNMLIB_ERR_REQUEST_PENDING);
      break;
    }

    fSuccess = ResetEvent(pDI->hIdleEvent);

  } while (FALSE);

  return fSuccess;
}

void
_ReleaseDeviceInstance(LPTNMLIB_DEVICE_INSTANCE pDI)
{
  BOOL fSuccess = FALSE;
  DWORD dwLastError;

  dwLastError = GetLastError();

  do 
  {
    BOOL fSuccess;
    _ASSERTE(WaitForSingleObject(pDI->hIdleEvent, 0) == WAIT_TIMEOUT);
    fSuccess = SetEvent(pDI->hIdleEvent);
    _ASSERTE(fSuccess);

  } while (FALSE);

  SetLastError(dwLastError);
}

LPTNMLIB_DEVICE_INSTANCE
_GetDeviceInstanceAndLock(HANDLE hTND)
{
  BOOL fSuccess;
  LPTNMLIB_DEVICE_INSTANCE pDI;

  EnterCriticalSection(&g_HM_CS);

  pDI = _HMGetInstanceByHandle(hTND, NULL);
  if (pDI != NULL)
  {
    fSuccess = _LockDeviceInstance(pDI);
    if (!fSuccess)
      // return NULL;
      pDI = NULL;
  }

  LeaveCriticalSection(&g_HM_CS);
  return pDI;
}

BOOL 
_ReadWriteDevicePipeSync(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  BOOL      fWrite,
  LPVOID    lpBuffer, 
  DWORD64   ldwNumberOfBytesToTransfer,
  DWORD64*  lpldwNumberOfBytesTransfered
  )
{
  BOOL    fSuccess = TRUE;
  DWORD64 ldwBytesReturned;
  HANDLE  hPipe;
  UCHAR   ucPipeID;

  DBG_ENTRY;

  ldwBytesReturned = 0;

  if (fWrite)
  { 
    hPipe = pDI->hOutPipe;
    ucPipeID = TNMLIB_DEVICE_CMDOUT_ENDPOINT;
  }
  else
  { 
    hPipe = pDI->hInPipe;
    ucPipeID = TNMLIB_DEVICE_CMDIN_ENDPOINT;
  }

  do 
  {
    LPBYTE p;
    DWORD dwTransactBytes;
    DWORD dwRW;

    if (lpldwNumberOfBytesTransfered != NULL)
      *lpldwNumberOfBytesTransfered = 0;

    p = (LPBYTE)lpBuffer;

    while (ldwNumberOfBytesToTransfer)
    {
      if (ldwNumberOfBytesToTransfer < pDI->dwMaxTransferSize)
        dwTransactBytes = (DWORD)ldwNumberOfBytesToTransfer;
      else
        dwTransactBytes = pDI->dwMaxTransferSize;
      
      if (IS_USBIO_BACKEND(pDI))
      {
        if (fWrite)
          fSuccess = WriteFile(hPipe, p, dwTransactBytes, &dwRW, &pDI->Overlapped);
        else
          fSuccess = ReadFile(hPipe, p, dwTransactBytes, &dwRW, &pDI->Overlapped);
      }

      if (IS_WINUSB_BACKEND(pDI))
      {
        if (fWrite)
          fSuccess = WinUsb_WritePipe(pDI->wihDevice, ucPipeID, p, dwTransactBytes, &dwRW, &pDI->Overlapped);
        else
          fSuccess = WinUsb_ReadPipe(pDI->wihDevice, ucPipeID, p, dwTransactBytes, &dwRW, &pDI->Overlapped);
      }

      if (!fSuccess)
      {
        DWORD dwWaitStatus;
        HANDLE hEvents[2] = { pDI->Overlapped.hEvent, pDI->hBreakEvent };

        if (GetLastError() != ERROR_IO_PENDING)
          break;

        // TODO !!!
        dwWaitStatus = WaitForMultipleObjects(2, hEvents, FALSE, pDI->dwTimeOut);

        if (dwWaitStatus != (WAIT_OBJECT_0 + 0))
        {
          if (IS_USBIO_BACKEND(pDI))
          {
            CancelIo(hPipe);
          }

          if (IS_WINUSB_BACKEND(pDI))
          {
            WinUsb_AbortPipe(pDI->wihDevice, ucPipeID);
          }
        }

        if (IS_USBIO_BACKEND(pDI))
        {
          fSuccess = GetOverlappedResult(hPipe,
            &pDI->Overlapped,
            &dwRW,
            FALSE);
        }

        if (IS_WINUSB_BACKEND(pDI))
        {
          fSuccess = WinUsb_GetOverlappedResult(
            pDI->wihDevice, 
            &pDI->Overlapped,
            &dwRW, FALSE);
        }

        if (dwWaitStatus == WAIT_TIMEOUT)
          SetLastError(WAIT_TIMEOUT);

        if (!fSuccess)
          break;
      }

      if (lpldwNumberOfBytesTransfered == NULL)
      {
        if (dwRW != dwTransactBytes)
        {
          fSuccess = FALSE;

          if (dwRW < dwTransactBytes)
            SetLastError(TNMLIB_ERR_PROTO_TRANSACTION_UNDERRUN);
          else
            SetLastError(TNMLIB_ERR_PROTO_TRANSACTION_OVERRUN);

          break;
        }
      }

      p += dwRW;
      ldwNumberOfBytesToTransfer -= dwRW;
      ldwBytesReturned += dwRW;
    }

    if (lpldwNumberOfBytesTransfered != NULL)
      *lpldwNumberOfBytesTransfered = ldwBytesReturned;

  } while (FALSE);

  DBG_LEAVE(fSuccess);
  return fSuccess;
}

BOOL
_ValidateCSW(LPTNMLIB_DEVICE_CSW pCSW, WORD wOperation)
{
  BOOL fSuccess = FALSE;
  DWORD dwStatus;

  DBG_ENTRY;

  do 
  {
    dwStatus = TNMLIB_ERR_PROTO_INVALID_TAG;
    if (pCSW->dwTag != TNMLIB_DEVICE_TAG)        
      break;

    dwStatus = TNMLIB_ERR_PROTO_INVALID_CSW_LENGTH;
    if (pCSW->wcbLength != sizeof(TNMLIB_DEVICE_CSW))
      break;
/*
    dwStatus = TNMLIB_ERR_PROTO_OPERATION_MISMATCH;
    if (pCSW->wOperation != wOperation)
      break;
*/
    dwStatus = TNMLIB_DEVICE_ERROR_BASE | pCSW->dwStatus;
    if (pCSW->dwStatus != 0x00000000)
      break;

    dwStatus = ERROR_SUCCESS;
    fSuccess = TRUE;

  } while (FALSE);

  SetLastError(dwStatus);

  DBG_LEAVE(fSuccess);

  return fSuccess;
}

BOOL
_DeviceSendCBW(LPTNMLIB_DEVICE_INSTANCE pDI)
{
  BOOL fSuccess;  
  DWORD64 dwRW;

  DBG_ENTRY;
  fSuccess = FALSE;
  do 
  {
    _ASSERTE(pDI->cbw.dwTag == TNMLIB_DEVICE_TAG);
    _ASSERTE(pDI->cbw.wcbLength == sizeof(pDI->cbw));

    fSuccess = _ReadWriteDevicePipeSync(pDI, TRUE, &pDI->cbw, sizeof(pDI->cbw), &dwRW);
    if (!fSuccess)
      break;

    if (dwRW != sizeof(pDI->cbw))
    {
      fSuccess = FALSE;
      SetLastError(TNMLIB_ERR_PROTO_CBW_SEND_FAILED);
      break;
    }

  } while (FALSE);

  DBG_LEAVE(fSuccess);
  return fSuccess;
}

BOOL
_DeviceRecvCSW(LPTNMLIB_DEVICE_INSTANCE pDI)
{
  BOOL fSuccess;  
  DWORD64 dwRW;

  DBG_ENTRY;
  fSuccess = FALSE;
  do 
  {
    fSuccess = _ReadWriteDevicePipeSync(pDI, FALSE, &pDI->csw, sizeof(pDI->csw), &dwRW);
    if (!fSuccess)
      break;

    if (dwRW != sizeof(pDI->csw))
    {
      fSuccess = FALSE;
      SetLastError(TNMLIB_ERR_PROTO_CSW_RECV_FAILED);
      break;
    }

  } while (FALSE);

  DBG_LEAVE(fSuccess);
  return fSuccess;
}

BOOL
_DeviceCBWAndDataStage(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  DWORD dwDataStageFlags,
  LPVOID pBuffer, DWORD dwNumberOfPages, DWORD dwPageSize
  )
{
  BOOL fSuccess;
  BOOL fReadOrWrite;

  DWORD64 ldwTransactionSize;

  do 
  {
    fSuccess = _DeviceSendCBW(pDI);
    if (!fSuccess)
      break;

    fSuccess = _DeviceRecvCSW(pDI);
    if (!fSuccess)
      break;

    fSuccess = _ValidateCSW(&pDI->csw, pDI->cbw.wOperation & TNMLIB_DEVICE_COMMAND_MASK);
    if (!fSuccess)
      break;

    if (dwDataStageFlags == _CBW_DATASTAGE_NONE)
      break;

    // Validate transaction size
    if (pDI->csw.dwTransactionSize != dwNumberOfPages)
    {
      fSuccess = FALSE;

      if (pDI->csw.dwTransactionSize < dwNumberOfPages)
        SetLastError(TNMLIB_DEVICE_ERR_PROTO_TRANSACTION_UNDERRUN);
      else
        SetLastError(TNMLIB_DEVICE_ERR_PROTO_TRANSACTION_OVERRUN);      

      break;
    }

    if (dwDataStageFlags == _CBW_DATASTAGE_VALIDATE)
      break;
    
    ldwTransactionSize  = dwPageSize;
    ldwTransactionSize *= dwNumberOfPages;

    fReadOrWrite = (dwDataStageFlags == _CBW_DATASTAGE_WRITE);

    fSuccess = _ReadWriteDevicePipeSync(
      pDI, 
      fReadOrWrite, 
      pBuffer, ldwTransactionSize, NULL);

    if (!fSuccess)
      break;
  
    /*
    if (dwRW != ldwTransactionSize)
    {
      fSuccess = FALSE;

      if (dwRW < ldwTransactionSize)
        SetLastError(TNMLIB_ERR_PROTO_TRANSACTION_UNDERRUN);
      else
        SetLastError(TNMLIB_ERR_PROTO_TRANSACTION_OVERRUN);

      break;
    }
    */

    // Synced queries
    if (pDI->cbw.wOperation & TNMLIB_DEVICE_COMMAND_ADD_SYNC_STAGE)
    {
      fSuccess = _DeviceRecvCSW(pDI);
      if (!fSuccess)
        break;

      fSuccess = _ValidateCSW(&pDI->csw, TNMLIB_DEVICE_COMMAND_SYNC_REPLY);
      if (!fSuccess)
        break;
    }

  } while (FALSE);

  return fSuccess;
}
