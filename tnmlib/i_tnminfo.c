#include "stdafx.h"

#include "tnmdefs.h"
#include "tnmlib.h"
#include "tnmmsg.h"

#include "i_devman.h"
#include "i_devrw.h"

#include "i_tnminfo.h"

#include "i_devcmd.h"

/*
  functions get info from device with no cache
*/
BOOL
_TND_NC_GetVersionInfo(
  LPTNMLIB_DEVICE_INSTANCE pDI, 
  LPTNMLIB_DEVICE_VERSION_INFO pDVI
  )
{
  BOOL fSuccess;
  DBG_ENTRY;

  {
    pDI->cbw.wOperation = TNMLIB_DEVICE_COMMAND_GET_VERSION_INFO;

    _ASSERTE(_CrtIsValidPointer(pDVI, sizeof(TNMLIB_DEVICE_VERSION_INFO), TRUE));

    fSuccess = _DeviceCBWAndDataStage(
      pDI, 
      _CBW_DATASTAGE_READ,
      pDVI, sizeof(TNMLIB_DEVICE_VERSION_INFO), 1);

  }

  DBG_LEAVE(fSuccess);
  return fSuccess;
}

BOOL
_TND_NC_GetMemoryInfo(
  LPTNMLIB_DEVICE_INSTANCE pDI, 
  LPTNMLIB_DEVICE_MEMORY_INFO pDMI
  )
{
  BOOL fSuccess;
  DBG_ENTRY;

  _ASSERTE(_CrtIsValidPointer(pDMI, sizeof(TNMLIB_DEVICE_MEMORY_INFO), TRUE));

  pDI->cbw.wOperation = TNMLIB_DEVICE_COMMAND_GET_MEMORY_INFO;
  fSuccess = _DeviceCBWAndDataStage(
    pDI, 
    _CBW_DATASTAGE_READ,
    pDMI, sizeof(TNMLIB_DEVICE_MEMORY_INFO), 1);

  DBG_LEAVE(fSuccess);
  return fSuccess;
}

BOOL
_TND_NC_GetMemoryExInfoArray(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  DWORD dwStartMemoryTypeIndex,
  DWORD dwNumberOfMemoryTypes,
  LPTNMLIB_DEVICE_MEMORY_EXINFO pDMEI
  )
{
  BOOL fSuccess;
  DWORD dwLength;

  DBG_ENTRY;

  pDI->cbw.wOperation = TNMLIB_DEVICE_COMMAND_GET_MEMORY_EXINFO;
  pDI->cbw.dwArgs[0]  = dwStartMemoryTypeIndex;
  pDI->cbw.dwArgs[1]  = dwNumberOfMemoryTypes;

  dwLength = dwNumberOfMemoryTypes * sizeof(TNMLIB_DEVICE_MEMORY_EXINFO);

  _ASSERTE(_CrtIsValidPointer(pDMEI, dwLength, TRUE));

  fSuccess = _DeviceCBWAndDataStage(
    pDI, 
    _CBW_DATASTAGE_READ,
    pDMEI, dwLength, 1);

  DBG_LEAVE(fSuccess);
  return fSuccess;
}

BOOL
_TND_NC_GetRecordsInfoArray(
  LPTNMLIB_DEVICE_INSTANCE pDI, 
  DWORD dwStartRecordIndex,
  DWORD dwNumberOfRecords,
  LPTNMLIB_DEVICE_RECORD_INFO pRI)
{
  BOOL fSuccess;
  DWORD dwLength;

  DBG_ENTRY;

  pDI->cbw.wOperation = TNMLIB_DEVICE_COMMAND_GET_RECORDS_INFO;
  pDI->cbw.dwArgs[0] = dwStartRecordIndex;
  pDI->cbw.dwArgs[1] = dwNumberOfRecords;

  dwLength = dwNumberOfRecords * sizeof(TNMLIB_DEVICE_RECORD_INFO);

  _ASSERTE(_CrtIsValidPointer(pRI, dwLength, TRUE));

  fSuccess = _DeviceCBWAndDataStage(
    pDI, 
    _CBW_DATASTAGE_READ,
    pRI, dwLength, 1);

  DBG_LEAVE(fSuccess);
  return fSuccess;
}

BOOL
_TND_NC_GetRecordsModulesMapArray(
  LPTNMLIB_DEVICE_INSTANCE pDI, 
  DWORD dwStartRecordIndex,
  DWORD dwNumberOfRecords,
  LPTNMLIB_RECORD_MODULES_MAP pRMM)
{
  BOOL fSuccess;
  DWORD dwLength;

  DBG_ENTRY;

  pDI->cbw.wOperation = TNMLIB_DEVICE_COMMAND_GET_RECORDS_MODULES_MAP;
  pDI->cbw.dwArgs[0] = dwStartRecordIndex;
  pDI->cbw.dwArgs[1] = dwNumberOfRecords;

  dwLength = dwNumberOfRecords * sizeof(TNMLIB_RECORD_MODULES_MAP);

  _ASSERTE(_CrtIsValidPointer(pRMM, dwLength, TRUE));

  fSuccess = _DeviceCBWAndDataStage(
    pDI, 
    _CBW_DATASTAGE_READ,
    pRMM, dwLength, 1);

  DBG_LEAVE(fSuccess);
  return fSuccess;
}

BOOL
_TND_NC_GetTasksInfoArray(
  LPTNMLIB_DEVICE_INSTANCE pDI, 
  DWORD dwStartTaskIndex,
  DWORD dwNumberOfTasks,
  LPTNMLIB_DEVICE_TASK_INFO pTI)
{
  BOOL fSuccess;
  DWORD dwLength;

  DBG_ENTRY;

  pDI->cbw.wOperation = TNMLIB_DEVICE_COMMAND_GET_TASKS_INFO;
  pDI->cbw.dwArgs[0] = dwStartTaskIndex;
  pDI->cbw.dwArgs[1] = dwNumberOfTasks;

  dwLength = dwNumberOfTasks * sizeof(TNMLIB_DEVICE_TASK_INFO);

  _ASSERTE(_CrtIsValidPointer(pTI, dwLength, TRUE));

  fSuccess = _DeviceCBWAndDataStage(
    pDI, 
    _CBW_DATASTAGE_READ,
    pTI, dwLength, 1);

  DBG_LEAVE(fSuccess);
  return fSuccess;
}

// Cached version of functions
BOOL
_TND_GetVersionInfo(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  LPTNMLIB_DEVICE_VERSION_INFO pDVI
  )
{
  BOOL fSuccess;
  DBG_ENTRY;

  {
    fSuccess = _IsCacheValid(&pDI->cache, _CACHE_VERSION_INFO);
    if (!fSuccess)
    {
      fSuccess = _TND_NC_GetVersionInfo(pDI, &pDI->cache.VI);
      if (!fSuccess)
        goto _cleanup;

      _MarkCacheAsValid(&pDI->cache, _CACHE_VERSION_INFO);
    }
    else
      SetLastError(TNMLIB_ERR_VALUE_FROM_CACHE);

    if (pDVI != NULL)
    {
      _ASSERTE(_CrtIsValidPointer(pDVI, sizeof(TNMLIB_DEVICE_VERSION_INFO), TRUE));
      *pDVI = pDI->cache.VI;
    }

  }

_cleanup:
  DBG_LEAVE(fSuccess);
  return fSuccess;
}

BOOL
_TND_GetMemoryInfo(
  LPTNMLIB_DEVICE_INSTANCE pDI, 
  LPTNMLIB_DEVICE_MEMORY_INFO pDMI
  )
{
  BOOL fSuccess;
  DBG_ENTRY;

  {
    fSuccess = _IsCacheValid(&pDI->cache, _CACHE_MEMORY_INFO);

    if (!fSuccess)
    {
      fSuccess = _TND_NC_GetMemoryInfo(pDI, &pDI->cache.MI);
      if (!fSuccess)
        goto _cleanup;

      _MarkCacheAsValid(&pDI->cache, _CACHE_MEMORY_INFO);
    }
    else
      SetLastError(TNMLIB_ERR_VALUE_FROM_CACHE);

    if (pDMI != NULL)
    {
      _ASSERTE(_CrtIsValidPointer(pDMI, sizeof(pDI->cache.MI), TRUE));
      *pDMI = pDI->cache.MI;
    }

  } 
_cleanup:
  DBG_LEAVE(fSuccess);
  return fSuccess;
}


BOOL
_TND_GetMemoryExInfoArray(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  DWORD dwStartMemoryTypeIndex,
  DWORD dwNumberOfMemoryTypes,
  LPTNMLIB_DEVICE_MEMORY_EXINFO pDMEI
  )
{
  BOOL fSuccess;
  DBG_ENTRY;

  {
    LPTNMLIB_DEVICE_MEMORY_EXINFO c_pDMEI;

    fSuccess = _TND_GetMemoryInfo(pDI, NULL);
    if (!fSuccess)
      goto _cleanup;

    if (dwStartMemoryTypeIndex >= pDI->cache.MI.dwNumberOfMemoryTypes)
    {
      fSuccess = FALSE;
      if (dwNumberOfMemoryTypes == 1)
        SetLastError(TNMLIB_DEVICE_ERR_NO_SUCH_MEMORY_TYPE);
      else
        SetLastError(TNMLIB_DEVICE_ERR_OFFSET_OUT_OF_RANGE);
      goto _cleanup;
    }

    if ( (dwStartMemoryTypeIndex + dwNumberOfMemoryTypes) > pDI->cache.MI.dwNumberOfMemoryTypes)
    {
      fSuccess = FALSE;
      SetLastError(TNMLIB_DEVICE_ERR_SIZE_OUT_OF_RANGE);
      goto _cleanup;
    }

    fSuccess = _IsCacheValid(&pDI->cache, _CACHE_MEMORY_EXINFO);
    if (!fSuccess)
    {
      DWORD dwLength = pDI->cache.MI.dwNumberOfMemoryTypes * sizeof(TNMLIB_DEVICE_MEMORY_EXINFO);
      c_pDMEI = _AllocVarCache(&pDI->cache, _CACHE_VAR_EXINFO_INDEX, dwLength);

      if (c_pDMEI == NULL)
      {
        fSuccess = FALSE;
        goto _cleanup;
      }

      fSuccess = _TND_NC_GetMemoryExInfoArray(pDI, 
        0, pDI->cache.MI.dwNumberOfMemoryTypes,
        c_pDMEI);

      if (!fSuccess)
        goto _cleanup;

      _MarkCacheAsValid(&pDI->cache, _CACHE_MEMORY_EXINFO);
    }
    else
      SetLastError(TNMLIB_ERR_VALUE_FROM_CACHE);

    if (pDMEI != NULL)
    {
      size_t i;
      c_pDMEI = _GetVarCache(&pDI->cache, _CACHE_VAR_EXINFO_INDEX);

      for (i = 0; i < dwNumberOfMemoryTypes; i ++)
        pDMEI[i] = c_pDMEI[dwStartMemoryTypeIndex + i];
    }

  }
_cleanup:
  DBG_LEAVE(fSuccess);
  return fSuccess;
}

BOOL
_TND_GetRecordsInfoArray(
  LPTNMLIB_DEVICE_INSTANCE pDI, 
  DWORD dwStartRecordIndex,
  DWORD dwNumberOfRecords,
  LPTNMLIB_DEVICE_RECORD_INFO pRI)
{
  BOOL fSuccess;

  DBG_ENTRY;

  {
    LPTNMLIB_DEVICE_RECORD_INFO c_pRI;

    DWORD dwTotalEntries;
    DWORD dwEntrySize;

    fSuccess = _TND_GetMemoryInfo(pDI, NULL);
    if (!fSuccess)
      goto _cleanup;

    dwTotalEntries = pDI->cache.MI.dwNumberOfRecords;
    dwEntrySize = sizeof(TNMLIB_DEVICE_RECORD_INFO);

    if (dwStartRecordIndex >= dwTotalEntries)
    {
      fSuccess = FALSE;
      if (dwNumberOfRecords == 1 || dwTotalEntries == 0)
        SetLastError(TNMLIB_DEVICE_ERR_NO_SUCH_RECORD);
      else
        SetLastError(TNMLIB_DEVICE_ERR_OFFSET_OUT_OF_RANGE);

      goto _cleanup;
    }

    if ( (dwStartRecordIndex + dwNumberOfRecords) > dwTotalEntries)
    {
      fSuccess = FALSE;
      SetLastError(TNMLIB_DEVICE_ERR_SIZE_OUT_OF_RANGE);
      goto _cleanup;
    }

    fSuccess = _IsCacheValid(&pDI->cache, _CACHE_RECORDS_INFO);
    if (!fSuccess)
    {
      DWORD dwLength = dwTotalEntries * dwEntrySize;
      c_pRI = _AllocVarCache(&pDI->cache, _CACHE_VAR_RECORDS_INDEX, dwLength);

      if (c_pRI == NULL)
      {
        fSuccess = FALSE;
        goto _cleanup;
      }

      fSuccess = _TND_NC_GetRecordsInfoArray(pDI, 
        0, dwTotalEntries,
        c_pRI);

      if (!fSuccess)
        goto _cleanup;

      _MarkCacheAsValid(&pDI->cache, _CACHE_RECORDS_INFO);
    }
    else
      SetLastError(TNMLIB_ERR_VALUE_FROM_CACHE);

    if (pRI != NULL)
    {
      size_t i;
      c_pRI = _GetVarCache(&pDI->cache, _CACHE_VAR_RECORDS_INDEX);

      for (i = 0; i < dwNumberOfRecords; i ++)
        pRI[i] = c_pRI[dwStartRecordIndex + i];
    }

  }

_cleanup:
  DBG_LEAVE(fSuccess);
  return fSuccess;
}

BOOL
_TND_GetTasksInfoArray(
  LPTNMLIB_DEVICE_INSTANCE pDI, 
  DWORD dwStartTaskIndex,
  DWORD dwNumberOfTasks,
  LPTNMLIB_DEVICE_TASK_INFO pTI)
{
  BOOL fSuccess;

  DBG_ENTRY;

  {
    LPTNMLIB_DEVICE_TASK_INFO c_pTI;
    DWORD dwTotalEntries;
    DWORD dwEntrySize;

    fSuccess = _TND_GetMemoryInfo(pDI, NULL);
    if (!fSuccess)
      goto _cleanup;

    dwTotalEntries = pDI->cache.MI.dwNumberOfTasks;
    dwEntrySize = sizeof(TNMLIB_DEVICE_TASK_INFO);

    if (dwStartTaskIndex >= dwTotalEntries)
    {
      fSuccess = FALSE;
      if (dwNumberOfTasks == 1 || dwTotalEntries == 0)
        SetLastError(TNMLIB_DEVICE_ERR_NO_SUCH_TASK);
      else
        SetLastError(TNMLIB_DEVICE_ERR_OFFSET_OUT_OF_RANGE);

      goto _cleanup;
    }

    if ( (dwStartTaskIndex + dwNumberOfTasks) > dwTotalEntries)
    {
      fSuccess = FALSE;
      SetLastError(TNMLIB_DEVICE_ERR_SIZE_OUT_OF_RANGE);
      goto _cleanup;
    }

    fSuccess = _IsCacheValid(&pDI->cache, _CACHE_TASKS_INFO);
    if (!fSuccess)
    {
      DWORD dwLength = dwTotalEntries * dwEntrySize;
      c_pTI = _AllocVarCache(&pDI->cache, _CACHE_VAR_TASKS_INDEX, dwLength);

      if (c_pTI == NULL)
      {
        fSuccess = FALSE;
        goto _cleanup;
      }

      fSuccess = _TND_NC_GetTasksInfoArray(pDI, 
        0, dwTotalEntries,
        c_pTI);

      if (!fSuccess)
        goto _cleanup;

      _MarkCacheAsValid(&pDI->cache, _CACHE_TASKS_INFO);
    }
    else
      SetLastError(TNMLIB_ERR_VALUE_FROM_CACHE);

    if (pTI != NULL)
    {
      size_t i;
      c_pTI = _GetVarCache(&pDI->cache, _CACHE_VAR_TASKS_INDEX);

      for (i = 0; i < dwNumberOfTasks; i ++)
        pTI[i] = c_pTI[dwStartTaskIndex + i];
    }

  }

_cleanup:
  DBG_LEAVE(fSuccess);
  return fSuccess;
}

BOOL
_TND_GetUTInfoObjectSize(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  DWORD   dwObjectIndex,
  DWORD*  pdwObjectSize
  )
{
  BOOL fSuccess;
  DBG_ENTRY;
  {
    pDI->cbw.wOperation = TNMLIB_DEVICE_COMMAND_GET_UTINFO;
    pDI->cbw.dwArgs[0] = dwObjectIndex;
    pDI->cbw.dwArgs[1] = 0x00000000;

    _ASSERTE(_CrtIsValidPointer(pdwObjectSize, sizeof(DWORD), TRUE));

    fSuccess = _DeviceCBWAndDataStage(
      pDI, 
      _CBW_DATASTAGE_READ,
      pdwObjectSize, sizeof(DWORD), 1);
  }
  DBG_LEAVE(fSuccess);
  return fSuccess;
}

BOOL
_TND_GetUTInfoObject(
  LPTNMLIB_DEVICE_INSTANCE pDI, 
  DWORD   dwObjectIndex,
  void*   pObject,
  DWORD   dwObjectSize
  )
{
  BOOL fSuccess;
  DBG_ENTRY;

  {
    pDI->cbw.wOperation = TNMLIB_DEVICE_COMMAND_GET_UTINFO;
    pDI->cbw.dwArgs[0] = dwObjectIndex;
    pDI->cbw.dwArgs[1] = 0x00000001;

    _ASSERTE(_CrtIsValidPointer(pObject, dwObjectSize, TRUE));

    fSuccess = _DeviceCBWAndDataStage(
      pDI, 
      _CBW_DATASTAGE_READ,
      pObject, dwObjectSize, 1);

  }
  DBG_LEAVE(fSuccess);
  return fSuccess;
}
