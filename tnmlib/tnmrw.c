#include "stdafx.h"

#include "tnmdefs.h"
#include "tnmlib.h"

#include "i_devman.h"
#include "i_devrw.h"

#include "i_memrw.h"
#include "i_v3memrw.h"

#include "i_extpep.h"

#include "tnmrw.h"

BOOL TNMLIB_API
TNMLIB_ReadRecord(
  TNMLIB_HANDLE hTND,
  DWORD   dwIndex,
  DWORD64 ldwOffsetBytes,
  DWORD64 ldwBytesToRead,
  LPVOID  pBuffer
  )
{
  _EXT_ENTRY

    fSuccess = _TND_ReadRecord(pDI, dwIndex, ldwOffsetBytes, ldwBytesToRead, pBuffer);

  _EXT_LEAVE
}

BOOL TNMLIB_API
TNMLIB_ReadMemory(
  TNMLIB_HANDLE hTND,
  DWORD   dwMemoryType,
  DWORD64 ldwOffsetBytes,
  DWORD64 ldwBytesToRead,
  LPVOID  pBuffer
  )
{
  _EXT_ENTRY

    fSuccess = _TND_ReadMemory(pDI, dwMemoryType, ldwOffsetBytes, ldwBytesToRead, pBuffer);

  _EXT_LEAVE
}

BOOL TNMLIB_API
TNMLIB_WriteMemory(
  TNMLIB_HANDLE hTND,
  DWORD   dwMemoryType,
  DWORD64 ldwOffsetBytes,
  DWORD64 ldwBytesToWrite,
  LPCVOID  pBuffer
  )
{
  _EXT_ENTRY

    fSuccess = _TND_WriteMemory(pDI, dwMemoryType, ldwOffsetBytes, ldwBytesToWrite, pBuffer);

  _EXT_LEAVE
}

BOOL TNMLIB_API
TNMLIB_RawReadRecord(
  TNMLIB_HANDLE hTND,
  DWORD   dwIndex,
  DWORD   dwOffsetPages,
  DWORD   dwCountPages,
  LPVOID  pBuffer
  )
{
  _EXT_ENTRY

    fSuccess = _TND_RawReadRecord(pDI, dwIndex, dwOffsetPages, dwCountPages, pBuffer);

  _EXT_LEAVE
}

BOOL TNMLIB_API
TNMLIB_RawReadMemory(
  TNMLIB_HANDLE hTND,
  DWORD  dwMemoryType,
  DWORD  dwOffsetPages,
  DWORD  dwCountPages,
  LPVOID  pBuffer
  )
{
  _EXT_ENTRY

    fSuccess = _TND_RawRWMemory(pDI, FALSE, dwMemoryType, dwOffsetPages, dwCountPages, pBuffer);

  _EXT_LEAVE
}

BOOL TNMLIB_API
TNMLIB_RawWriteMemory(
  TNMLIB_HANDLE hTND,
  DWORD  dwMemoryType,
  DWORD  dwOffsetPages,
  DWORD  dwCountPages,
  LPVOID  pBuffer
  )
{
  _EXT_ENTRY

    fSuccess = _TND_RawRWMemory(pDI, TRUE, dwMemoryType, dwOffsetPages, dwCountPages, pBuffer);

  _EXT_LEAVE
}

BOOL TNMLIB_API
TNMLIB_DeleteAllRecords(TNMLIB_HANDLE hTND)
{
  _EXT_ENTRY

    fSuccess = _TND_DeleteAllRecords(pDI);

  _EXT_LEAVE
}

BOOL TNMLIB_API
TNMLIB_VerifyBlock(TNMLIB_HANDLE hTND, DWORD dwBlock)
{
  _EXT_ENTRY

    fSuccess = _TND_VerifyBlock(pDI, dwBlock);

  _EXT_LEAVE
}

BOOL TNMLIB_API
TNMLIB_ReadRecordsTableSnapshot
(
  TNMLIB_HANDLE hTND,
  LPTNMLIB_DEVICE_RECORDS_TABLE_SNAPSHOT  pRTS
)
{
  _EXT_ENTRY

    fSuccess = _TND_ReadRecordsTableSnapshot(pDI, pRTS);

  _EXT_LEAVE
}

BOOL TNMLIB_API
TNMLIB_WriteRecordsTableSnapshot
(
  TNMLIB_HANDLE hTND,
  LPTNMLIB_DEVICE_RECORDS_TABLE_SNAPSHOT  pRTS
)
{
  _EXT_ENTRY

    fSuccess = _TND_WriteRecordsTableSnapshot(pDI, pRTS);

  _EXT_LEAVE
}

