#pragma once

#define _CBW_DATASTAGE_NONE       (0x00000000)
#define _CBW_DATASTAGE_VALIDATE   (0x00000001)
#define _CBW_DATASTAGE_READ       (0x00000002)
#define _CBW_DATASTAGE_WRITE      (0x00000003)

#define TNMLIB_DEVICE_COMMAND_ADD_SYNC_STAGE  (0x00008000)
#define TNMLIB_DEVICE_COMMAND_SYNC_REPLY      (0x00000100)
#define TNMLIB_DEVICE_COMMAND_MASK            (0x000000FF)

BOOL
_LockDeviceInstance(LPTNMLIB_DEVICE_INSTANCE pDI);

void
_ReleaseDeviceInstance(LPTNMLIB_DEVICE_INSTANCE pDI);

BOOL
_CancelDeviceOperation(LPTNMLIB_DEVICE_INSTANCE pDI);

LPTNMLIB_DEVICE_INSTANCE
_GetDeviceInstanceAndLock(HANDLE hTND);

BOOL
_DeviceSendCBW(LPTNMLIB_DEVICE_INSTANCE pDI);

BOOL
_DeviceRecvCSW(LPTNMLIB_DEVICE_INSTANCE pDI);

BOOL
_ValidateCSW(LPTNMLIB_DEVICE_CSW pCSW, WORD wOperation);

BOOL
_DeviceCBWAndValidate(LPTNMLIB_DEVICE_INSTANCE pDI);

BOOL
_DeviceCBWAndDataStage(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  DWORD dwDataStageFlags,
  LPVOID pBuffer, DWORD dwNumberOfPages, DWORD dwPageSize
  );

BOOL 
_ReadWriteDevicePipeSync(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  BOOL      fWrite,
  LPCVOID   lpBuffer, 
  DWORD64   ldwNumberOfBytesToWrite,
  DWORD64*  lpldwNumberOfBytesWritten
  );
