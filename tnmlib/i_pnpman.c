#include "stdafx.h"

#include <dbt.h>
#include <process.h>
#include <winioctl.h>

#include <setupapi.h>
#pragma  comment(lib, "setupapi.lib")

#include "i_gvars.h"

#include "i_pnpman.h"

static const TCHAR szPnPWndClassName[] = _T("TNMLIB_PnPNotificationWindowClass");
static const TCHAR szPnPWindowTitle[]  = _T("TNMLIB_PnPNotificationWindow_Title");

static HANDLE  _g_hThread = NULL;
static ATOM    _g_ClassAtom = 0;
static HWND    _g_hWnd = NULL;

static HANDLE  _g_hThreadEvent = NULL;

LRESULT
_PnP_WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
  LRESULT retcode = 0;

  switch (uMsg)
  {

  case WM_DESTROY:
    PostQuitMessage(0);
    break;

  case WM_DEVICECHANGE:
    if (wParam == DBT_DEVICEREMOVECOMPLETE)
    {
      DEV_BROADCAST_HDR*    pHDR;
      DEV_BROADCAST_HANDLE*  pDBH;

      pHDR = (DEV_BROADCAST_HDR*)(lParam);

      if (pHDR->dbch_devicetype == DBT_DEVTYP_HANDLE)
      {
        pDBH = (DEV_BROADCAST_HANDLE*)(lParam);
        UnregisterDeviceNotification(pDBH->dbch_hdevnotify);
      }
    }
    break;

  default:
    retcode = DefWindowProc(hWnd, uMsg, wParam, lParam);
  }

  return retcode;
}

ATOM
_PnP_RegisterClass(LPCTSTR pszClassName)
{
  ATOM hClassAtom;
  WNDCLASSEX  wcex;
  ZeroMemory(&wcex, sizeof(wcex));

  wcex.cbSize      = sizeof(wcex); 
  //wcex.style    = 0;
  wcex.lpfnWndProc  = (WNDPROC)_PnP_WindowProc;
  //wcex.cbClsExtra   = 0;
  //wcex.cbWndExtra   = 0;
  wcex.hInstance    = g_hModule;
  //wcex.hIcon    = NULL;
  //wcex.hCursor    = NULL;
  //wcex.hbrBackground  = NULL;
  //wcex.lpszMenuName  = NULL;
  wcex.lpszClassName  = pszClassName;
  //wcex.hIconSm    = NULL;

  hClassAtom = RegisterClassEx(&wcex);

  return hClassAtom;
}

HWND
_PnP_CreateWindow(LPCTSTR pszClassName, LPCTSTR pszWindowName)
{
  HWND hWnd;

  hWnd = CreateWindow(
    pszClassName, 
    pszWindowName, 
    WS_OVERLAPPEDWINDOW,
    CW_USEDEFAULT, CW_USEDEFAULT,
    CW_USEDEFAULT, CW_USEDEFAULT,
    NULL,
    NULL,
    g_hModule,
    NULL
    );

  return hWnd;
}

unsigned int
__stdcall
_PnP_MessageLoopThread(void *pParam)
{
  MSG    msg;
  BOOL  bRet;

  UNREFERENCED_PARAMETER(pParam);

  _g_ClassAtom = _PnP_RegisterClass(szPnPWndClassName);
  if (_g_ClassAtom == 0)
  {
    // fail
  }

  _g_hWnd = _PnP_CreateWindow(szPnPWndClassName, szPnPWindowTitle);
  if (_g_hWnd == NULL)
  {
    // fail
  }

  SetEvent(_g_hThreadEvent);

  for (;;)
  {
    bRet = GetMessage(&msg, NULL, 0, 0);

    if (bRet == -1)
      break;

    if (bRet == 0)
      break;

    TranslateMessage(&msg);
    DispatchMessage(&msg);

  }

  return 0;
}

BOOL
_PnP_Initialize()
{
  BOOL fSuccess;
  unsigned int uiTID;

  _g_hThreadEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

  _g_hThread = (HANDLE)_beginthreadex(
    NULL, 
    0, 
    _PnP_MessageLoopThread, 
    NULL, 
    0, 
    &uiTID);

  if (_g_hThread != NULL)
  {
    DWORD dwWaitStatus;
    dwWaitStatus = WaitForSingleObject(_g_hThreadEvent, 1000 * 3);
    if (dwWaitStatus != WAIT_OBJECT_0)
    {
      // fail
    }
  }

  // CloseHandle(_g_hThreadEvent);

  fSuccess = _g_hThread != NULL;

  return fSuccess;
}

BOOL
_PnP_RegisterHandleNotification(HANDLE hHandle)
{
  BOOL fSuccess;

  HDEVNOTIFY hDevNotify;
  DEV_BROADCAST_HANDLE flt;

  fSuccess = TRUE;

  if (_g_hWnd == NULL)
    _PnP_Initialize();

  ZeroMemory(&flt, sizeof(flt));

  flt.dbch_size = sizeof(flt);
  flt.dbch_devicetype = DBT_DEVTYP_HANDLE;
  flt.dbch_handle = hHandle;

  hDevNotify = RegisterDeviceNotification(
    _g_hWnd, 
    &flt,
    DEVICE_NOTIFY_WINDOW_HANDLE);

  return fSuccess;
}

BOOL
_PnP_GetDeviceInterfacePath(
  LPCGUID lpGUID, 
  DWORD dwInterfaceNumber, 
  LPTSTR lpszDevicePath, 
  DWORD dwcchDevicePathLength, 
  LPDWORD lpcchRequiredDevicePathLen) 
{
  DWORD dwStatus = ERROR_SUCCESS;
  BOOL  fSuccess = TRUE;
  DWORD n;
  size_t rl;

  HDEVINFO hardwareDeviceInfo;
  SP_INTERFACE_DEVICE_DATA deviceInfoData;
  PSP_INTERFACE_DEVICE_DETAIL_DATA functionClassDeviceData = 0;

  // _ASSERTE(_CrtIsValidPointer(lpszDevicePath, dwcbDevicePathLength * sizeof(TCHAR), TRUE));

  do {
    deviceInfoData.cbSize = sizeof (SP_INTERFACE_DEVICE_DATA);
    hardwareDeviceInfo = SetupDiGetClassDevs(
      (LPGUID)lpGUID,  
      0, 0, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE
      );
    if (hardwareDeviceInfo == INVALID_HANDLE_VALUE) 
    {
      fSuccess = FALSE;
      dwStatus = GetLastError();
      break;
    }

    fSuccess = SetupDiEnumDeviceInterfaces(hardwareDeviceInfo, 0, lpGUID, dwInterfaceNumber, &deviceInfoData);
    if (!fSuccess) 
    {
      dwStatus = ERROR_DEVICE_NOT_CONNECTED;
      break;
    }

    fSuccess = SetupDiGetInterfaceDeviceDetail(hardwareDeviceInfo, &deviceInfoData, 0, 0, &n, NULL);
    dwStatus = GetLastError();

    if (!fSuccess && (dwStatus != ERROR_INSUFFICIENT_BUFFER)) 
    {
      break;
    }

    functionClassDeviceData = (PSP_INTERFACE_DEVICE_DETAIL_DATA)malloc(n);
    if (functionClassDeviceData == NULL) 
    {
      dwStatus = GetLastError();
      fSuccess = FALSE;
      break;
    }

    functionClassDeviceData->cbSize = sizeof (SP_INTERFACE_DEVICE_DETAIL_DATA);
    fSuccess = SetupDiGetInterfaceDeviceDetail(
      hardwareDeviceInfo, 
      &deviceInfoData, 
      functionClassDeviceData, n, &n, NULL
      );

    if (!fSuccess) 
    {
      dwStatus = GetLastError();
      break;
    }

    rl = (_tcslen(functionClassDeviceData->DevicePath) + 1);

    if (lpszDevicePath != NULL)
    {
      memcpy(
        lpszDevicePath, 
        functionClassDeviceData->DevicePath, 
        min(rl, dwcchDevicePathLength) * sizeof(TCHAR)
        );
    }

    if (lpcchRequiredDevicePathLen != NULL)
      *lpcchRequiredDevicePathLen = (DWORD)rl;

    if (rl > dwcchDevicePathLength) {
      dwStatus = ERROR_INSUFFICIENT_BUFFER;
      fSuccess = FALSE;
    }
    else {
      dwStatus = ERROR_SUCCESS;
    }

  } while (FALSE);

  if (functionClassDeviceData != NULL)
    free(functionClassDeviceData);

  if (hardwareDeviceInfo != INVALID_HANDLE_VALUE) 
  {
    BOOL fSuccessSD;
    fSuccessSD = SetupDiDestroyDeviceInfoList(hardwareDeviceInfo);

    if (fSuccess && !fSuccessSD) 
    {
      dwStatus = GetLastError();
      fSuccess = FALSE;
    }
  }

  SetLastError(dwStatus);
  return fSuccess;
}
