#ifndef _TNMMSG_HEADER_
#define _TNMMSG_HEADER_
//
//  Values are 32 bit values laid out as follows:
//
//   3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1
//   1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  +---+-+-+-----------------------+-------------------------------+
//  |Sev|C|R|     Facility          |               Code            |
//  +---+-+-+-----------------------+-------------------------------+
//
//  where
//
//      Sev - is the severity code
//
//          00 - Success
//          01 - Informational
//          10 - Warning
//          11 - Error
//
//      C - is the Customer code flag
//
//      R - is a reserved bit
//
//      Facility - is the facility code
//
//      Code - is the facility's status code
//
//
// Define the facility codes
//


//
// Define the severity codes
//


//
// MessageId: 0xE0000001L (No symbolic name defined)
//
// MessageText:
//
// HCD: A CRC error has been detected
//


// SymbolicName=USBIO_ERR_CRC
//
// MessageId: 0xE0000002L (No symbolic name defined)
//
// MessageText:
//
// HCD: A bit stuffing error has been detected
//


// SymbolicName=USBIO_ERR_BTSTUFF
//
// MessageId: 0xE0000003L (No symbolic name defined)
//
// MessageText:
//
// HCD: A DATA toggle mismatch (DATA0/DATA1 tokens) has been detected
//


// SymbolicName=USBIO_ERR_DATA_TOGGLE_MISMATCH
//
// MessageId: 0xE0000004L (No symbolic name defined)
//
// MessageText:
//
// HCD: A STALL PID has been detected
//


// SymbolicName=USBIO_ERR_STALL_PID
//
// MessageId: 0xE0000005L (No symbolic name defined)
//
// MessageText:
//
// HCD: The USB device is not responding
//


// SymbolicName=USBIO_ERR_DEV_NOT_RESPONDING
//
// MessageId: 0xE0000006L (No symbolic name defined)
//
// MessageText:
//
// HCD: A PID check has failed
//


// SymbolicName=USBIO_ERR_PID_CHECK_FAILURE
//
// MessageId: 0xE0000007L (No symbolic name defined)
//
// MessageText:
//
// HCD: An unexpected PID has been detected
//


// SymbolicName=USBIO_ERR_UNEXPECTED_PID
//
// MessageId: 0xE0000008L (No symbolic name defined)
//
// MessageText:
//
// HCD: A data overrun error has been detected
//


// SymbolicName=USBIO_ERR_DATA_OVERRUN
//
// MessageId: 0xE0000009L (No symbolic name defined)
//
// MessageText:
//
// HCD: A data underrun error has been detected
//


// SymbolicName=USBIO_ERR_DATA_UNDERRUN
//
// MessageId: 0xE000000AL (No symbolic name defined)
//
// MessageText:
//
// HCD: Reserved 1
//


// SymbolicName=USBIO_ERR_RESERVED1
//
// MessageId: 0xE000000BL (No symbolic name defined)
//
// MessageText:
//
// HCD: Reserved 2
//


// SymbolicName=USBIO_ERR_RESERVED2
//
// MessageId: 0xE000000CL (No symbolic name defined)
//
// MessageText:
//
// HCD: A buffer overrun has been detected
//


// SymbolicName=USBIO_ERR_BUFFER_OVERRUN
//
// MessageId: 0xE000000DL (No symbolic name defined)
//
// MessageText:
//
// HCD: A buffer underrun has been detected
//


// SymbolicName=USBIO_ERR_BUFFER_UNDERRUN
//
// MessageId: 0xE000000FL (No symbolic name defined)
//
// MessageText:
//
// HCD: A data buffer was not accessed. An isochronous data buffer was scheduled too late. The specified frame number does not match the actual frame number
//


// SymbolicName=USBIO_ERR_NOT_ACCESSED
//
// MessageId: 0xE0000010L (No symbolic name defined)
//
// MessageText:
//
// HCD: A FIFO error has been detected. The PCI bus latency was too long
//


// SymbolicName=USBIO_ERR_FIFO
//
// MessageId: 0xE0000011L (No symbolic name defined)
//
// MessageText:
//
// HCD: A XACT error has been detected
//


// SymbolicName=USBIO_ERR_XACT_ERROR
//
// MessageId: 0xE0000012L (No symbolic name defined)
//
// MessageText:
//
// HCD: A device is babbling. The data transfer phase exceeds the USB frame length
//


// SymbolicName=USBIO_ERR_BABBLE_DETECTED
//
// MessageId: 0xE0000013L (No symbolic name defined)
//
// MessageText:
//
// HCD: A data buffer error has been detected
//


// SymbolicName=USBIO_ERR_DATA_BUFFER_ERROR
//
// MessageId: 0xE0000030L (No symbolic name defined)
//
// MessageText:
//
// USBD: The endpoint has been halted
//


// SymbolicName=USBIO_ERR_ENDPOINT_HALTED
//
// MessageId: 0xE0000100L (No symbolic name defined)
//
// MessageText:
//
// USBD: A memory allocation attempt has failed
//


// SymbolicName=USBIO_ERR_NO_MEMORY
//
// MessageId: 0xE0000200L (No symbolic name defined)
//
// MessageText:
//
// USBD: An invalid URB function code has been passed
//


// SymbolicName=USBIO_ERR_INVALID_URB_FUNCTION
//
// MessageId: 0xE0000300L (No symbolic name defined)
//
// MessageText:
//
// USBD: An invalid parameter has been passed
//


// SymbolicName=USBIO_ERR_INVALID_PARAMETER
//
// MessageId: 0xE0000400L (No symbolic name defined)
//
// MessageText:
//
// USBD: There are data transfer requests pending for the device
//


// SymbolicName=USBIO_ERR_ERROR_BUSY
//
// MessageId: 0xE0000500L (No symbolic name defined)
//
// MessageText:
//
// USBD: A request has failed
//


// SymbolicName=USBIO_ERR_REQUEST_FAILED
//
// MessageId: 0xE0000600L (No symbolic name defined)
//
// MessageText:
//
// USBD: An invalid pipe handle has been passed
//


// SymbolicName=USBIO_ERR_INVALID_PIPE_HANDLE
//
// MessageId: 0xE0000700L (No symbolic name defined)
//
// MessageText:
//
// USBD: There is not enough bandwidth available
//


// SymbolicName=USBIO_ERR_NO_BANDWIDTH
//
// MessageId: 0xE0000800L (No symbolic name defined)
//
// MessageText:
//
// USBD: An internal host controller error has been detected
//


// SymbolicName=USBIO_ERR_INTERNAL_HC_ERROR
//
// MessageId: 0xE0000900L (No symbolic name defined)
//
// MessageText:
//
// USBD: A short transfer has been detected
//


// SymbolicName=USBIO_ERR_ERROR_SHORT_TRANSFER
//
// MessageId: 0xE0000A00L (No symbolic name defined)
//
// MessageText:
//
// USBD: A bad start frame has been specified
//


// SymbolicName=USBIO_ERR_BAD_START_FRAME
//
// MessageId: 0xE0000B00L (No symbolic name defined)
//
// MessageText:
//
// USBD: An isochronous request has failed
//


// SymbolicName=USBIO_ERR_ISOCH_REQUEST_FAILED
//
// MessageId: 0xE0000C00L (No symbolic name defined)
//
// MessageText:
//
// USBD: The USB frame control is currently owned
//


// SymbolicName=USBIO_ERR_FRAME_CONTROL_OWNED
//
// MessageId: 0xE0000D00L (No symbolic name defined)
//
// MessageText:
//
// USBD: The USB frame control is currently not owned
//


// SymbolicName=USBIO_ERR_FRAME_CONTROL_NOT_OWNED
//
// MessageId: 0xE0000E00L (No symbolic name defined)
//
// MessageText:
//
// USBD: The operation is not supported
//


// SymbolicName=USBIO_ERR_NOT_SUPPORTED
//
// MessageId: 0xE0000F00L (No symbolic name defined)
//
// MessageText:
//
// USBD: An invalid configuration descriptor was reported by the device
//


// SymbolicName=USBIO_ERR_INVALID_CONFIGURATION_DESCRIPTOR
//
// MessageId: 0xE8001000L (No symbolic name defined)
//
// MessageText:
//
// USBD: There are not enough resources available to complete the operation
//


// SymbolicName=USBIO_ERR_INSUFFICIENT_RESOURCES
//
// MessageId: 0xE0002000L (No symbolic name defined)
//
// MessageText:
//
// USBD: The set configuration request has failed
//


// SymbolicName=USBIO_ERR_SET_CONFIG_FAILED
//
// MessageId: 0xE0003000L (No symbolic name defined)
//
// MessageText:
//
// USBD: The buffer is too small.
//


// SymbolicName=USBIO_ERR_USBD_BUFFER_TOO_SMALL
//
// MessageId: 0xE0004000L (No symbolic name defined)
//
// MessageText:
//
// USBD: The interface was not found
//


// SymbolicName=USBIO_ERR_USBD_INTERFACE_NOT_FOUND
//
// MessageId: 0xE0005000L (No symbolic name defined)
//
// MessageText:
//
// USBD: Invalid pipe flags have been specified
//


// SymbolicName=USBIO_ERR_INVALID_PIPE_FLAGS
//
// MessageId: 0xE0006000L (No symbolic name defined)
//
// MessageText:
//
// USBD: The operation has been timed out
//


// SymbolicName=USBIO_ERR_USBD_TIMEOUT
//
// MessageId: 0xE0007000L (No symbolic name defined)
//
// MessageText:
//
// USBD: Device is gone
//


// SymbolicName=USBIO_ERR_DEVICE_GONE
//
// MessageId: 0xE0008000L (No symbolic name defined)
//
// MessageText:
//
// USBD: Status not mapped
//


// SymbolicName=USBIO_ERR_STATUS_NOT_MAPPED
//
// MessageId: 0xE0010000L (No symbolic name defined)
//
// MessageText:
//
// USBD: The operation has been cancelled
//


// SymbolicName=USBIO_ERR_CANCELED
//
// MessageId: 0xE0020000L (No symbolic name defined)
//
// MessageText:
//
// USBD: The isochronous data buffer was not accessed by the USB host controller
//


// SymbolicName=USBIO_ERR_ISO_NOT_ACCESSED_BY_HW
//
// MessageId: 0xE0030000L (No symbolic name defined)
//
// MessageText:
//
// USBD: The USB host controller reported an error in a transfer descriptor
//


// SymbolicName=USBIO_ERR_ISO_TD_ERROR
//
// MessageId: 0xE0040000L (No symbolic name defined)
//
// MessageText:
//
// USBD: An isochronous data packet was submitted in time but failed to reach the USB host controller in time
//


// SymbolicName=USBIO_ERR_ISO_NA_LATE_USBPORT
//
// MessageId: 0xE0050000L (No symbolic name defined)
//
// MessageText:
//
// USBD: An isochronous data packet was submitted too late
//


// SymbolicName=USBIO_ERR_ISO_NOT_ACCESSED_LATE
//
// MessageId: 0xE0001000L (No symbolic name defined)
//
// MessageText:
//
// USBIO: The operation has failed
//


// SymbolicName=USBIO_ERR_FAILED
//
// MessageId: 0xE0001001L (No symbolic name defined)
//
// MessageText:
//
// USBIO: An invalid input buffer has been passed to an IOCTL operation
//


// SymbolicName=USBIO_ERR_INVALID_INBUFFER
//
// MessageId: 0xE0001002L (No symbolic name defined)
//
// MessageText:
//
// USBIO: An invalid output buffer has been passed to an IOCTL operation
//


// SymbolicName=USBIO_ERR_INVALID_OUTBUFFER
//
// MessageId: 0xE0001003L (No symbolic name defined)
//
// MessageText:
//
// USBIO: There is not enough system memory available to complete the operation
//


// SymbolicName=USBIO_ERR_OUT_OF_MEMORY
//
// MessageId: 0xE0001004L (No symbolic name defined)
//
// MessageText:
//
// USBIO: There are read or write requests pending
//


// SymbolicName=USBIO_ERR_PENDING_REQUESTS
//
// MessageId: 0xE0001005L (No symbolic name defined)
//
// MessageText:
//
// USBIO: The USB device is already configured
//


// SymbolicName=USBIO_ERR_ALREADY_CONFIGURED
//
// MessageId: 0xE0001006L (No symbolic name defined)
//
// MessageText:
//
// USBIO: The USB device is not configured
//


// SymbolicName=USBIO_ERR_NOT_CONFIGURED
//
// MessageId: 0xE0001007L (No symbolic name defined)
//
// MessageText:
//
// USBIO: There are open pipes
//


// SymbolicName=USBIO_ERR_OPEN_PIPES
//
// MessageId: 0xE0001008L (No symbolic name defined)
//
// MessageText:
//
// USBIO: Either the handle is already bound to a pipe or the specified pipe is already bound to another handle
//


// SymbolicName=USBIO_ERR_ALREADY_BOUND
//
// MessageId: 0xE0001009L (No symbolic name defined)
//
// MessageText:
//
// USBIO: The handle is not bound to a pipe
//


// SymbolicName=USBIO_ERR_NOT_BOUND
//
// MessageId: 0xE000100AL (No symbolic name defined)
//
// MessageText:
//
// USBIO: The USB device has been removed from the system
//


// SymbolicName=USBIO_ERR_DEVICE_NOT_PRESENT
//
// MessageId: 0xE000100BL (No symbolic name defined)
//
// MessageText:
//
// USBIO: The specified control code is not supported
//


// SymbolicName=USBIO_ERR_CONTROL_NOT_SUPPORTED
//
// MessageId: 0xE000100CL (No symbolic name defined)
//
// MessageText:
//
// USBIO: The operation has been timed out
//


// SymbolicName=USBIO_ERR_TIMEOUT
//
// MessageId: 0xE000100DL (No symbolic name defined)
//
// MessageText:
//
// USBIO: An invalid recipient has been specified
//


// SymbolicName=USBIO_ERR_INVALID_RECIPIENT
//
// MessageId: 0xE000100EL (No symbolic name defined)
//
// MessageText:
//
// USBIO: Either an invalid request type has been specified or the operation is not supported by that pipe type
//


// SymbolicName=USBIO_ERR_INVALID_TYPE
//
// MessageId: 0xE000100FL (No symbolic name defined)
//
// MessageText:
//
// USBIO: An invalid IOCTL code has been specified
//


// SymbolicName=USBIO_ERR_INVALID_IOCTL
//
// MessageId: 0xE0001010L (No symbolic name defined)
//
// MessageText:
//
// USBIO: The direction of the data transfer request is not supported by that pipe
//


// SymbolicName=USBIO_ERR_INVALID_DIRECTION
//
// MessageId: 0xE0001011L (No symbolic name defined)
//
// MessageText:
//
// USBIO: The number of isochronous data packets specified in an isochronous read or write request exceeds the maximum number of packets supported by the USBIO driver
//


// SymbolicName=USBIO_ERR_TOO_MUCH_ISO_PACKETS
//
// MessageId: 0xE0001012L (No symbolic name defined)
//
// MessageText:
//
// USBIO: The memory resources are exhausted
//


// SymbolicName=USBIO_ERR_POOL_EMPTY
//
// MessageId: 0xE0001013L (No symbolic name defined)
//
// MessageText:
//
// USBIO: The specified pipe was not found in the current configuration
//


// SymbolicName=USBIO_ERR_PIPE_NOT_FOUND
//
// MessageId: 0xE0001014L (No symbolic name defined)
//
// MessageText:
//
// USBIO: An invalid isochronous data packet has been specified
//


// SymbolicName=USBIO_ERR_INVALID_ISO_PACKET
//
// MessageId: 0xE0001015L (No symbolic name defined)
//
// MessageText:
//
// USBIO: There are not enough system resources to complete the operation
//


// SymbolicName=USBIO_ERR_OUT_OF_ADDRESS_SPACE
//
// MessageId: 0xE0001016L (No symbolic name defined)
//
// MessageText:
//
// USBIO: The specified interface was not found in the current configuration or in the configuration descriptor
//


// SymbolicName=USBIO_ERR_INTERFACE_NOT_FOUND
//
// MessageId: 0xE0001017L (No symbolic name defined)
//
// MessageText:
//
// USBIO: The operation cannot be executed while the USB device is in the current state
//


// SymbolicName=USBIO_ERR_INVALID_DEVICE_STATE
//
// MessageId: 0xE0001018L (No symbolic name defined)
//
// MessageText:
//
// USBIO: An invalid parameter has been specified with an IOCTL operation
//


// SymbolicName=USBIO_ERR_INVALID_PARAM
//
// MessageId: 0xE0001019L (No symbolic name defined)
//
// MessageText:
//
// USBIO: The evaluation interval of the USBIO DEMO version has expired
//


// SymbolicName=USBIO_ERR_DEMO_EXPIRED
//
// MessageId: 0xE000101AL (No symbolic name defined)
//
// MessageText:
//
// USBIO: An invalid power state has been specified
//


// SymbolicName=USBIO_ERR_INVALID_POWER_STATE
//
// MessageId: 0xE000101BL (No symbolic name defined)
//
// MessageText:
//
// USBIO: The device has entered a power down state
//


// SymbolicName=USBIO_ERR_POWER_DOWN
//
// MessageId: 0xE000101CL (No symbolic name defined)
//
// MessageText:
//
// USBIO: The API version reported by the USBIO driver does not match the expected version
//


// SymbolicName=USBIO_ERR_VERSION_MISMATCH
//
// MessageId: 0xE000101DL (No symbolic name defined)
//
// MessageText:
//
// USBIO: The set configuration operation has failed
//


// SymbolicName=USBIO_ERR_SET_CONFIGURATION_FAILED
//
// MessageId: 0xE000101EL (No symbolic name defined)
//
// MessageText:
//
// An additional event object that has been passed to a function, was signalled
//


// SymbolicName=USBIO_ERR_ADDITIONAL_EVENT_SIGNALLED
//
// MessageId: 0xE000101FL (No symbolic name defined)
//
// MessageText:
//
// USBIO: This operation is not allowed for this process.
//


// SymbolicName=USBIO_ERR_INVALID_PROCESS
//
// MessageId: 0xE0001020L (No symbolic name defined)
//
// MessageText:
//
// USBIO: The device is acquired by a different process for exclusive usage
//


// SymbolicName=USBIO_ERR_DEVICE_ACQUIRED
//
// MessageId: 0xE0001021L (No symbolic name defined)
//
// MessageText:
//
// USBIO: The device is opened by a different process.
//


// SymbolicName=USBIO_ERR_DEVICE_OPENED
 /*
  TNMLIB DEVICE specific error codes
 */
#define TNMLIB_DEVICE_ERROR_BASE  ((DWORD)0xEF000000L)
#define TNMLIB_ERROR_BASE      ((DWORD)0xEE000000L)
//
// MessageId: TNMLIB_DEVICE_ERR_PROTO_NOTIMPLEMENTED
//
// MessageText:
//
// SSD: Operation not implemented
//
#define TNMLIB_DEVICE_ERR_PROTO_NOTIMPLEMENTED ((DWORD)0xEF001000L)

//
// MessageId: TNMLIB_DEVICE_ERR_PROTO_INVALID_TAG
//
// MessageText:
//
// SSD: Invalid device tag
//
#define TNMLIB_DEVICE_ERR_PROTO_INVALID_TAG ((DWORD)0xEF001001L)

//
// MessageId: TNMLIB_DEVICE_ERR_PROTO_INVALID_CBW_LENGTH
//
// MessageText:
//
// SSD: Invalid length of control block
//
#define TNMLIB_DEVICE_ERR_PROTO_INVALID_CBW_LENGTH ((DWORD)0xEF001002L)

//
// MessageId: TNMLIB_DEVICE_ERR_PROTO_TRANSACTION_UNDERRUN
//
// MessageText:
//
// SSD: Transaction length is less than required
//
#define TNMLIB_DEVICE_ERR_PROTO_TRANSACTION_UNDERRUN ((DWORD)0xEF001003L)

//
// MessageId: TNMLIB_DEVICE_ERR_PROTO_TRANSACTION_OVERRUN
//
// MessageText:
//
// SSD: Transaction length is great than required
//
#define TNMLIB_DEVICE_ERR_PROTO_TRANSACTION_OVERRUN ((DWORD)0xEF001004L)

//
// MessageId: TNMLIB_DEVICE_ERR_PROTO_SYNC_FAILED
//
// MessageText:
//
// SSD: Request's synchronization failed
//
#define TNMLIB_DEVICE_ERR_PROTO_SYNC_FAILED ((DWORD)0xEF001005L)

//
// MessageId: TNMLIB_DEVICE_ERR_DEVICE_BUSY
//
// MessageText:
//
// SSD: Device is busy
//
#define TNMLIB_DEVICE_ERR_DEVICE_BUSY    ((DWORD)0xEF000001L)

//
// MessageId: TNMLIB_DEVICE_ERR_NOT_LOCKED
//
// MessageText:
//
// SSD: Specified request require exclusive access to device
//
#define TNMLIB_DEVICE_ERR_NOT_LOCKED     ((DWORD)0xEF000002L)

//
// MessageId: TNMLIB_DEVICE_ERR_ALREADY_LOCKED
//
// MessageText:
//
// SSD: Exclusive access to device has been already granted
//
#define TNMLIB_DEVICE_ERR_ALREADY_LOCKED ((DWORD)0xEF000003L)

//
// MessageId: TNMLIB_DEVICE_ERR_NO_SUCH_MEMORY_TYPE
//
// MessageText:
//
// SSD: No such type of memory
//
#define TNMLIB_DEVICE_ERR_NO_SUCH_MEMORY_TYPE ((DWORD)0xEF000004L)

//
// MessageId: TNMLIB_DEVICE_ERR_OFFSET_OUT_OF_RANGE
//
// MessageText:
//
// SSD: Offset out of range
//
#define TNMLIB_DEVICE_ERR_OFFSET_OUT_OF_RANGE ((DWORD)0xEF000005L)

//
// MessageId: TNMLIB_DEVICE_ERR_SIZE_OUT_OF_RANGE
//
// MessageText:
//
// SSD: Size out of range
//
#define TNMLIB_DEVICE_ERR_SIZE_OUT_OF_RANGE ((DWORD)0xEF000006L)

//
// MessageId: TNMLIB_DEVICE_ERR_FLASH_VERIFY_FAIL
//
// MessageText:
//
// SSD: FLASH memory block has defect(s)
//
#define TNMLIB_DEVICE_ERR_FLASH_VERIFY_FAIL ((DWORD)0xEF000007L)

//
// MessageId: TNMLIB_DEVICE_ERR_DEVICE_NOT_EMPTY
//
// MessageText:
//
// SSD: Device has record(s)
//
#define TNMLIB_DEVICE_ERR_DEVICE_NOT_EMPTY ((DWORD)0xEF000008L)

//
// MessageId: TNMLIB_DEVICE_ERR_NO_FREE_MEMORY
//
// MessageText:
//
// SSD: Device memory is full
//
#define TNMLIB_DEVICE_ERR_NO_FREE_MEMORY ((DWORD)0xEF000009L)

//
// MessageId: TNMLIB_DEVICE_ERR_NO_FREE_RECORD_ENTRY
//
// MessageText:
//
// SSD: No free entries in records table
//
#define TNMLIB_DEVICE_ERR_NO_FREE_RECORD_ENTRY ((DWORD)0xEF00000AL)

//
// MessageId: TNMLIB_DEVICE_ERR_NO_OPEN_RECORD
//
// MessageText:
//
// SSD: Record has not been started
//
#define TNMLIB_DEVICE_ERR_NO_OPEN_RECORD ((DWORD)0xEF00000BL)

//
// MessageId: TNMLIB_DEVICE_ERR_NO_SUCH_RECORD
//
// MessageText:
//
// SSD: No such record
//
#define TNMLIB_DEVICE_ERR_NO_SUCH_RECORD ((DWORD)0xEF00000CL)

//
// MessageId: TNMLIB_DEVICE_ERR_NOT_ALIGNED
//
// MessageText:
//
// SSD: Parameter has wrong alignment
//
#define TNMLIB_DEVICE_ERR_NOT_ALIGNED    ((DWORD)0xEF00000DL)

//
// MessageId: TNMLIB_DEVICE_ERR_NO_TASKS
//
// MessageText:
//
// SSD: Device has no tasks
//
#define TNMLIB_DEVICE_ERR_NO_TASKS       ((DWORD)0xEF00000EL)

//
// MessageId: TNMLIB_DEVICE_ERR_TASK_ID_MISMATCH
//
// MessageText:
//
// SSD: TaskID does not match to TaskID requested by Data unit
//
#define TNMLIB_DEVICE_ERR_TASK_ID_MISMATCH ((DWORD)0xEF00000FL)

//
// MessageId: TNMLIB_DEVICE_ERR_TASK_VERSION_SMALL
//
// MessageText:
//
// SSD: Task version mismatch
//
#define TNMLIB_DEVICE_ERR_TASK_VERSION_SMALL ((DWORD)0xEF000010L)

//
// MessageId: TNMLIB_DEVICE_ERR_SEND_TASK_TIMEOUT
//
// MessageText:
//
// SSD: Task sending has been timeout
//
#define TNMLIB_DEVICE_ERR_SEND_TASK_TIMEOUT ((DWORD)0xEF000011L)

//
// MessageId: TNMLIB_DEVICE_ERR_WAIT_ONWR_TIMEOUT
//
// MessageText:
//
// SSD: Waiting for "RECORD STARTED" event has been timeout
//
#define TNMLIB_DEVICE_ERR_WAIT_ONWR_TIMEOUT ((DWORD)0xEF000012L)

//
// MessageId: TNMLIB_DEVICE_ERR_WAIT_DUINFO_TIMEOUT
//
// MessageText:
//
// SSD: Waiting for DUINFO has been timeout
//
#define TNMLIB_DEVICE_ERR_WAIT_DUINFO_TIMEOUT ((DWORD)0xEF000013L)

//
// MessageId: TNMLIB_DEVICE_ERR_DUINFO_WRONG_CHECKSUM
//
// MessageText:
//
// SSD: Data unit ID packet has wrong checksum 
//
#define TNMLIB_DEVICE_ERR_DUINFO_WRONG_CHECKSUM ((DWORD)0xEF000014L)

//
// MessageId: TNMLIB_DEVICE_ERR_NO_SUCH_TASK
//
// MessageText:
//
// SSD: No such task
//
#define TNMLIB_DEVICE_ERR_NO_SUCH_TASK   ((DWORD)0xEF000015L)

//
// MessageId: TNMLIB_DEVICE_ERR_NOT_IN_CONTROL
//
// MessageText:
//
// SSD: Realtime control mode has not been started
//
#define TNMLIB_DEVICE_ERR_NOT_IN_CONTROL ((DWORD)0xEF000016L)

//
// MessageId: TNMLIB_DEVICE_ERR_FAKETASK_WRONG_HEADER_SIZE
//
// MessageText:
//
// SSD: Wrong header size of task for realtime control
//
#define TNMLIB_DEVICE_ERR_FAKETASK_WRONG_HEADER_SIZE ((DWORD)0xEF000017L)

//
// MessageId: TNMLIB_DEVICE_ERR_FAKETASK_WRONG_SIZE
//
// MessageText:
//
// SSD: Wrong size of task for realtime control
//
#define TNMLIB_DEVICE_ERR_FAKETASK_WRONG_SIZE ((DWORD)0xEF000018L)

//
// MessageId: TNMLIB_DEVICE_ERR_FAKETASK_WRONG_CHECKSUM
//
// MessageText:
//
// SSD: Wrong checksum of task for realtime control
//
#define TNMLIB_DEVICE_ERR_FAKETASK_WRONG_CHECKSUM ((DWORD)0xEF000019L)

//
// MessageId: TNMLIB_DEVICE_ERR_NOTSUPPORTED
//
// MessageText:
//
// SSD: Requested operation is not supported
//
#define TNMLIB_DEVICE_ERR_NOTSUPPORTED   ((DWORD)0xEF00001AL)

/*
 TNMLIB specific error codes
*/
//
// MessageId: TNMLIB_ERR_PROTO_INVALID_TAG
//
// MessageText:
//
// TNMLIB: Invalid device tag
//
#define TNMLIB_ERR_PROTO_INVALID_TAG     ((DWORD)0xEE000001L)

//
// MessageId: TNMLIB_ERR_PROTO_INVALID_CSW_LENGTH
//
// MessageText:
//
// TNMLIB: Invalid length of status block
//
#define TNMLIB_ERR_PROTO_INVALID_CSW_LENGTH ((DWORD)0xEE000002L)

//
// MessageId: TNMLIB_ERR_PROTO_OPERATION_MISMATCH
//
// MessageText:
//
// TNMLIB: Operation code in CBW and CSW does not match
//
#define TNMLIB_ERR_PROTO_OPERATION_MISMATCH ((DWORD)0xEE000003L)

//
// MessageId: TNMLIB_ERR_PROTO_CBW_SEND_FAILED
//
// MessageText:
//
// TNMLIB: Sending CBW to device failed
//
#define TNMLIB_ERR_PROTO_CBW_SEND_FAILED ((DWORD)0xEE000004L)

//
// MessageId: TNMLIB_ERR_PROTO_CSW_RECV_FAILED
//
// MessageText:
//
// TNMLIB: Receiving CSW from device failed
//
#define TNMLIB_ERR_PROTO_CSW_RECV_FAILED ((DWORD)0xEE000005L)

//
// MessageId: TNMLIB_ERR_PROTO_TRANSACTION_UNDERRUN
//
// MessageText:
//
// TNMLIB: Transaction size is less than requested
//
#define TNMLIB_ERR_PROTO_TRANSACTION_UNDERRUN ((DWORD)0xEE000006L)

//
// MessageId: TNMLIB_ERR_PROTO_TRANSACTION_OVERRUN
//
// MessageText:
//
// TNMLIB: Transaction size is great than requested
//
#define TNMLIB_ERR_PROTO_TRANSACTION_OVERRUN ((DWORD)0xEE000007L)

//
// MessageId: TNMLIB_ERR_REQUEST_PENDING
//
// MessageText:
//
// TNMLIB: Device is busy due to previous requests
//
#define TNMLIB_ERR_REQUEST_PENDING       ((DWORD)0xEE000008L)

//
// MessageId: TNMLIB_ERR_CANCEL_PENDING
//
// MessageText:
//
// TNMLIB: Cancelling is already pending
//
#define TNMLIB_ERR_CANCEL_PENDING        ((DWORD)0xEE000009L)

//
// MessageId: TNMLIB_ERR_VALUE_FROM_CACHE
//
// MessageText:
//
// TNMLIB: Operation result was obtained from cache
//
#define TNMLIB_ERR_VALUE_FROM_CACHE      ((DWORD)0xEE00000AL)

#endif // _TNMMSG_HEADER_
