#ifndef _tnmrec_header_
#define _tnmrec_header_

BOOL TNMLIB_API
TNMLIB_OpenDataRecord(
  TNMLIB_HANDLE hTND,
  LPTNMLIB_DEVICE_RECORD_PARAM  pDRP
  );

BOOL TNMLIB_API
TNMLIB_RawWriteDataRecord(
  TNMLIB_HANDLE hTND,
  LPTNMLIB_DEVICE_TIME  pTime,
  DWORD   dwBytesToWrite,
  LPVOID  pBuffer
  );

BOOL TNMLIB_API
TNMLIB_CloseDataRecord(
  TNMLIB_HANDLE hTND,
  LPTNMLIB_DEVICE_TIME  pTime
  );

#endif // _tnmrec_header_
