#include "stdafx.h"

#include "tnmdefs.h"
#include "tnmlib.h"

#include "i_devman.h"
#include "i_devrw.h"

#include "i_tnminfo.h"
#include "i_extpep.h"

#include "tnminfo.h"

BOOL TNMLIB_API
TNMLIB_GetVersionInfo(
  TNMLIB_HANDLE hTND, 
  LPTNMLIB_DEVICE_VERSION_INFO pDVI)
{
  _EXT_ENTRY

    fSuccess = _TND_GetVersionInfo(pDI, pDVI);

  _EXT_LEAVE
}

BOOL TNMLIB_API
TNMLIB_GetMemoryInfo(
  TNMLIB_HANDLE hTND,
  LPTNMLIB_DEVICE_MEMORY_INFO pDMI
  )
{
  _EXT_ENTRY

    fSuccess = _TND_GetMemoryInfo(pDI, pDMI);

  _EXT_LEAVE
}

BOOL TNMLIB_API
TNMLIB_GetMemoryExInfoArray(
  TNMLIB_HANDLE hTND,
  DWORD dwStartMemoryTypeIndex,
  DWORD dwNumberOfMemoryTypes,
  LPTNMLIB_DEVICE_MEMORY_EXINFO pDMEI)
{
  _EXT_ENTRY

    fSuccess = _TND_GetMemoryExInfoArray(
      pDI, 
      dwStartMemoryTypeIndex, 
      dwNumberOfMemoryTypes, 
      pDMEI
      );

  _EXT_LEAVE
}

BOOL TNMLIB_API
TNMLIB_GetRecordsInfoArray(
  TNMLIB_HANDLE hTND,
  DWORD dwStartRecordIndex,
  DWORD dwNumberOfRecords,
  LPTNMLIB_DEVICE_RECORD_INFO pRI)
{
  _EXT_ENTRY

    fSuccess = _TND_GetRecordsInfoArray(pDI, dwStartRecordIndex, dwNumberOfRecords, pRI);

  _EXT_LEAVE
}

BOOL TNMLIB_API
TNMLIB_GetRecordsModulesMapArray(
  TNMLIB_HANDLE hTND,
  DWORD dwStartRecordIndex,
  DWORD dwNumberOfRecords,
  LPTNMLIB_RECORD_MODULES_MAP pRMM)
{
  _EXT_ENTRY

    fSuccess = _TND_NC_GetRecordsModulesMapArray(pDI, dwStartRecordIndex, dwNumberOfRecords, pRMM);

  _EXT_LEAVE
}

BOOL TNMLIB_API
  TNMLIB_GetTasksInfoArray(
  TNMLIB_HANDLE hTND, 
  DWORD dwStartTaskIndex,
  DWORD dwNumberOfTasks,
  LPTNMLIB_DEVICE_TASK_INFO pTI)
{
  _EXT_ENTRY

    fSuccess = _TND_GetTasksInfoArray(pDI, dwStartTaskIndex, dwNumberOfTasks, pTI);

  _EXT_LEAVE
}

BOOL TNMLIB_API
TNMLIB_GetUTObjectSize(
  TNMLIB_HANDLE hTND,
  DWORD         dwObjectIndex,
  DWORD*        pdwObjectSize
)
{
  _EXT_ENTRY

    fSuccess = _TND_GetUTInfoObjectSize(pDI, dwObjectIndex, pdwObjectSize);

  _EXT_LEAVE
}

BOOL TNMLIB_API
TNMLIB_GetUTObject(
  TNMLIB_HANDLE hTND,
  DWORD         dwObjectIndex,
  void*         pObject,
  DWORD         dwObjectSize
  )
{
  _EXT_ENTRY

    fSuccess = _TND_GetUTInfoObject(pDI, dwObjectIndex, pObject, dwObjectSize);

  _EXT_LEAVE
}
