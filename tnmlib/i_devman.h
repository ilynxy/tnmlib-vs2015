#pragma once

#include <winusb.h>
#include "i_cache.h"

#define TNMLIB_DEVICE_DEFAULT_TIMEOUT         1000
#define TNMLIB_DEVICE_IOCTL_TIMEOUT           500
#define TNMLIB_DEVICE_CONFIGURATION_INDEX     0
#define TNMLIB_DEVICE_MAXIMUM_TRANSFER_SIZE   65536

#define TNMLIB_DEVICE_CMDOUT_ENDPOINT         0x02
#define TNMLIB_DEVICE_CMDIN_ENDPOINT          0x86
#define TNMLIB_DEVICE_CTLIN_ENDPOINT          0x88

#define TNMLIB_DEVICE_TAG   ((DWORD)0x54555342L)

typedef enum _TNMLIB_BACKEND
{
  TNMLIB_USBIO_BACKEND,
  TNMLIB_WINUSB_BACKEND
} TNMLIB_BACKEND;

#define IS_WINUSB_BACKEND(pDI)  (pDI->wihDevice != NULL)
#define IS_USBIO_BACKEND(pDI)   (pDI->wihDevice == NULL)

#include <PshPack1.h>

typedef struct  _TNMLIB_DEVICE_CBW
{
  DWORD   dwTag;
  WORD    wOperation;
  WORD    wcbLength;
  DWORD   dwArgs[6];
} TNMLIB_DEVICE_CBW, *LPTNMLIB_DEVICE_CBW;

typedef struct  _TNMLIB_DEVICE_CSW 
{
  DWORD   dwTag;
  WORD    wOperation;
  WORD    wcbLength;
  DWORD   dwTransactionSize;
  DWORD   dwStatus;
} TNMLIB_DEVICE_CSW, *LPTNMLIB_DEVICE_CSW;

#include <PopPack.h>

typedef struct _TNMLIB_DEVICE_PIPE
{
  HANDLE    hFileHandle;
  DWORD     dwMaximumPacketSize;
  DWORD     dwMaximumTransferSize;
} TNMLIB_DEVICE_PIPE, *LPTNMLIB_DEVICE_PIPE;

typedef struct _TNMLIB_DEVICE_INSTANCE 
{
  LPTSTR            lpszDevicePath;

  HANDLE            hDevice;
  WINUSB_INTERFACE_HANDLE wihDevice;

  OVERLAPPED        Overlapped;

  HANDLE            hMutex;

  HANDLE            hIdleEvent;

  CRITICAL_SECTION  csBreakLock;
  HANDLE            hBreakEvent;
  BOOL              bIOIgnoreBreakEvent;
  
  DWORD             dwTimeOut;

  DWORD             dwMaxTransferSize;

  HANDLE            hInPipe;
  DWORD             dwInMaximumPacketSize;
  
  HANDLE            hOutPipe;
  DWORD             dwOutMaximumPacketSize;

  HANDLE            hCtlPipe;
  DWORD             dwCtlMaximumPacketSize;

  TNMLIB_DEVICE_CBW  cbw;
  TNMLIB_DEVICE_CSW  csw;

  LPTNMLIB_PROGRESSPROC  lpProgressProc;

  TNMLIB_DEVICE_CACHE  cache;

} TNMLIB_DEVICE_INSTANCE, *LPTNMLIB_DEVICE_INSTANCE;

LPTNMLIB_DEVICE_INSTANCE
_CreateDeviceInstanceByPath(LPCTSTR  lpszDevicePath, TNMLIB_BACKEND backend);

BOOL
_DeleteDeviceInstance(LPTNMLIB_DEVICE_INSTANCE pDI);

TNMLIB_HANDLE
_HMCreateHandle(LPTNMLIB_DEVICE_INSTANCE pDI);

LPTNMLIB_DEVICE_INSTANCE
_HMGetInstanceByHandle(TNMLIB_HANDLE hTND, size_t *pIndex);

LPTNMLIB_DEVICE_INSTANCE
_HMDeleteHandle(TNMLIB_HANDLE hTND);

BOOL
_AsyncResetDevice(LPTNMLIB_DEVICE_INSTANCE pDI);
