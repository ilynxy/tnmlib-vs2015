#pragma once

#define _CACHE_VERSION_INFO   (0x00000001)
#define _CACHE_MEMORY_INFO    (0x00000002)
#define _CACHE_MEMORY_EXINFO  (0x00000004)
#define _CACHE_RECORDS_INFO   (0x00000008)
#define _CACHE_TASKS_INFO     (0x00000010)

#define _CACHE_VAR_EXINFO_INDEX    0
#define _CACHE_VAR_RECORDS_INDEX   1
#define _CACHE_VAR_TASKS_INDEX     2

#define _CACHE_VAR_COUNTS          3

typedef struct _TNMLIB_VARSIZE_CACHE
{
  size_t  uiSize;
  LPVOID  pData;
} TNMLIB_VARSIZE_CACHE, *LPTNMLIB_VARSIZE_CACHE;

typedef struct _TNMLIB_DEVICE_CACHE
{
  DWORD                       bmValid;

  TNMLIB_DEVICE_VERSION_INFO  VI;
  TNMLIB_DEVICE_MEMORY_INFO   MI;

  TNMLIB_VARSIZE_CACHE        pVSC[_CACHE_VAR_COUNTS];

} TNMLIB_DEVICE_CACHE, *LPTNMLIB_DEVICE_CACHE;

void
_InitializeCache(LPTNMLIB_DEVICE_CACHE pDC);

void
_DeleteCache(LPTNMLIB_DEVICE_CACHE pDC);

LPVOID
_AllocVarCache(LPTNMLIB_DEVICE_CACHE pDC, size_t uiIdx, size_t uiSize);

LPVOID
_GetVarCache(LPTNMLIB_DEVICE_CACHE pDC, size_t uiIdx);


#define _IsCacheValid(pDC, dwType)          ((pDC)->bmValid & (dwType))
#define _MarkCacheAsValid(pDC, dwType)      (pDC)->bmValid |= (dwType)
#define _MarkCacheAsInvalid(pDC, dwType)    (pDC)->bmValid &= ~(dwType)
#define _MarkAllCacheAsInvalid(pDC)         (pDC)->bmValid = 0
