#include "stdafx.h"

#include "tnmdefs.h"
#include "tnmlib.h"

#include "i_devman.h"
#include "i_devrw.h"

#include "tnmrec.h"

#include "i_devcmd.h"
#include "i_extpep.h"

#include "i_recrw.h"

BOOL TNMLIB_API
TNMLIB_OpenDataRecord(
  TNMLIB_HANDLE hTND,
  LPTNMLIB_DEVICE_RECORD_PARAM  pDRP
  )
{
  _EXT_ENTRY

    TNMLIB_DEVICE_RECORD_PARAM  rp;
    if (pDRP == NULL)
    {
      _GetCurrentDateTime(&rp.rsTime, &rp.rsDate);
      // rp.wTaskNumber = (0xFFFE); // TODO: Set real value
      rp.wTaskNumber = 0;
      rp.wFlyNumber  = 0;

      rp.wDataUnitSerial  = 0; // TODO: Set real value
      rp.wDataUnitVersion = 0; // TODO: Set real value

      rp.dwUReserved = 0;

      pDRP = &rp;
    }

    pDI->cbw.wOperation = TNMLIB_DEVICE_COMMAND_OPEN_PC_RECORD;
    pDI->cbw.dwArgs[0] = pDRP->rsTime.dwUTime;
    pDI->cbw.dwArgs[1] = pDRP->rsDate.dwUDate;
    pDI->cbw.dwArgs[2] = pDRP->dwUTaskFly;
    pDI->cbw.dwArgs[3] = pDRP->dwUSerialVersion;
    pDI->cbw.dwArgs[4] = pDRP->dwUReserved;

    fSuccess = _DeviceCBWAndDataStage(pDI, _CBW_DATASTAGE_NONE, NULL, 0, 0);
    if (fSuccess)
      _MarkAllCacheAsInvalid(&pDI->cache);

  _EXT_LEAVE
}

BOOL TNMLIB_API
TNMLIB_RawWriteDataRecord(
  TNMLIB_HANDLE hTND,
  LPTNMLIB_DEVICE_TIME  pTime,
  DWORD   dwBytesToWrite,
  LPVOID  pBuffer
  )
{
  _EXT_ENTRY

    TNMLIB_DEVICE_TIME tm;
    if (pTime == NULL)
    {
      _GetCurrentDateTime(&tm, NULL);
      pTime = &tm;
    }

    pDI->cbw.wOperation = TNMLIB_DEVICE_COMMAND_WRITE_PC_DATA;
    pDI->cbw.dwArgs[0] = dwBytesToWrite;
    pDI->cbw.dwArgs[1] = pTime->dwUTime;

    fSuccess = _DeviceCBWAndDataStage(pDI, _CBW_DATASTAGE_WRITE, 
      pBuffer, dwBytesToWrite, 1);

  _EXT_LEAVE
}

BOOL TNMLIB_API
TNMLIB_CloseDataRecord(
  TNMLIB_HANDLE hTND,
  LPTNMLIB_DEVICE_TIME  pTime
  )
{
  _EXT_ENTRY

    TNMLIB_DEVICE_TIME tm;
    if (pTime == NULL)
    {
      _GetCurrentDateTime(&tm, NULL);
      pTime = &tm;
    }

    pDI->cbw.wOperation = TNMLIB_DEVICE_COMMAND_CLOSE_PC_RECORD;
    pDI->cbw.dwArgs[0] = pTime->dwUTime;

    fSuccess = _DeviceCBWAndDataStage(pDI, _CBW_DATASTAGE_VALIDATE, NULL, 0, 0);

  _EXT_LEAVE
}
