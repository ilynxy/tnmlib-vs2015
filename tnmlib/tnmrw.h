#ifndef _tnmrw_header_
#define _tnmrw_header_

BOOL TNMLIB_API
TNMLIB_ReadRecord(
  TNMLIB_HANDLE hTND,
  DWORD    dwIndex,
  DWORD64  ldwOffsetBytes,
  DWORD64  ldwBytesToRead,
  LPVOID  pBuffer
  );

BOOL TNMLIB_API
TNMLIB_ReadMemory(
  TNMLIB_HANDLE hTND,
  DWORD   dwMemoryType,
  DWORD64 ldwOffsetBytes,
  DWORD64 ldwBytesToRead,
  LPVOID  pBuffer
  );

BOOL TNMLIB_API
TNMLIB_WriteMemory(
  TNMLIB_HANDLE hTND,
  DWORD   dwMemoryType,
  DWORD64 ldwOffsetBytes,
  DWORD64 ldwBytesToWrite,
  LPCVOID  pBuffer
  );

BOOL TNMLIB_API
TNMLIB_RawReadRecord(
  TNMLIB_HANDLE hTND,
  DWORD   dwIndex,
  DWORD   dwOffsetPages,
  DWORD   dwCountPages,
  LPVOID  pBuffer
  );

BOOL TNMLIB_API
TNMLIB_RawReadMemory(
  TNMLIB_HANDLE hTND,
  DWORD  dwMemoryType,
  DWORD  dwOffsetPages,
  DWORD  dwCountPages,
  LPVOID  pBuffer
  );

BOOL TNMLIB_API
TNMLIB_RawWriteMemory(
  TNMLIB_HANDLE hTND,
  DWORD  dwMemoryType,
  DWORD  dwOffsetPages,
  DWORD  dwCountPages,
  LPVOID  pBuffer
  );

BOOL TNMLIB_API
TNMLIB_DeleteAllRecords(TNMLIB_HANDLE hTND);

BOOL TNMLIB_API
TNMLIB_VerifyBlock(TNMLIB_HANDLE hTND, DWORD dwBlock);

BOOL TNMLIB_API
TNMLIB_ReadRecordsTableSnapshot(
  TNMLIB_HANDLE hTND,
  LPTNMLIB_DEVICE_RECORDS_TABLE_SNAPSHOT  pRTS
);
BOOL TNMLIB_API
TNMLIB_WriteRecordsTableSnapshot(
  TNMLIB_HANDLE hTND,
  LPTNMLIB_DEVICE_RECORDS_TABLE_SNAPSHOT  pRTS
);

#endif
