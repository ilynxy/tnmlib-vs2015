#ifndef _i_extpep_header_
#define _i_extpep_header_

#define _EXT_ENTRY \
  BOOL fSuccess = FALSE; \
  LPTNMLIB_DEVICE_INSTANCE pDI; \
  DBG_ENTRY; \
  pDI = _GetDeviceInstanceAndLock(hTND); \
  if (pDI != NULL) \
  {

#define _EXT_LEAVE \
    _ReleaseDeviceInstance(pDI); \
  } \
  DBG_LEAVE(fSuccess); \
  return fSuccess;


#endif
