#ifndef _tnminfo_header_
#define _tnminfo_header_

BOOL TNMLIB_API
TNMLIB_GetVersionInfo(TNMLIB_HANDLE hTND, LPTNMLIB_DEVICE_VERSION_INFO pDVI);

BOOL TNMLIB_API
TNMLIB_GetMemoryInfo(TNMLIB_HANDLE hTND, LPTNMLIB_DEVICE_MEMORY_INFO pDMI);

BOOL TNMLIB_API
TNMLIB_GetMemoryExInfoArray(
  TNMLIB_HANDLE hTND,
  DWORD dwStartMemoryTypeIndex,
  DWORD dwNumberOfMemoryTypes,
  LPTNMLIB_DEVICE_MEMORY_EXINFO pDMEI);

BOOL TNMLIB_API
TNMLIB_GetRecordsInfoArray(
  TNMLIB_HANDLE hTND,
  DWORD dwStartRecordIndex,
  DWORD dwNumberOfRecords,
  LPTNMLIB_DEVICE_RECORD_INFO pRI);

BOOL TNMLIB_API
TNMLIB_GetRecordsModulesMapArray(
  TNMLIB_HANDLE hTND,
  DWORD dwStartRecordIndex,
  DWORD dwNumberOfRecords,
  LPTNMLIB_RECORD_MODULES_MAP pRMM);

BOOL TNMLIB_API
TNMLIB_GetTasksInfoArray(
  TNMLIB_HANDLE hTND, 
  DWORD dwStartTaskIndex,
  DWORD dwNumberOfTasks,
  LPTNMLIB_DEVICE_TASK_INFO pTI);

BOOL TNMLIB_API
TNMLIB_GetUTObjectSize(
  TNMLIB_HANDLE hTND,
  DWORD         dwObjectIndex,
  DWORD*        pdwObjectSize
  );

BOOL TNMLIB_API
TNMLIB_GetUTObject(
  TNMLIB_HANDLE hTND,
  DWORD         dwObjectIndex,
  void*         pObject,
  DWORD         dwObjectSize
  );

#endif // _tnminfo_header_
