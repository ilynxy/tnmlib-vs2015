#include "stdafx.h"

#include "tnmdefs.h"
#include "tnmlib.h"
#include "tnmmsg.h"

#include "i_devman.h"
#include "i_devrw.h"

#include "i_tnminfo.h"
#include "i_memrw.h"

#include "i_devcmd.h"

static const TNMLIB_DEVICE_MEMORY_EXINFO*
_TND_GetCachedMEI(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  DWORD dwMemoryTypeIndex
  )
{
  BOOL fSuccess = FALSE;
  const TNMLIB_DEVICE_MEMORY_EXINFO* c_pDMEI = NULL;

  DBG_ENTRY;

  {

    // Update cache and validate param
    fSuccess = _TND_GetMemoryExInfoArray(pDI, dwMemoryTypeIndex, 1, NULL);
    if (!fSuccess)
      goto _cleanup;

    // Get value from cache
    c_pDMEI = _GetVarCache(&pDI->cache, _CACHE_VAR_EXINFO_INDEX);
    if (c_pDMEI == NULL)
    {
      fSuccess = FALSE;
      goto _cleanup;
    }

    c_pDMEI += dwMemoryTypeIndex;

  }

_cleanup:
  DBG_LEAVE(c_pDMEI);
  return c_pDMEI;
}

static const TNMLIB_DEVICE_RECORD_INFO*
_TND_GetCachedRI(
  LPTNMLIB_DEVICE_INSTANCE pDI, 
  DWORD dwRecordIndex)
{
  BOOL fSuccess = FALSE;
  const TNMLIB_DEVICE_RECORD_INFO* c_pRI = NULL;

  DBG_ENTRY;

  {
    // Update cache and validate param
    fSuccess = _TND_GetRecordsInfoArray(pDI, dwRecordIndex, 1, NULL);
    if (!fSuccess)
      goto _cleanup;

    // Get value from cache
    c_pRI = _GetVarCache(&pDI->cache, _CACHE_VAR_RECORDS_INDEX);
    if (c_pRI == NULL)
    {
      fSuccess = FALSE;
      goto _cleanup;
    }

    c_pRI += dwRecordIndex;

  }

_cleanup:
  DBG_LEAVE(c_pRI);
  return c_pRI;
}

static BOOL
_CheckRange(
  DWORD64 ldwTransferByteOffset,
  DWORD64 ldwTransferByteLength,
  DWORD64 ldwTotalMemoryByteSize
  )
{
  BOOL fSuccess = FALSE;

  {
    if (ldwTotalMemoryByteSize == 0)
    {
      SetLastError(TNMLIB_DEVICE_ERR_NOTSUPPORTED);
      goto _cleanup;
    }

    if (ldwTransferByteOffset >= ldwTotalMemoryByteSize)
    {
      SetLastError(TNMLIB_DEVICE_ERR_OFFSET_OUT_OF_RANGE);
      goto _cleanup;
    }

    if ((ldwTransferByteOffset + ldwTransferByteLength) > ldwTotalMemoryByteSize)
    {
      SetLastError(TNMLIB_DEVICE_ERR_SIZE_OUT_OF_RANGE);
      goto _cleanup;
    }

    fSuccess = TRUE;

  }

_cleanup:
  return fSuccess;
}

static BOOL
_GetPagedRequestArgs(
  DWORD64 ldwTransferByteOffset,
  DWORD64 ldwTransferByteLength,
  DWORD   dwPageByteLength,

  LPDWORD   lpdwTransferPageOffset,
  LPDWORD   lpdwTransferPageLength
)
{
  BOOL fSuccess = FALSE;
  DWORD64 ldwTransferEndByteOffset;
  DWORD dwOffset;
  DWORD dwLength;

  {
    if (dwPageByteLength == 0)
    {
      SetLastError(ERROR_INVALID_PARAMETER);
      goto _cleanup;
    }

    dwOffset = (DWORD)(ldwTransferByteOffset / dwPageByteLength);
    
    ldwTransferEndByteOffset = ldwTransferByteLength + ldwTransferByteOffset;

    dwLength = (DWORD)(ldwTransferEndByteOffset / dwPageByteLength) - dwOffset;

    if ( (ldwTransferEndByteOffset % dwPageByteLength) != 0)
      ++ dwLength;

    *lpdwTransferPageOffset = dwOffset;
    *lpdwTransferPageLength = dwLength;

    fSuccess = TRUE;

  }

_cleanup:
  return fSuccess;
}

typedef struct
{
  DWORD64 ldwBodyTransferByteLength;

  DWORD   dwHeadTransferByteLength;
  DWORD   dwHeadByteOffset;
  DWORD   dwHeadByteLength;

  DWORD   dwTailTransferByteLength;
  DWORD   dwTailByteLength;

} EPALIGNED_PARAM;

static BOOL
_GetEPSizeAlignedArgs
(
  DWORD64 ldwTransferByteOffset,
  DWORD64 ldwTransferByteLength,

  DWORD   dwPageByteLength,
  DWORD   dwTransferPageLength,

  DWORD   dwEPMaximumTransferSize,

  EPALIGNED_PARAM* p
)
{
  DWORD64 ldwTotalTransferByteLength;

  DWORD dwEPAlignedSize;

  dwEPAlignedSize = dwEPMaximumTransferSize;
  if (dwEPAlignedSize < dwPageByteLength)
  {
    // dwEPAlignedSize += (dwPageByteLength - (dwPageByteLength % dwEPMaximumTransferSize));

    // EP size is always power of 2 ???
    DWORD mask = (dwEPMaximumTransferSize - 1);
    dwEPAlignedSize = (dwPageByteLength + mask) & (~mask);
  }

  ldwTotalTransferByteLength =  dwPageByteLength;
  ldwTotalTransferByteLength *= dwTransferPageLength;

  p->dwHeadTransferByteLength = 0;
  p->dwHeadByteLength = 0;
  p->dwHeadByteOffset = (DWORD)(ldwTransferByteOffset % dwPageByteLength);
  if (p->dwHeadByteOffset != 0)
  {
    p->dwHeadByteLength = (dwEPAlignedSize - p->dwHeadByteOffset);
    if (p->dwHeadByteLength > ldwTransferByteLength)
      p->dwHeadByteLength = (DWORD)(ldwTransferByteLength);

    p->dwHeadTransferByteLength = dwEPAlignedSize;
    if (p->dwHeadTransferByteLength > ldwTotalTransferByteLength)
      p->dwHeadTransferByteLength = (DWORD)ldwTotalTransferByteLength;
  }

  ldwTotalTransferByteLength -= p->dwHeadTransferByteLength;
  ldwTransferByteLength -= p->dwHeadByteLength;

  p->dwTailByteLength = (DWORD)(ldwTransferByteLength % dwEPMaximumTransferSize);

  p->ldwBodyTransferByteLength = ldwTransferByteLength - p->dwTailByteLength;

  p->dwTailTransferByteLength = (DWORD)(ldwTotalTransferByteLength - p->ldwBodyTransferByteLength);

  if (p->dwTailByteLength == p->dwTailTransferByteLength)
  {
    p->ldwBodyTransferByteLength += p->dwTailTransferByteLength;

    p->dwTailTransferByteLength = 0;
    p->dwTailByteLength = 0;
  }

  return TRUE;
}

#define STATIC_HEAD_BUFFER_SIZE   (0x1000)
#define STATIC_TAIL_BUFFER_SIZE   (0x1000)

static BOOL
_Read_Transfer(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  const EPALIGNED_PARAM* p,
  LPVOID pBuffer)
{
  BOOL fSuccess = FALSE;

  LPBYTE  pAllocTailBuffer = NULL;

  BYTE    pStaticTailBuffer[STATIC_TAIL_BUFFER_SIZE];
  DWORD   dwTailBufferReqSize = 0;
  LPBYTE  pTailBuffer = pStaticTailBuffer;

  LPBYTE pData = (LPBYTE)pBuffer;

  DBG_ENTRY;

  {
    // Prepare tail buffer
    dwTailBufferReqSize = p->dwHeadTransferByteLength;
    if (dwTailBufferReqSize < p->dwTailTransferByteLength)
      dwTailBufferReqSize = p->dwTailTransferByteLength;

    if (dwTailBufferReqSize > STATIC_TAIL_BUFFER_SIZE)
    {
      pAllocTailBuffer = (LPBYTE)malloc(dwTailBufferReqSize);
      if (pAllocTailBuffer == NULL)
        goto _cleanup;

      pTailBuffer = pAllocTailBuffer;
    }

    if (p->dwHeadTransferByteLength != 0)
    {
      fSuccess = _ReadWriteDevicePipeSync(pDI, FALSE, pTailBuffer, p->dwHeadTransferByteLength, NULL);
      if (!fSuccess)
        goto _cleanup;
      memcpy(pData, pTailBuffer + p->dwHeadByteOffset, p->dwHeadByteLength);
      pData += p->dwHeadByteLength;
    }

    if (p->ldwBodyTransferByteLength != 0)
    {
      fSuccess = _ReadWriteDevicePipeSync(pDI, FALSE, pData, p->ldwBodyTransferByteLength, NULL);
      if (!fSuccess)
        goto _cleanup;
      pData += p->ldwBodyTransferByteLength;
    }

    if (p->dwTailTransferByteLength != 0)
    {
      fSuccess = _ReadWriteDevicePipeSync(pDI, FALSE, pTailBuffer, p->dwTailTransferByteLength, NULL);
      if (!fSuccess)
        goto _cleanup;
      memcpy(pData, pTailBuffer, p->dwTailByteLength);
    }

  }

_cleanup:
  if (pAllocTailBuffer != NULL)
    free(pAllocTailBuffer);

  DBG_LEAVE(fSuccess);

  return fSuccess;
}

static BOOL
_StagedRead_Transfer(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  const EPALIGNED_PARAM* p,
  LPTNMLIB_STAGEDREADPROC lpReadProc,
  LPVOID                  lpReadProcParam
  )
{
  BOOL fSuccess = FALSE;

  LPBYTE  pAllocTailBuffer = NULL;

  BYTE    pStaticTailBuffer[STATIC_TAIL_BUFFER_SIZE];
  LPBYTE  pTailBuffer = pStaticTailBuffer;

  // LPBYTE pData = (LPBYTE)pBuffer;

  DBG_ENTRY;

  {
    // Prepare tail buffer
    if (p->dwHeadTransferByteLength > STATIC_TAIL_BUFFER_SIZE)
    {
      pAllocTailBuffer = (LPBYTE)malloc(p->dwHeadTransferByteLength);
      if (pAllocTailBuffer == NULL)
        goto _cleanup;

      pTailBuffer = pAllocTailBuffer;
    }

    if (p->dwHeadTransferByteLength != 0)
    {
      fSuccess = _ReadWriteDevicePipeSync(pDI, FALSE, pTailBuffer, p->dwHeadTransferByteLength, NULL);
      if (!fSuccess)
        goto _cleanup;

      // memcpy(pData, pTailBuffer + p->dwHeadByteOffset, p->dwHeadByteLength);
      // pData += p->dwHeadByteLength;

      if (lpReadProc != NULL)
      {
        lpReadProc( 
          (pTailBuffer + p->dwHeadByteOffset), 
          p->dwHeadByteLength, 
          lpReadProcParam);
      }
    }

    if (p->ldwBodyTransferByteLength != 0)
    {
      // fSuccess = _ReadWriteDevicePipeSync(pDI, FALSE, pData, p->ldwBodyTransferByteLength, NULL);
      // if (!fSuccess)
      //  goto _cleanup;
      // pData += p->ldwBodyTransferByteLength;
    }

    if (p->dwTailTransferByteLength != 0)
    {
      fSuccess = _ReadWriteDevicePipeSync(pDI, FALSE, pTailBuffer, p->dwTailTransferByteLength, NULL);
      if (!fSuccess)
        goto _cleanup;
      // memcpy(pData, pTailBuffer, p->dwTailByteLength);
      if (lpReadProc != NULL)
      {
        lpReadProc( 
          pTailBuffer, 
          p->dwTailTransferByteLength, 
          lpReadProcParam);
      }
    }

  }

_cleanup:
  if (pAllocTailBuffer != NULL)
    free(pAllocTailBuffer);

  DBG_LEAVE(fSuccess);

  return fSuccess;
}


BOOL
_TND_RawReadRecord(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  DWORD   dwIndex,
  DWORD   dwOffsetPages,
  DWORD   dwCountPages,
  LPVOID  pBuffer
  )
{
  BOOL fSuccess = FALSE;
  DBG_ENTRY;

  {
    const TNMLIB_DEVICE_MEMORY_EXINFO* c_pDMEI;
    DWORD dwPageSize;

    c_pDMEI = _TND_GetCachedMEI(pDI, TNMLIB_DEVICE_MEMTYPE_FLASH);
    if (c_pDMEI == NULL)
      goto _cleanup;
    dwPageSize = c_pDMEI->RW[TNMLIB_DEVICE_MEMORY_EXINFO_READ].dwPageSize;

    pDI->cbw.wOperation = TNMLIB_DEVICE_COMMAND_READ_RECORD;
    pDI->cbw.dwArgs[0] = dwOffsetPages;
    pDI->cbw.dwArgs[1] = dwCountPages;
    pDI->cbw.dwArgs[2] = dwIndex;

    fSuccess = _DeviceCBWAndDataStage(
      pDI, 
      _CBW_DATASTAGE_READ,
      pBuffer,
      dwCountPages, dwPageSize);

    if (!fSuccess)
      goto _cleanup;

  }

_cleanup:
  DBG_LEAVE(fSuccess);
  return fSuccess;
}

BOOL
_TND_RawRWMemory(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  BOOL    bIsWrite,
  DWORD   dwMemoryType,
  DWORD   dwOffsetPages,
  DWORD   dwCountPages,
  LPVOID  pBuffer
  )
{
  BOOL fSuccess = FALSE;
  DBG_ENTRY;

  {
    LPTNMLIB_DEVICE_MEMORY_RWINFO pRWI;
    LPTNMLIB_DEVICE_MEMORY_EXINFO c_pDMEI;

    fSuccess = _TND_GetMemoryExInfoArray(pDI, dwMemoryType, 1, NULL);
    if (!fSuccess)
      goto _cleanup;

    pDI->cbw.wOperation = 
      bIsWrite ? (TNMLIB_DEVICE_COMMAND_EXWRITE_MEMORY | TNMLIB_DEVICE_COMMAND_ADD_SYNC_STAGE) : TNMLIB_DEVICE_COMMAND_EXREAD_MEMORY;

    pDI->cbw.dwArgs[0] = dwOffsetPages;
    pDI->cbw.dwArgs[1] = dwCountPages;
    pDI->cbw.dwArgs[2] = dwMemoryType;

    c_pDMEI = _GetVarCache(&pDI->cache, _CACHE_VAR_EXINFO_INDEX);
    if (c_pDMEI == NULL)
    {
      fSuccess = FALSE;
      goto _cleanup;
    }

    pRWI = &(c_pDMEI[dwMemoryType].RW[
      bIsWrite ? TNMLIB_DEVICE_MEMORY_EXINFO_WRITE : TNMLIB_DEVICE_MEMORY_EXINFO_READ]);

    if (bIsWrite)
    {
      if (dwMemoryType == TNMLIB_DEVICE_MEMTYPE_RECORDS)
        _MarkCacheAsInvalid(&pDI->cache, _CACHE_MEMORY_INFO | _CACHE_RECORDS_INFO);

      if (dwMemoryType == TNMLIB_DEVICE_MEMTYPE_BADBLOCKS)
        _MarkCacheAsInvalid(&pDI->cache, _CACHE_MEMORY_INFO | _CACHE_RECORDS_INFO);

      if (dwMemoryType == TNMLIB_DEVICE_MEMTYPE_TASKS)
        _MarkCacheAsInvalid(&pDI->cache, _CACHE_MEMORY_INFO | _CACHE_TASKS_INFO);
    }

    fSuccess = _DeviceCBWAndDataStage(
      pDI, 
      bIsWrite ? _CBW_DATASTAGE_WRITE : _CBW_DATASTAGE_READ,
      pBuffer, dwCountPages, 
      pRWI->dwPageSize);

    if (!fSuccess)
      goto _cleanup;

  }

_cleanup:
  DBG_LEAVE(fSuccess);
  return fSuccess;
}


BOOL
_TND_ReadRecord(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  DWORD   dwIndex,
  DWORD64 ldwOffsetBytes,
  DWORD64 ldwBytesToRead,
  LPVOID  pBuffer
)
{
  BOOL fSuccess = FALSE;

  DBG_ENTRY;

  {
    const TNMLIB_DEVICE_MEMORY_EXINFO* c_pDMEI;
    const TNMLIB_DEVICE_RECORD_INFO* c_pDRI;

    EPALIGNED_PARAM pEAP;

    DWORD   dwOffset;
    DWORD   dwLength;

    DWORD   dwMemoryPageByteSize;
    DWORD64 ldwMemoryTotalByteSize;
        
    c_pDMEI = _TND_GetCachedMEI(pDI, TNMLIB_DEVICE_MEMTYPE_FLASH);
    if (c_pDMEI == NULL)
      goto _cleanup;
    dwMemoryPageByteSize = c_pDMEI->RW[TNMLIB_DEVICE_MEMORY_EXINFO_READ].dwPageSize;

    c_pDRI = _TND_GetCachedRI(pDI, dwIndex);
    if (c_pDRI == NULL)
      goto _cleanup;
    ldwMemoryTotalByteSize = c_pDRI->ldwSize;

    fSuccess = _CheckRange(ldwOffsetBytes, ldwBytesToRead, ldwMemoryTotalByteSize);
    if (!fSuccess)
      goto _cleanup;

    fSuccess = _GetPagedRequestArgs(
      ldwOffsetBytes, ldwBytesToRead, dwMemoryPageByteSize, 
      &dwOffset, &dwLength);
    if (!fSuccess)
      goto _cleanup;

    pDI->cbw.wOperation = TNMLIB_DEVICE_COMMAND_READ_RECORD;
    pDI->cbw.dwArgs[0] = dwOffset;
    pDI->cbw.dwArgs[1] = dwLength;
    pDI->cbw.dwArgs[2] = dwIndex;

    fSuccess = _DeviceCBWAndDataStage(
      pDI, 
      _CBW_DATASTAGE_VALIDATE, 
      NULL, 
      dwLength, dwMemoryPageByteSize);
    if (!fSuccess)
      goto _cleanup;

    fSuccess = _GetEPSizeAlignedArgs(
      ldwOffsetBytes, ldwBytesToRead, dwMemoryPageByteSize, dwLength,
      pDI->dwInMaximumPacketSize, &pEAP);

    fSuccess = _Read_Transfer(pDI, &pEAP, pBuffer);
    if (!fSuccess)
      goto _cleanup;

  }

_cleanup:
  DBG_LEAVE(fSuccess);
  return fSuccess;
}

BOOL
_TND_StagedReadRecord(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  DWORD   dwIndex,
  DWORD64 ldwOffsetBytes,
  DWORD64 ldwBytesToRead,
  LPTNMLIB_STAGEDREADPROC lpReadProc,
  LPVOID                  lpReadProcParam
  )
{
  BOOL fSuccess = FALSE;

  DBG_ENTRY;

  {
    const TNMLIB_DEVICE_MEMORY_EXINFO* c_pDMEI;
    const TNMLIB_DEVICE_RECORD_INFO* c_pDRI;

    EPALIGNED_PARAM pEAP;

    DWORD   dwOffset;
    DWORD   dwLength;

    DWORD   dwMemoryPageByteSize;
    DWORD64 ldwMemoryTotalByteSize;

    c_pDMEI = _TND_GetCachedMEI(pDI, TNMLIB_DEVICE_MEMTYPE_FLASH);
    if (c_pDMEI == NULL)
      goto _cleanup;
    dwMemoryPageByteSize = c_pDMEI->RW[TNMLIB_DEVICE_MEMORY_EXINFO_READ].dwPageSize;

    c_pDRI = _TND_GetCachedRI(pDI, dwIndex);
    if (c_pDRI == NULL)
      goto _cleanup;
    ldwMemoryTotalByteSize = c_pDRI->ldwSize;

    fSuccess = _CheckRange(ldwOffsetBytes, ldwBytesToRead, ldwMemoryTotalByteSize);
    if (!fSuccess)
      goto _cleanup;

    fSuccess = _GetPagedRequestArgs(
      ldwOffsetBytes, ldwBytesToRead, dwMemoryPageByteSize, 
      &dwOffset, &dwLength);
    if (!fSuccess)
      goto _cleanup;

    pDI->cbw.wOperation = TNMLIB_DEVICE_COMMAND_READ_RECORD;
    pDI->cbw.dwArgs[0] = dwOffset;
    pDI->cbw.dwArgs[1] = dwLength;
    pDI->cbw.dwArgs[2] = dwIndex;

    fSuccess = _DeviceCBWAndDataStage(
      pDI, 
      _CBW_DATASTAGE_VALIDATE, 
      NULL, 
      dwLength, dwMemoryPageByteSize);
    if (!fSuccess)
      goto _cleanup;

    fSuccess = _GetEPSizeAlignedArgs(
      ldwOffsetBytes, ldwBytesToRead, dwMemoryPageByteSize, dwLength,
      pDI->dwInMaximumPacketSize, &pEAP);

    fSuccess = _StagedRead_Transfer(pDI, &pEAP, lpReadProc, lpReadProcParam);
    if (!fSuccess)
      goto _cleanup;

  }

_cleanup:
  DBG_LEAVE(fSuccess);
  return fSuccess;
}



BOOL
_TND_ReadMemory(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  DWORD   dwMemoryType,
  DWORD64 ldwOffsetBytes,
  DWORD64 ldwBytesToRead,
  LPVOID  pBuffer
  )
{
  BOOL fSuccess = FALSE;
  DBG_ENTRY;

  {
    const TNMLIB_DEVICE_MEMORY_EXINFO* c_pDMEI;
    EPALIGNED_PARAM pEAP;

    DWORD   dwOffset;
    DWORD   dwLength;

    DWORD   dwMemoryPageByteSize;
    DWORD64 ldwMemoryTotalByteSize;

    c_pDMEI = _TND_GetCachedMEI(pDI, dwMemoryType);
    if (c_pDMEI == NULL)
      goto _cleanup;
    
    dwMemoryPageByteSize = c_pDMEI->RW[TNMLIB_DEVICE_MEMORY_EXINFO_READ].dwPageSize;

    ldwMemoryTotalByteSize = dwMemoryPageByteSize;
    ldwMemoryTotalByteSize *= c_pDMEI->RW[TNMLIB_DEVICE_MEMORY_EXINFO_READ].dwTotalPages;

    fSuccess = _CheckRange(ldwOffsetBytes, ldwBytesToRead, ldwMemoryTotalByteSize);
    if (!fSuccess)
      goto _cleanup;

    fSuccess = _GetPagedRequestArgs(
      ldwOffsetBytes, ldwBytesToRead, dwMemoryPageByteSize, 
      &dwOffset, &dwLength);
    if (!fSuccess)
      goto _cleanup;

    pDI->cbw.wOperation = TNMLIB_DEVICE_COMMAND_EXREAD_MEMORY;
    pDI->cbw.dwArgs[0] = dwOffset;
    pDI->cbw.dwArgs[1] = dwLength;
    pDI->cbw.dwArgs[2] = dwMemoryType;

    fSuccess = _DeviceCBWAndDataStage(
      pDI, 
      _CBW_DATASTAGE_VALIDATE, 
      NULL, 
      dwLength, 
      dwMemoryPageByteSize);
    if (!fSuccess)
      goto _cleanup;

    fSuccess = _GetEPSizeAlignedArgs(
      ldwOffsetBytes, ldwBytesToRead, dwMemoryPageByteSize, dwLength,
      pDI->dwInMaximumPacketSize, &pEAP);

    fSuccess = _Read_Transfer(pDI, &pEAP, pBuffer);
    if (!fSuccess)
      goto _cleanup;

  }

_cleanup:
  DBG_LEAVE(fSuccess);
  return fSuccess;
}

BOOL
_TND_WriteMemory(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  DWORD   dwMemoryType,
  DWORD64 ldwOffsetBytes,
  DWORD64 ldwBytesToWrite,
  LPCVOID  pBuffer
  )
{
  BOOL fSuccess = FALSE;
  
  BYTE    pHeadPageStatic[STATIC_HEAD_BUFFER_SIZE];
  LPBYTE  pHeadPageAllocated = NULL;
  LPBYTE  pHeadPage = pHeadPageStatic;

  BYTE    pTailPageStatic[STATIC_TAIL_BUFFER_SIZE];
  LPBYTE  pTailPageAllocated = NULL;
  LPBYTE  pTailPage = pTailPageStatic;

  DBG_ENTRY;

  {
    const TNMLIB_DEVICE_MEMORY_EXINFO* c_pDMEI;
    EPALIGNED_PARAM pEAP;

    DWORD   dwOffset;
    DWORD   dwLength;

    DWORD   dwMemoryPageByteSize;
    DWORD64 ldwMemoryTotalByteSize;

    LPBYTE pData = (LPBYTE)pBuffer;

    c_pDMEI = _TND_GetCachedMEI(pDI, dwMemoryType);
    if (c_pDMEI == NULL)
      goto _cleanup;

    dwMemoryPageByteSize = c_pDMEI->RW[TNMLIB_DEVICE_MEMORY_EXINFO_WRITE].dwPageSize;

    ldwMemoryTotalByteSize = dwMemoryPageByteSize;
    ldwMemoryTotalByteSize *= c_pDMEI->RW[TNMLIB_DEVICE_MEMORY_EXINFO_WRITE].dwTotalPages;

    fSuccess = _CheckRange(ldwOffsetBytes, ldwBytesToWrite, ldwMemoryTotalByteSize);
    if (!fSuccess)
      goto _cleanup;

    fSuccess = _GetPagedRequestArgs(
      ldwOffsetBytes, ldwBytesToWrite, dwMemoryPageByteSize,
      &dwOffset, &dwLength);
    if (!fSuccess)
      goto _cleanup;

    fSuccess = _GetEPSizeAlignedArgs(
      ldwOffsetBytes, ldwBytesToWrite, dwMemoryPageByteSize, dwLength,
      pDI->dwOutMaximumPacketSize, &pEAP);

    if (pEAP.dwHeadTransferByteLength != 0)
    {
      DWORD64 ldwAlignedOffset;

      if (pEAP.dwHeadTransferByteLength > STATIC_HEAD_BUFFER_SIZE)
      {
        pHeadPageAllocated = (LPBYTE)malloc(pEAP.dwHeadTransferByteLength);
        if (pHeadPageAllocated == NULL)
          goto _cleanup;

        pHeadPage = pHeadPageAllocated;
      }

      ldwAlignedOffset = ldwOffsetBytes - pEAP.dwHeadByteOffset;

      fSuccess = _TND_ReadMemory(pDI, dwMemoryType, ldwAlignedOffset, pEAP.dwHeadTransferByteLength, pHeadPage);
      if (!fSuccess)
        goto _cleanup;

      memcpy(pHeadPage + pEAP.dwHeadByteOffset, pData, pEAP.dwHeadByteLength);
      pData += pEAP.dwHeadByteLength;
    }

    if (pEAP.dwTailTransferByteLength != 0)
    {
      DWORD64 ldwAlignedOffset = ldwOffsetBytes - pEAP.dwHeadByteOffset;

      if (pEAP.dwTailTransferByteLength > STATIC_TAIL_BUFFER_SIZE)
      {
        pTailPageAllocated = (LPBYTE)malloc(pEAP.dwTailTransferByteLength);
        if (pHeadPageAllocated == NULL)
          goto _cleanup;

        pHeadPage = pHeadPageAllocated;
      }

      ldwAlignedOffset += pEAP.dwHeadTransferByteLength + pEAP.ldwBodyTransferByteLength;

      fSuccess = _TND_ReadMemory(pDI, dwMemoryType, ldwAlignedOffset, pEAP.dwTailTransferByteLength, pTailPage);
      if (!fSuccess)
        goto _cleanup;

      memcpy(pTailPage, pData + pEAP.ldwBodyTransferByteLength, pEAP.dwTailByteLength);
    }

    pDI->cbw.wOperation = TNMLIB_DEVICE_COMMAND_EXWRITE_MEMORY | TNMLIB_DEVICE_COMMAND_ADD_SYNC_STAGE;
    pDI->cbw.dwArgs[0] = dwOffset;
    pDI->cbw.dwArgs[1] = dwLength;
    pDI->cbw.dwArgs[2] = dwMemoryType;

    fSuccess = _DeviceCBWAndDataStage(
      pDI, 
      _CBW_DATASTAGE_VALIDATE,
      NULL, dwLength, 
      0);
    if (!fSuccess)
      goto _cleanup;

    // Invalidate cache
    if (dwMemoryType == TNMLIB_DEVICE_MEMTYPE_RECORDS)
      _MarkCacheAsInvalid(&pDI->cache, _CACHE_MEMORY_INFO | _CACHE_RECORDS_INFO);

    if (dwMemoryType == TNMLIB_DEVICE_MEMTYPE_BADBLOCKS)
      _MarkCacheAsInvalid(&pDI->cache, _CACHE_MEMORY_INFO | _CACHE_RECORDS_INFO);

    if (dwMemoryType == TNMLIB_DEVICE_MEMTYPE_TASKS)
      _MarkCacheAsInvalid(&pDI->cache, _CACHE_MEMORY_INFO | _CACHE_TASKS_INFO);

    // Do staged transfer
    if (pEAP.dwHeadTransferByteLength != 0)
    {
      fSuccess = _ReadWriteDevicePipeSync(pDI, TRUE, pHeadPage, pEAP.dwHeadTransferByteLength, NULL);
      if (!fSuccess)
        goto _cleanup;
    }

    if (pEAP.ldwBodyTransferByteLength != 0)
    {
      fSuccess = _ReadWriteDevicePipeSync(pDI, TRUE, pData, pEAP.ldwBodyTransferByteLength, NULL);
      if (!fSuccess)
        goto _cleanup;
    }

    if (pEAP.dwTailByteLength != 0)
    {
      fSuccess = _ReadWriteDevicePipeSync(pDI, TRUE, pTailPage, pEAP.dwTailTransferByteLength, NULL);
      if (!fSuccess)
        goto _cleanup;
    }

    if (pDI->cbw.wOperation & TNMLIB_DEVICE_COMMAND_ADD_SYNC_STAGE)
    {
      fSuccess = _DeviceRecvCSW(pDI);
      if (!fSuccess)
        goto _cleanup;

      fSuccess = _ValidateCSW(&pDI->csw, TNMLIB_DEVICE_COMMAND_SYNC_REPLY);
      if (!fSuccess)
        goto _cleanup;
    }

  }

_cleanup:
  if (pHeadPageAllocated != NULL)
    free(pHeadPageAllocated);

  if (pTailPageAllocated != NULL)
    free(pTailPageAllocated);

  DBG_LEAVE(fSuccess);
  return fSuccess;
}

BOOL
_TND_DeleteAllRecords(LPTNMLIB_DEVICE_INSTANCE pDI)
{
  BOOL fSuccess;
  DBG_ENTRY;

  _MarkCacheAsInvalid(&pDI->cache, _CACHE_MEMORY_INFO | _CACHE_RECORDS_INFO);

  pDI->cbw.wOperation = TNMLIB_DEVICE_COMMAND_ERASE_RECORDS;
  fSuccess = _DeviceCBWAndDataStage(
    pDI, 
    _CBW_DATASTAGE_VALIDATE,
    NULL, 0, 0);

  DBG_LEAVE(fSuccess);
  return fSuccess;
}

BOOL
_TND_VerifyBlock(LPTNMLIB_DEVICE_INSTANCE pDI, DWORD dwBlock)
{
  BOOL fSuccess;
  DBG_ENTRY;

  pDI->cbw.wOperation = TNMLIB_DEVICE_COMMAND_VERIFY_BLOCK;
  pDI->cbw.dwArgs[0] = dwBlock;

  fSuccess = _DeviceCBWAndDataStage(
    pDI, 
    _CBW_DATASTAGE_VALIDATE,
    NULL, 0, 0);

  DBG_LEAVE(fSuccess);
  return fSuccess;
}
