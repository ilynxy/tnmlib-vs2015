MessageIdTypedef=DWORD

LanguageNames=(English=0x409:TNMMSG409)
LanguageNames=(Russian=0x419:TNMMSG419)

FacilityNames=(
  Special800=0x800
  Special000=0x000
  Special001=0x001
  Special002=0x002
  Special003=0x003
  Special004=0x004
  Special005=0x005
  DeviceErrors  = 0xF00
  LibraryErrors  = 0xE00
)

;#ifndef _TNMMSG_HEADER_
;#define _TNMMSG_HEADER_

MessageId=0x00000001
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_CRC
Language=English
HCD: A CRC error has been detected
.
Language=Russian
HCD: Неверная контрольная сумма
.

MessageId=0x00000002
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_BTSTUFF
Language=English
HCD: A bit stuffing error has been detected
.
Language=Russian
HCD: Неверная последовательность бит
.

MessageId=0x00000003
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_DATA_TOGGLE_MISMATCH
Language=English
HCD: A DATA toggle mismatch (DATA0/DATA1 tokens) has been detected
.
Language=Russian
HCD: Неверная последовательность токенов DATA0/DATA1
.

MessageId=0x00000004
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_STALL_PID
Language=English
HCD: A STALL PID has been detected
.
Language=Russian
HCD: Устройство вернуло идентификатор пакета "Остановлено (STALL)"
.

MessageId=0x00000005
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_DEV_NOT_RESPONDING
Language=English
HCD: The USB device is not responding
.
Language=Russian
HCD: Устройство не отвечает
.

MessageId=0x00000006
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_PID_CHECK_FAILURE
Language=English
HCD: A PID check has failed
.
Language=Russian
HCD: Ошибка проверки идентификатора пакета
.

MessageId=0x00000007
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_UNEXPECTED_PID
Language=English
HCD: An unexpected PID has been detected
.
Language=Russian
HCD: Неожидаемый идентификатор пакета
.

MessageId=0x00000008
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_DATA_OVERRUN
Language=English
HCD: A data overrun error has been detected
.
Language=Russian
HCD: Превышение допустимого количества данных
.

MessageId=0x00000009
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_DATA_UNDERRUN
Language=English
HCD: A data underrun error has been detected
.
Language=Russian
HCD: Количество данных недостаточно
.

MessageId=0x0000000A
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_RESERVED1
Language=English
HCD: Reserved 1
.
Language=Russian
HCD: Зарезервированный код ошибки 1
.

MessageId=0x0000000B
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_RESERVED2
Language=English
HCD: Reserved 2
.
Language=Russian
HCD: Зарезервированный код ошибки 2
.

MessageId=0x0000000C
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_BUFFER_OVERRUN
Language=English
HCD: A buffer overrun has been detected
.
Language=Russian
HCD: Буфер переполнен
.

MessageId=0x0000000D
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_BUFFER_UNDERRUN
Language=English
HCD: A buffer underrun has been detected
.
Language=Russian
HCD: Буфер не заполнен
.

MessageId=0x0000000F
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_NOT_ACCESSED
Language=English
HCD: A data buffer was not accessed. An isochronous data buffer was scheduled too late. The specified frame number does not match the actual frame number
.
Language=Russian
HCD: Не удалось получить доступ к буферу данных. Изохронный буфер данных поставлен в очередь слишком поздно. Указанный номер кадра не соответствует действительному
.

MessageId=0x00000010
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_FIFO
Language=English
HCD: A FIFO error has been detected. The PCI bus latency was too long
.
Language=Russian
HCD: Ошибка FIFO. Произошла слишком большая задержка шины PCI
.

MessageId=0x00000011
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_XACT_ERROR
Language=English
HCD: A XACT error has been detected
.
Language=Russian
HCD: Ошибка XACT
.

MessageId=0x00000012
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_BABBLE_DETECTED
Language=English
HCD: A device is babbling. The data transfer phase exceeds the USB frame length
.
Language=Russian
HCD: Обнаружено "бормотание" на линии. Фаза передачи данных превысила допустимую длину кадра USB
.

MessageId=0x00000013
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_DATA_BUFFER_ERROR
Language=English
HCD: A data buffer error has been detected
.
Language=Russian
HCD: Ошибка буфера данных
.

MessageId=0x00000030
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_ENDPOINT_HALTED
Language=English
USBD: The endpoint has been halted
.
Language=Russian
USBD: Конечная точка остановлена
.

MessageId=0x00000100
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_NO_MEMORY
Language=English
USBD: A memory allocation attempt has failed
.
Language=Russian
USBD: Недостаточно памяти
.

MessageId=0x00000200
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_INVALID_URB_FUNCTION
Language=English
USBD: An invalid URB function code has been passed
.
Language=Russian
USBD: Неверный код функции URB
.

MessageId=0x00000300
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_INVALID_PARAMETER
Language=English
USBD: An invalid parameter has been passed
.
Language=Russian
USBD: Неверный параметр
.

MessageId=0x00000400
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_ERROR_BUSY
Language=English
USBD: There are data transfer requests pending for the device
.
Language=Russian
USBD: Есть отложенные запросы передачи данных
.

MessageId=0x00000500
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_REQUEST_FAILED
Language=English
USBD: A request has failed
.
Language=Russian
USBD: Запрос не выполнен
.

MessageId=0x00000600
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_INVALID_PIPE_HANDLE
Language=English
USBD: An invalid pipe handle has been passed
.
Language=Russian
USBD: Неверный дескриптор канала
.

MessageId=0x00000700
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_NO_BANDWIDTH
Language=English
USBD: There is not enough bandwidth available
.
Language=Russian
USBD: Недостаточно полосы пропускания
.

MessageId=0x00000800
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_INTERNAL_HC_ERROR
Language=English
USBD: An internal host controller error has been detected
.
Language=Russian
USBD: Внутренняя ошибка хост-контроллера
.

MessageId=0x00000900
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_ERROR_SHORT_TRANSFER
Language=English
USBD: A short transfer has been detected
.
Language=Russian
USBD: Короткая транзакция
.

MessageId=0x00000A00
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_BAD_START_FRAME
Language=English
USBD: A bad start frame has been specified
.
Language=Russian
USBD: Неверный стартовый кадр
.

MessageId=0x00000B00
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_ISOCH_REQUEST_FAILED
Language=English
USBD: An isochronous request has failed
.
Language=Russian
USBD: Изохронный запрос не выполнен
.

MessageId=0x00000C00
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_FRAME_CONTROL_OWNED
Language=English
USBD: The USB frame control is currently owned
.
Language=Russian
USBD: Управление USB кадрами уже получено
.

MessageId=0x00000D00
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_FRAME_CONTROL_NOT_OWNED
Language=English
USBD: The USB frame control is currently not owned
.
Language=Russian
USBD: Управление USB кадрами не получено
.

MessageId=0x00000E00
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_NOT_SUPPORTED
Language=English
USBD: The operation is not supported
.
Language=Russian
USBD: Запрос не поддерживается
.

MessageId=0x00000F00
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_INVALID_CONFIGURATION_DESCRIPTOR
Language=English
USBD: An invalid configuration descriptor was reported by the device
.
Language=Russian
USBD: Устройство вернуло неверный конфигурационный дескриптор
.

MessageId=0x00001000
Severity=Error
Facility=Special800
;// SymbolicName=USBIO_ERR_INSUFFICIENT_RESOURCES
Language=English
USBD: There are not enough resources available to complete the operation
.
Language=Russian
USBD: Недостаточно ресурсов для завершения запроса
.

MessageId=0x00002000
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_SET_CONFIG_FAILED
Language=English
USBD: The set configuration request has failed
.
Language=Russian
USBD: Запрос конфигурирования не выполнен
.

MessageId=0x00003000
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_USBD_BUFFER_TOO_SMALL
Language=English
USBD: The buffer is too small.
.
Language=Russian
USBD: Размер буфера слишком мал
.

MessageId=0x00004000
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_USBD_INTERFACE_NOT_FOUND
Language=English
USBD: The interface was not found
.
Language=Russian
USBD: Интерфейс не найден
.

MessageId=0x00005000
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_INVALID_PIPE_FLAGS
Language=English
USBD: Invalid pipe flags have been specified
.
Language=Russian
USBD: Указаны неверные флаги канала
.

MessageId=0x00006000
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_USBD_TIMEOUT
Language=English
USBD: The operation has been timed out
.
Language=Russian
USBD: Время ожидания завершения операции истекло
.

MessageId=0x00007000
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_DEVICE_GONE
Language=English
USBD: Device is gone
.
Language=Russian
USBD: Устройство было отключено
.

MessageId=0x00008000
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_STATUS_NOT_MAPPED
Language=English
USBD: Status not mapped
.
Language=Russian
USBD: Состояние не отражено
.

MessageId=0x00000000
Severity=Error
Facility=Special001
;// SymbolicName=USBIO_ERR_CANCELED
Language=English
USBD: The operation has been cancelled
.
Language=Russian
USBD: Выполнение запроса отменено
.

MessageId=0x00000000
Severity=Error
Facility=Special002
;// SymbolicName=USBIO_ERR_ISO_NOT_ACCESSED_BY_HW
Language=English
USBD: The isochronous data buffer was not accessed by the USB host controller
.
Language=Russian
USBD: Буфер изохронных данных недоступен хост-контроллеру USB
.

MessageId=0x00000000
Severity=Error
Facility=Special003
;// SymbolicName=USBIO_ERR_ISO_TD_ERROR
Language=English
USBD: The USB host controller reported an error in a transfer descriptor
.
Language=Russian
USBD: Неверный дескриптор транзакции
.

MessageId=0x00000000
Severity=Error
Facility=Special004
;// SymbolicName=USBIO_ERR_ISO_NA_LATE_USBPORT
Language=English
USBD: An isochronous data packet was submitted in time but failed to reach the USB host controller in time
.
Language=Russian
USBD: Изохронный пакет был помещён в очередь запросов, но не достиг хост-контроллера вовремя
.

MessageId=0x00000000
Severity=Error
Facility=Special005
;// SymbolicName=USBIO_ERR_ISO_NOT_ACCESSED_LATE
Language=English
USBD: An isochronous data packet was submitted too late
.
Language=Russian
USBD: Изохронный пакет был помещён в очередь запросов слишком поздно
.

MessageId=0x00001000
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_FAILED
Language=English
USBIO: The operation has failed
.
Language=Russian
USBIO: Операция не выполнена
.

MessageId=0x00001001
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_INVALID_INBUFFER
Language=English
USBIO: An invalid input buffer has been passed to an IOCTL operation
.
Language=Russian
USBIO: Размер входного буфера слишком мал для выполнения IOCTL запроса
.

MessageId=0x00001002
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_INVALID_OUTBUFFER
Language=English
USBIO: An invalid output buffer has been passed to an IOCTL operation
.
Language=Russian
USBIO: Размер выходного буфера слишком мал для выполнения IOCTL запроса
.

MessageId=0x00001003
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_OUT_OF_MEMORY
Language=English
USBIO: There is not enough system memory available to complete the operation
.
Language=Russian
USBIO: Недостаточно памяти для завершения операции
.

MessageId=0x00001004
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_PENDING_REQUESTS
Language=English
USBIO: There are read or write requests pending
.
Language=Russian
USBIO: Есть отложенные запросы ввода/вывода
.

MessageId=0x00001005
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_ALREADY_CONFIGURED
Language=English
USBIO: The USB device is already configured
.
Language=Russian
USBIO: Устройство уже сконфигурированно
.

MessageId=0x00001006
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_NOT_CONFIGURED
Language=English
USBIO: The USB device is not configured
.
Language=Russian
USBIO: Устройство не сконфигурированно
.

MessageId=0x00001007
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_OPEN_PIPES
Language=English
USBIO: There are open pipes
.
Language=Russian
USBIO: Есть открытые каналы
.

MessageId=0x00001008
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_ALREADY_BOUND
Language=English
USBIO: Either the handle is already bound to a pipe or the specified pipe is already bound to another handle
.
Language=Russian
USBIO: Дескриптор уже связан с конечной точкой
.

MessageId=0x00001009
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_NOT_BOUND
Language=English
USBIO: The handle is not bound to a pipe
.
Language=Russian
USBIO: Дескриптор не связан с конечной точкой
.

MessageId=0x0000100A
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_DEVICE_NOT_PRESENT
Language=English
USBIO: The USB device has been removed from the system
.
Language=Russian
USBIO: Устройство удалено из системы
.

MessageId=0x0000100B
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_CONTROL_NOT_SUPPORTED
Language=English
USBIO: The specified control code is not supported
.
Language=Russian
USBIO: Управляющий код не поддерживается
.

MessageId=0x0000100C
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_TIMEOUT
Language=English
USBIO: The operation has been timed out
.
Language=Russian
USBIO: Время ожидания завершения запроса истекло
.

MessageId=0x0000100D
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_INVALID_RECIPIENT
Language=English
USBIO: An invalid recipient has been specified
.
Language=Russian
USBIO: Неверный адресат
.

MessageId=0x0000100E
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_INVALID_TYPE
Language=English
USBIO: Either an invalid request type has been specified or the operation is not supported by that pipe type
.
Language=Russian
USBIO: Неверный тип запроса. Запрос не поддерживается для этого канала
.

MessageId=0x0000100F
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_INVALID_IOCTL
Language=English
USBIO: An invalid IOCTL code has been specified
.
Language=Russian
USBIO: Неверный IOCTL код
.

MessageId=0x00001010
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_INVALID_DIRECTION
Language=English
USBIO: The direction of the data transfer request is not supported by that pipe
.
Language=Russian
USBIO: Канал не поддерживает передачу данных в запрошенном направлении
.

MessageId=0x00001011
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_TOO_MUCH_ISO_PACKETS
Language=English
USBIO: The number of isochronous data packets specified in an isochronous read or write request exceeds the maximum number of packets supported by the USBIO driver
.
Language=Russian
USBIO: Число изохронных пакетов данных, указанных для операции ввода/вывода превышает максимальное поддерживаемое драйвером
.

MessageId=0x00001012
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_POOL_EMPTY
Language=English
USBIO: The memory resources are exhausted
.
Language=Russian
USBIO: Память исчерпана
.

MessageId=0x00001013
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_PIPE_NOT_FOUND
Language=English
USBIO: The specified pipe was not found in the current configuration
.
Language=Russian
USBIO: Указанный канал не найден в текущей конфигурации
.

MessageId=0x00001014
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_INVALID_ISO_PACKET
Language=English
USBIO: An invalid isochronous data packet has been specified
.
Language=Russian
USBIO: Указан неверный изохронный пакет данных
.

MessageId=0x00001015
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_OUT_OF_ADDRESS_SPACE
Language=English
USBIO: There are not enough system resources to complete the operation
.
Language=Russian
USBIO: Недостаточно системных ресурсов для завершения операции
.

MessageId=0x00001016
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_INTERFACE_NOT_FOUND
Language=English
USBIO: The specified interface was not found in the current configuration or in the configuration descriptor
.
Language=Russian
USBIO: Указанный интерфейс не найден в текущей конфигурации или в конфигурационном дескрипторе
.

MessageId=0x00001017
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_INVALID_DEVICE_STATE
Language=English
USBIO: The operation cannot be executed while the USB device is in the current state
.
Language=Russian
USBIO: Текущее состояние энергопотребления устройства не позволяет выполнить запрошенную операцию
.

MessageId=0x00001018
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_INVALID_PARAM
Language=English
USBIO: An invalid parameter has been specified with an IOCTL operation
.
Language=Russian
USBIO: Указан неверный параметр для IOCTL запроса
.

MessageId=0x00001019
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_DEMO_EXPIRED
Language=English
USBIO: The evaluation interval of the USBIO DEMO version has expired
.
Language=Russian
USBIO: Оценочный период использования драйвера USBIO DEMO истёк
.

MessageId=0x0000101A
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_INVALID_POWER_STATE
Language=English
USBIO: An invalid power state has been specified
.
Language=Russian
USBIO: Указано неверное значение энергопотребления
.

MessageId=0x0000101B
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_POWER_DOWN
Language=English
USBIO: The device has entered a power down state
.
Language=Russian
USBIO: Устройство вошло в режим пониженного энергопотребления
.

MessageId=0x0000101C
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_VERSION_MISMATCH
Language=English
USBIO: The API version reported by the USBIO driver does not match the expected version
.
Language=Russian
USBIO: Версия API полученная от драйвера не соответствует ожидаемой
.

MessageId=0x0000101D
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_SET_CONFIGURATION_FAILED
Language=English
USBIO: The set configuration operation has failed
.
Language=Russian
USBIO: Операция конфигурирования не выполнена
.

MessageId=0x0000101E
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_ADDITIONAL_EVENT_SIGNALLED
Language=English
An additional event object that has been passed to a function, was signalled
.
Language=Russian
Объект ядра "Событие (Event)", переданный в функцию, в сигнальном состоянии
.

MessageId=0x0000101F
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_INVALID_PROCESS
Language=English
USBIO: This operation is not allowed for this process.
.
Language=Russian
USBIO: Отказ выполнения запрошенной операции для этого процесса
.

MessageId=0x00001020
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_DEVICE_ACQUIRED
Language=English
USBIO: The device is acquired by a different process for exclusive usage
.
Language=Russian
USBIO: Устройство эксклюзивно используется другим процессом
.

MessageId=0x00001021
Severity=Error
Facility=Special000
;// SymbolicName=USBIO_ERR_DEVICE_OPENED
Language=English
USBIO: The device is opened by a different process.
.
Language=Russian
USBIO: Устройство используется другим процессом
.

; /*
;  TNMLIB DEVICE specific error codes
; */

;#define TNMLIB_DEVICE_ERROR_BASE  ((DWORD)0xEF000000L)
;#define TNMLIB_ERROR_BASE      ((DWORD)0xEE000000L)

MessageId=0x00001000
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_PROTO_NOTIMPLEMENTED
Language=English
SSD: Operation not implemented
.
Language=Russian
ТН: Операция не поддерживается
.

MessageId=0x00001001
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_PROTO_INVALID_TAG
Language=English
SSD: Invalid device tag
.
Language=Russian
ТН: Неверная метка устройства
.

MessageId=0x00001002
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_PROTO_INVALID_CBW_LENGTH
Language=English
SSD: Invalid length of control block
.
Language=Russian
ТН: Неверный размер управляющего блока
.

MessageId=0x00001003
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_PROTO_TRANSACTION_UNDERRUN
Language=English
SSD: Transaction length is less than required
.
Language=Russian
ТН: Размер транзакции меньше затребованной
.

MessageId=0x00001004
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_PROTO_TRANSACTION_OVERRUN
Language=English
SSD: Transaction length is great than required
.
Language=Russian
ТН: Размер транзакции меньше затребованной
.

MessageId=0x00001005
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_PROTO_SYNC_FAILED
Language=English
SSD: Request's synchronization failed
.
Language=Russian
ТН: Cинхронизация запроса не выполнена
.

MessageId=0x00000001
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_DEVICE_BUSY
Language=English
SSD: Device is busy
.
Language=Russian
ТН: Устройство занято выполнением запроса
.

MessageId=0x00000002
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_NOT_LOCKED
Language=English
SSD: Specified request require exclusive access to device
.
Language=Russian
ТН: Для выполнения запроса требуется эксклюзивный доступ к устройству
.

MessageId=0x00000003
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_ALREADY_LOCKED
Language=English
SSD: Exclusive access to device has been already granted
.
Language=Russian
ТН: Эксклюзивный доступ к устройству уже получен
.

MessageId=0x00000004
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_NO_SUCH_MEMORY_TYPE
Language=English
SSD: No such type of memory
.
Language=Russian
ТН: Запрошенный тип памяти не существует
.

MessageId=0x00000005
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_OFFSET_OUT_OF_RANGE
Language=English
SSD: Offset out of range
.
Language=Russian
ТН: Смещение выходит за границы диапазона
.

MessageId=0x00000006
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_SIZE_OUT_OF_RANGE
Language=English
SSD: Size out of range
.
Language=Russian
ТН: Размер выходит за границы диапазона
.

MessageId=0x00000007
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_FLASH_VERIFY_FAIL
Language=English
SSD: FLASH memory block has defect(s)
.
Language=Russian
ТН: Блок флэш-памяти имеет дефект(ы)
.

MessageId=0x00000008
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_DEVICE_NOT_EMPTY
Language=English
SSD: Device has record(s)
.
Language=Russian
ТН: Устройство содержит накопленные режимы
.

MessageId=0x00000009
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_NO_FREE_MEMORY
Language=English
SSD: Device memory is full
.
Language=Russian
ТН: Память устройства полностью заполнена
.

MessageId=0x0000000A
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_NO_FREE_RECORD_ENTRY
Language=English
SSD: No free entries in records table
.
Language=Russian
ТН: Достигнуто максимальное число накопленных режимов
.

MessageId=0x0000000B
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_NO_OPEN_RECORD
Language=English
SSD: Record has not been started
.
Language=Russian
ТН: Режим записи не был инициирован
.

MessageId=0x0000000C
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_NO_SUCH_RECORD
Language=English
SSD: No such record
.
Language=Russian
ТН: Режима с запрошенным номером не существует
.

MessageId=0x0000000D
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_NOT_ALIGNED
Language=English
SSD: Parameter has wrong alignment
.
Language=Russian
ТН: Параметр имеет неверное выравнивание
.

MessageId=0x0000000E
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_NO_TASKS
Language=English
SSD: Device has no tasks
.
Language=Russian
ТН: Устройство не содержит заданий
.

MessageId=0x0000000F
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_TASK_ID_MISMATCH
Language=English
SSD: TaskID does not match to TaskID requested by Data unit
.
Language=Russian
ТН: Идентификатор задания не соответствует идентификатору устройства сбора
.

MessageId=0x00000010
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_TASK_VERSION_SMALL
Language=English
SSD: Task version mismatch
.
Language=Russian
Ошибка ТН: Версия задания не соответствует версии устройства сбора
.

MessageId=0x00000011
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_SEND_TASK_TIMEOUT
Language=English
SSD: Task sending has been timeout
.
Language=Russian
ТН: Превышено время ожидания при отправки задания устройству сбора
.

MessageId=0x00000012
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_WAIT_ONWR_TIMEOUT
Language=English
SSD: Waiting for "RECORD STARTED" event has been timeout
.
Language=Russian
Ошибка ТН: Превышено время ожидания признака "ЗАПИСЬ ВКЛЮЧЕНА" от устройства сбора
.

MessageId=0x00000013
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_WAIT_DUINFO_TIMEOUT
Language=English
SSD: Waiting for DUINFO has been timeout
.
Language=Russian
ТН: Превышено время ожидания пакета идентификации (DUINFO) устройства сбора
.

MessageId=0x00000014
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_DUINFO_WRONG_CHECKSUM
Language=English
SSD: Data unit ID packet has wrong checksum 
.
Language=Russian
ТН: Неверная контрольная сумма пакета идентификации устройства сбора
.

MessageId=0x00000015
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_NO_SUCH_TASK
Language=English
SSD: No such task
.
Language=Russian
ТН: Устройство не содержит задания с запрошенным номером
.

MessageId=0x00000016
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_NOT_IN_CONTROL
Language=English
SSD: Realtime control mode has not been started
.
Language=Russian
ТН: Режим получения данных в реальном масштабе времени не был инициирован
.

MessageId=0x00000017
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_FAKETASK_WRONG_HEADER_SIZE
Language=English
SSD: Wrong header size of task for realtime control
.
Language=Russian
Ошибка ТН: Неверный размер заголовка контрольного задания
.

MessageId=0x00000018
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_FAKETASK_WRONG_SIZE
Language=English
SSD: Wrong size of task for realtime control
.
Language=Russian
ТН: Неверный размер контрольного задания
.

MessageId=0x00000019
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_FAKETASK_WRONG_CHECKSUM
Language=English
SSD: Wrong checksum of task for realtime control
.
Language=Russian
ТН: Контрольное задание имеет неверную контрольную сумму
.

MessageId=0x0000001A
Severity=Error
Facility=DeviceErrors
SymbolicName=TNMLIB_DEVICE_ERR_NOTSUPPORTED
Language=English
SSD: Requested operation is not supported
.
Language=Russian
ТН: Операция не поддерживается
.

;/*
; TNMLIB specific error codes
;*/

MessageId=0x00000001
Severity=Error
Facility=LibraryErrors
SymbolicName=TNMLIB_ERR_PROTO_INVALID_TAG
Language=English
TNMLIB: Invalid device tag
.
Language=Russian
TNMLIB: Неверная метка устройства
.

MessageId=0x00000002
Severity=Error
Facility=LibraryErrors
SymbolicName=TNMLIB_ERR_PROTO_INVALID_CSW_LENGTH
Language=English
TNMLIB: Invalid length of status block
.
Language=Russian
TNMLIB: Неверный размер статусного блока
.

MessageId=0x00000003
Severity=Error
Facility=LibraryErrors
SymbolicName=TNMLIB_ERR_PROTO_OPERATION_MISMATCH
Language=English
TNMLIB: Operation code in CBW and CSW does not match
.
Language=Russian
TNMLIB: Код операции в командном блоке и статусном блоке не совпадают
.

MessageId=0x00000004
Severity=Error
Facility=LibraryErrors
SymbolicName=TNMLIB_ERR_PROTO_CBW_SEND_FAILED
Language=English
TNMLIB: Sending CBW to device failed
.
Language=Russian
TNMLIB: Отправка командного блока не выполнена
.

MessageId=0x00000005
Severity=Error
Facility=LibraryErrors
SymbolicName=TNMLIB_ERR_PROTO_CSW_RECV_FAILED
Language=English
TNMLIB: Receiving CSW from device failed
.
Language=Russian
TNMLIB: Приём статусного блока не выполнен
.

MessageId=0x00000006
Severity=Error
Facility=LibraryErrors
SymbolicName=TNMLIB_ERR_PROTO_TRANSACTION_UNDERRUN
Language=English
TNMLIB: Transaction size is less than requested
.
Language=Russian
TNMLIB: Размер транзакции меньше затребованного
.

MessageId=0x00000007
Severity=Error
Facility=LibraryErrors
SymbolicName=TNMLIB_ERR_PROTO_TRANSACTION_OVERRUN
Language=English
TNMLIB: Transaction size is great than requested
.
Language=Russian
TNMLIB: Размер транзакции больше затребованного
.

MessageId=0x00000008
Severity=Error
Facility=LibraryErrors
SymbolicName=TNMLIB_ERR_REQUEST_PENDING
Language=English
TNMLIB: Device is busy due to previous requests
.
Language=Russian
TNMLIB: Устройство уже занято выполнением команды
.

MessageId=0x00000009
Severity=Error
Facility=LibraryErrors
SymbolicName=TNMLIB_ERR_CANCEL_PENDING
Language=English
TNMLIB: Cancelling is already pending
.
Language=Russian
TNMLIB: Выполняется отмена операции
.

MessageId=0x0000000A
Severity=Error
Facility=LibraryErrors
SymbolicName=TNMLIB_ERR_VALUE_FROM_CACHE
Language=English
TNMLIB: Operation result was obtained from cache
.
Language=Russian
TNMLIB: Результат операции был получен из кэша
.

;#endif // _TNMMSG_HEADER_
