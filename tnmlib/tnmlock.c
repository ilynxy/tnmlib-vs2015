#include "stdafx.h"

#include "tnmdefs.h"
#include "tnmlib.h"

#include "i_devman.h"

#include "i_devrw.h"

#include "tnmlock.h"

#include "i_devcmd.h"
#include "i_extpep.h"

BOOL TNMLIB_API
TNMLIB_LockDevice(TNMLIB_HANDLE hTND)
{
  _EXT_ENTRY

  pDI->cbw.wOperation = TNMLIB_DEVICE_COMMAND_LOCK;
#ifdef _DEBUG
  pDI->cbw.dwArgs[0] = 0x0000FFFF;
  pDI->cbw.dwArgs[1] = 0xAAAA5555;
  pDI->cbw.dwArgs[2] = 0xFF00FF00;
  pDI->cbw.dwArgs[3] = 0x00FF00FF;
  pDI->cbw.dwArgs[4] = 0x01234567;
  pDI->cbw.dwArgs[5] = 0x89ABCDEF;
#endif
  fSuccess = _DeviceCBWAndDataStage(
    pDI,
    _CBW_DATASTAGE_VALIDATE,
    NULL, 0, 0);

  if (!fSuccess)
    goto _cleanup;

_cleanup:

  _EXT_LEAVE
}

BOOL TNMLIB_API
TNMLIB_UnLockDevice(TNMLIB_HANDLE hTND)
{
  _EXT_ENTRY

  pDI->cbw.wOperation = TNMLIB_DEVICE_COMMAND_UNLOCK;
  fSuccess = _DeviceCBWAndDataStage(
    pDI, 
    _CBW_DATASTAGE_VALIDATE,
    NULL, 0, 0);

  if (!fSuccess)
    goto _cleanup;

  _MarkAllCacheAsInvalid(&pDI->cache);

_cleanup:

  _EXT_LEAVE
}
