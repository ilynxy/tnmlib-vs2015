#include "stdafx.h"

#include <winusb.h>

#include "usbio_i.h"

#include "tnmdefs.h"

#include "i_devman.h"

#include "i_pnpman.h"

CRITICAL_SECTION  g_HM_CS;
static size_t     l_HM_dwHandlesCount;
static size_t     l_HM_dwNextAvailableIdx;
LPVOID            g_HM_pDI[_TNMLIB_MAX_DEVICES];

static BOOL _AcquireDevice(LPTNMLIB_DEVICE_INSTANCE pDI);
static BOOL _ReleaseDevice(LPTNMLIB_DEVICE_INSTANCE pDI);

void
_HMInitialize()
{
  size_t i;

  DBG_ENTRY;

  InitializeCriticalSection(&g_HM_CS);

  l_HM_dwHandlesCount     = 0;
  l_HM_dwNextAvailableIdx = 0;

  for (i = 0; i < _TNMLIB_MAX_DEVICES; i ++)
    g_HM_pDI[i] = NULL;

  DBG_LEAVE(NULL);
}

void
_HMFinalize()
{
  DBG_ENTRY;

  EnterCriticalSection(&g_HM_CS);

  LeaveCriticalSection(&g_HM_CS);

  DeleteCriticalSection(&g_HM_CS);

  DBG_LEAVE(NULL);
}

TNMLIB_HANDLE
_HMCreateHandle(LPTNMLIB_DEVICE_INSTANCE pDI)
{
  TNMLIB_HANDLE h;

  size_t uiIndex;
  size_t i;

  DBG_ENTRY;

  h = TNMLIB_INVALID_HANDLE;

  {
    if (l_HM_dwHandlesCount == _TNMLIB_MAX_DEVICES)
    {
      SetLastError(ERROR_TOO_MANY_OPEN_FILES);
      goto _cleanup;
    }

    uiIndex = l_HM_dwNextAvailableIdx;

    g_HM_pDI[uiIndex] = pDI;

    ++ l_HM_dwHandlesCount;

    for (i = l_HM_dwNextAvailableIdx; i < _TNMLIB_MAX_DEVICES; i ++)
    {
      if (g_HM_pDI[i] == NULL)
      {
        l_HM_dwNextAvailableIdx = i;
        break;
      }
    }

    h = (TNMLIB_HANDLE)((uiIndex + 1) << 2);

  }
_cleanup:
  DBG_LEAVE(h);
  return h;
}

LPTNMLIB_DEVICE_INSTANCE
_HMGetInstanceByHandle(TNMLIB_HANDLE hTND, size_t *pIndex)
{
  size_t uiIndex;
  LPTNMLIB_DEVICE_INSTANCE pDI;

  DBG_ENTRY;

  pDI = NULL;

  {
    uiIndex = (((size_t)hTND) >> 2) - 1;
    
    if (uiIndex >= _TNMLIB_MAX_DEVICES)
    {
      SetLastError(ERROR_INVALID_HANDLE);
      goto _cleanup;
    }

    pDI = g_HM_pDI[uiIndex];
    if (pDI == NULL)
    {
      SetLastError(ERROR_INVALID_HANDLE);
      goto _cleanup;
    }

    if (pIndex != NULL)
      *pIndex = uiIndex;

  }

_cleanup:
  DBG_LEAVE(pDI);
  return pDI;
}

LPTNMLIB_DEVICE_INSTANCE
_HMDeleteHandle(TNMLIB_HANDLE hTND)
{
  LPTNMLIB_DEVICE_INSTANCE pDI;
  size_t uiIndex;

  DBG_ENTRY;

  EnterCriticalSection(&g_HM_CS);

  {
    pDI = _HMGetInstanceByHandle(hTND, &uiIndex);
    if (pDI == NULL)
      goto _cleanup;

    g_HM_pDI[uiIndex] = NULL;

    if (uiIndex < l_HM_dwNextAvailableIdx)
      l_HM_dwNextAvailableIdx = uiIndex;

    -- l_HM_dwHandlesCount;

  }

_cleanup:
  LeaveCriticalSection(&g_HM_CS);

  DBG_LEAVE(pDI);
  return pDI;
}

static LPTNMLIB_DEVICE_INSTANCE
_AllocDeviceInstance()
{
  LPTNMLIB_DEVICE_INSTANCE pDI = NULL;
  DBG_ENTRY;

  pDI = (LPTNMLIB_DEVICE_INSTANCE)malloc(sizeof(TNMLIB_DEVICE_INSTANCE));

  if (pDI != NULL)
  {
    InitializeCriticalSection(&pDI->csBreakLock);
    pDI->bIOIgnoreBreakEvent = FALSE;

    ZeroMemory(&pDI->Overlapped, sizeof(pDI->Overlapped));

    pDI->lpszDevicePath = NULL;

    pDI->hDevice = INVALID_HANDLE_VALUE;
    pDI->wihDevice = NULL;
    pDI->hMutex  = NULL;

    pDI->hInPipe  = INVALID_HANDLE_VALUE;
    pDI->dwInMaximumPacketSize = 0;

    pDI->hOutPipe = INVALID_HANDLE_VALUE;
    pDI->dwOutMaximumPacketSize = 0;

    pDI->hCtlPipe = INVALID_HANDLE_VALUE;
    pDI->dwCtlMaximumPacketSize = 0;

    pDI->hIdleEvent = NULL;
    pDI->hBreakEvent = NULL;
    pDI->dwTimeOut = TNMLIB_DEVICE_DEFAULT_TIMEOUT;
    pDI->dwMaxTransferSize = TNMLIB_DEVICE_MAXIMUM_TRANSFER_SIZE;

    pDI->cbw.dwTag     = TNMLIB_DEVICE_TAG;
    pDI->cbw.wcbLength = sizeof(pDI->cbw);
  
    pDI->lpProgressProc = NULL;

    _InitializeCache(&pDI->cache);

    _ASSERTE(_CrtCheckMemory());
  }

  DBG_LEAVE(pDI);
  return pDI;
}

static void
_FreeDeviceInstance(LPTNMLIB_DEVICE_INSTANCE pDI)
{
  DBG_ENTRY;
  _DeleteCache(&pDI->cache);
  DeleteCriticalSection(&pDI->csBreakLock);
  free(pDI);

  _ASSERTE(_CrtCheckMemory());
  DBG_LEAVE(NULL);
}

static BOOL 
_UnBindAndCloseEndpointHandle(HANDLE hPipe)
{
  BOOL fSuccess = TRUE;
  DBG_ENTRY;

  /*
  It is not necessary to unbind a pipe handle before it is closed. Closing a handle unbinds it
  implicitly. 
  */
  // fSuccess &= USBUD_UnBindPipe(hPipe);

  fSuccess &= CloseHandle(hPipe);

  DBG_LEAVE(fSuccess);
  return fSuccess;
}

static BOOL
_CloseDeviceInstance(LPTNMLIB_DEVICE_INSTANCE pDI)
{
  BOOL fSuccess = TRUE;

  DBG_ENTRY;
  if (pDI->hDevice != INVALID_HANDLE_VALUE)
  {

    if (pDI->Overlapped.hEvent != NULL)
    {
      if (IS_USBIO_BACKEND(pDI))
      {
        if (pDI->hCtlPipe != INVALID_HANDLE_VALUE)
          fSuccess &= _UnBindAndCloseEndpointHandle(pDI->hCtlPipe);

        if (pDI->hOutPipe != INVALID_HANDLE_VALUE)
          fSuccess &= _UnBindAndCloseEndpointHandle(pDI->hOutPipe);

        if (pDI->hInPipe != INVALID_HANDLE_VALUE)
          fSuccess &= _UnBindAndCloseEndpointHandle(pDI->hInPipe);
      }

      fSuccess &= _ReleaseDevice(pDI); 

      fSuccess &= CloseHandle(pDI->Overlapped.hEvent);
    }

    if (pDI->hBreakEvent != NULL)
      fSuccess &= CloseHandle(pDI->hBreakEvent);

    if (pDI->hIdleEvent != NULL)
      fSuccess &= CloseHandle(pDI->hIdleEvent);

    if (pDI->wihDevice != NULL)
      fSuccess &= WinUsb_Free(pDI->wihDevice);

    fSuccess &= CloseHandle(pDI->hDevice);

    if (pDI->hMutex != NULL)
      CloseHandle(pDI->hMutex);
  }

  if (pDI->lpszDevicePath != NULL)
    free(pDI->lpszDevicePath);

  DBG_LEAVE(fSuccess);
  return fSuccess;
}

static HANDLE
_CreateDeviceFileHandle(LPCTSTR lpszDevicePath)
{
  HANDLE hFile;
  DBG_ENTRY;

  hFile = CreateFile(
    lpszDevicePath,
    GENERIC_READ | GENERIC_WRITE,
    FILE_SHARE_WRITE | FILE_SHARE_READ, 
    NULL,
    OPEN_EXISTING,
    FILE_FLAG_OVERLAPPED,
    NULL
    );

  DBG_LEAVE(hFile);
  return hFile;
}

static BOOL
_IoctlDevice(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  HANDLE  hDeviceHandle,
  DWORD   dwIoControlCode, 
  LPCVOID lpInBuffer, DWORD dwInBufferSize, 
  LPVOID  lpOutBuffer, DWORD dwOutBufferSize, 
  LPDWORD lpdwBytesReturned
  )
{
  BOOL fSuccess;
  DWORD dwBytesReturned = 0;

  DBG_ENTRY;

  if (lpdwBytesReturned != NULL)
    *lpdwBytesReturned = 0;

  {
    fSuccess = DeviceIoControl(
      hDeviceHandle, 
      dwIoControlCode, 
      (LPVOID)lpInBuffer, dwInBufferSize, 
      lpOutBuffer, dwOutBufferSize, 
      &dwBytesReturned, 
      &pDI->Overlapped
      );

    if (!fSuccess)
    {
      DWORD dwWaitStatus;
      // HANDLE hEvents[2] = { pDI->Overlapped.hEvent, pDI->hBreakEvent };
      HANDLE hEvents[2];
      hEvents[0] = pDI->Overlapped.hEvent;
      hEvents[1] = pDI->hBreakEvent;

      if (GetLastError() != ERROR_IO_PENDING)
        goto _cleanup;

      dwWaitStatus = WaitForMultipleObjects(
        pDI->bIOIgnoreBreakEvent ? 1 : 2, 
        hEvents, 
        FALSE, 
        pDI->dwTimeOut);

      if (dwWaitStatus != (WAIT_OBJECT_0 + 0))
        CancelIo(hDeviceHandle);

      fSuccess = GetOverlappedResult(
        hDeviceHandle,
        &pDI->Overlapped, 
        &dwBytesReturned,
        TRUE);

      if (dwWaitStatus == WAIT_TIMEOUT)
        SetLastError(WAIT_TIMEOUT);
    }

  }

_cleanup:
  if (lpdwBytesReturned != NULL)
    *lpdwBytesReturned = dwBytesReturned;

  DBG_LEAVE(fSuccess);
  return fSuccess;
}

static BOOL 
_AcquireDevice2(LPTNMLIB_DEVICE_INSTANCE pDI)
{
  BOOL	  fSuccess = FALSE;
  DWORD	  dwStatus;
  HANDLE	hMutex = NULL;

  TCHAR  _mutex_path[MAX_PATH] = _T("");

  {
    if (pDI->hMutex != NULL) 
    {
      SetLastError(ERROR_ALREADY_ASSIGNED);
      goto _cleanup;
    }

    _tcscpy_s(_mutex_path, _countof(_mutex_path), pDI->lpszDevicePath);
    _tcscat_s(_mutex_path, _countof(_mutex_path), _T("#mutex#"));

    // Replace all '\' to '/' due to 'mutex naming convention'
    {
      TCHAR *p = _mutex_path;
      while (*p != _T('\0'))
      {
        if (*p == _T('\\'))
          *p = _T('/');

        ++ p;
      }
    }

    hMutex = CreateMutex(NULL, TRUE, _mutex_path);
    if (hMutex == NULL) {
      goto _cleanup;
    }

    dwStatus = GetLastError();
    if (dwStatus != ERROR_SUCCESS) {
      CloseHandle(hMutex);

      if (dwStatus == ERROR_ALREADY_EXISTS)
        SetLastError(USBIO_ERR_DEVICE_ACQUIRED);

      goto _cleanup;
    }

    pDI->hMutex = hMutex;
    fSuccess = TRUE;
  }
_cleanup:
  return fSuccess;
}

static BOOL 
_ReleaseDevice2(LPTNMLIB_DEVICE_INSTANCE pDI)
{
  BOOL	fSuccess = FALSE;
  // HANDLE	hMutex = NULL;

  {
    if (pDI->hMutex == NULL) {
      SetLastError(ERROR_INVALID_HANDLE);
      goto _cleanup;
    }

    fSuccess = CloseHandle(pDI->hMutex);
    pDI->hMutex = NULL;

  }
_cleanup:
  return fSuccess;
}


static BOOL 
_AcquireDevice(LPTNMLIB_DEVICE_INSTANCE pDI)
{
  BOOL fSuccess = FALSE;
  DBG_ENTRY;

  if (IS_USBIO_BACKEND(pDI))
  {
    fSuccess = _IoctlDevice(
      pDI,
      pDI->hDevice,
      IOCTL_USBIO_ACQUIRE_DEVICE,
      NULL, 0,
      NULL, 0,
      NULL);
  }

  if (!fSuccess)
    fSuccess = _AcquireDevice2(pDI);

  DBG_LEAVE(fSuccess);
  return fSuccess;
}

static BOOL 
_ReleaseDevice(LPTNMLIB_DEVICE_INSTANCE pDI)
{
  BOOL fSuccess = FALSE;
  DBG_ENTRY;

  if (IS_USBIO_BACKEND(pDI))
  {
    fSuccess = _IoctlDevice(
      pDI,
      pDI->hDevice,
      IOCTL_USBIO_RELEASE_DEVICE,
      NULL, 0,
      NULL, 0,
      NULL);
  }

  if (!fSuccess)
    fSuccess = _ReleaseDevice2(pDI);

  DBG_LEAVE(fSuccess);
  return fSuccess;
}


static BOOL
_SetDeviceParameters(LPTNMLIB_DEVICE_INSTANCE pDI, DWORD dwOptions, DWORD dwRequestTimeout)
{
  BOOL fSuccess = FALSE;
  USBIO_DEVICE_PARAMETERS DevParam;

  DBG_ENTRY;

  if (IS_USBIO_BACKEND(pDI))
  {
    ZeroMemory(&DevParam, sizeof(DevParam));

    DevParam.Options = dwOptions;
    DevParam.RequestTimeout = dwRequestTimeout;

    fSuccess = _IoctlDevice(
      pDI,
      pDI->hDevice,
      IOCTL_USBIO_SET_DEVICE_PARAMETERS,
      &DevParam, sizeof(DevParam),
      NULL, 0,
      NULL);
  }
  
  if (IS_WINUSB_BACKEND(pDI))
  {
    fSuccess = TRUE;
  }

  DBG_LEAVE(fSuccess);
  return fSuccess;
}

static BOOL
_ConfigureDevice(LPTNMLIB_DEVICE_INSTANCE pDI)
{
  BOOL fSuccess = FALSE;
  DBG_ENTRY;

  if (IS_USBIO_BACKEND(pDI))
  {
    USBIO_SET_CONFIGURATION DevConfig;

    ZeroMemory(&DevConfig, sizeof(DevConfig));

    DevConfig.ConfigurationIndex = TNMLIB_DEVICE_CONFIGURATION_INDEX;
    DevConfig.NbOfInterfaces = 1;
    DevConfig.InterfaceList[0].InterfaceIndex = 0;
    DevConfig.InterfaceList[0].AlternateSettingIndex = 0;
    DevConfig.InterfaceList[0].MaximumTransferSize = TNMLIB_DEVICE_MAXIMUM_TRANSFER_SIZE;

    fSuccess = _IoctlDevice(
      pDI,
      pDI->hDevice,
      IOCTL_USBIO_SET_CONFIGURATION,
      &DevConfig, sizeof(DevConfig),
      NULL, 0,
      NULL);
  }

  if (IS_WINUSB_BACKEND(pDI))
  {
    fSuccess = TRUE;
  }

  DBG_LEAVE(fSuccess);
  return fSuccess;
}

static BOOL
_GetEndpointsMaxTransferSize(
  LPTNMLIB_DEVICE_INSTANCE pDI
)
{
  BOOL fSuccess = FALSE;

  USBIO_CONFIGURATION_INFO  ci;

  DBG_ENTRY;

  if (IS_USBIO_BACKEND(pDI))
  {  
    ULONG i;

    fSuccess = _IoctlDevice(
      pDI,
      pDI->hDevice,
      IOCTL_USBIO_GET_CONFIGURATION_INFO,
      NULL, 0,
      &ci, sizeof(ci),
      NULL);
    if (!fSuccess)
      goto _cleanup;

    for (i = 0; i < ci.NbOfPipes; i ++)
    {
      USBIO_PIPE_CONFIGURATION_INFO* pPCI;
      pPCI = &ci.PipeInfo[i];

      if (pPCI->EndpointAddress == TNMLIB_DEVICE_CMDOUT_ENDPOINT)
        pDI->dwOutMaximumPacketSize = pPCI->MaximumPacketSize;

      if (pPCI->EndpointAddress == TNMLIB_DEVICE_CMDIN_ENDPOINT)
        pDI->dwInMaximumPacketSize = pPCI->MaximumPacketSize;

      if (pPCI->EndpointAddress == TNMLIB_DEVICE_CTLIN_ENDPOINT)
        pDI->dwCtlMaximumPacketSize = pPCI->MaximumPacketSize;
    }
  }
  
  if (IS_WINUSB_BACKEND(pDI))
  {
    /*
    ULONG ulValue;
    ULONG ulValueLength = sizeof(ulValue);

    fSuccess = WinUsb_GetPipePolicy(pDI->wihDevice, 
      TNMLIB_DEVICE_CMDOUT_ENDPOINT, MAXIMUM_TRANSFER_SIZE, 
      &ulValueLength, &ulValue);
    if (!fSuccess)
      goto _cleanup;
    pDI->dwOutMaximumPacketSize = ulValue;

    fSuccess = WinUsb_GetPipePolicy(pDI->wihDevice,
      TNMLIB_DEVICE_CMDIN_ENDPOINT, MAXIMUM_TRANSFER_SIZE,
      &ulValueLength, &ulValue);
    if (!fSuccess)
      goto _cleanup;
    pDI->dwInMaximumPacketSize = ulValue;
    */

    WINUSB_PIPE_INFORMATION pi;
    
    fSuccess = WinUsb_QueryPipe(pDI->wihDevice, 0, 0, &pi);
    if (!fSuccess)
      goto _cleanup;
    pDI->dwOutMaximumPacketSize = pi.MaximumPacketSize;

    fSuccess = WinUsb_QueryPipe(pDI->wihDevice, 0, 1, &pi);
    if (!fSuccess)
      goto _cleanup;
    pDI->dwInMaximumPacketSize = pi.MaximumPacketSize;
  }

_cleanup:
  DBG_LEAVE(fSuccess);
  return fSuccess;
}

static HANDLE
_CreateAndBindPipe(
  LPTNMLIB_DEVICE_INSTANCE pDI, 
  UCHAR ucEndpointAddress, 
  ULONG ulFlags)
{
  HANDLE hPipe = INVALID_HANDLE_VALUE;

  BOOL fSuccess = FALSE;

  DBG_ENTRY;

  if (IS_USBIO_BACKEND(pDI))
  {
    USBIO_BIND_PIPE       BindPipe;
    USBIO_PIPE_PARAMETERS PipeParameters;

    BindPipe.EndpointAddress = ucEndpointAddress;
    PipeParameters.Flags = ulFlags;

    {
      hPipe = _CreateDeviceFileHandle(pDI->lpszDevicePath);
      if (hPipe == INVALID_HANDLE_VALUE)
        goto _cleanup;

      fSuccess = _IoctlDevice(
        pDI,
        hPipe,
        IOCTL_USBIO_BIND_PIPE,
        &BindPipe, sizeof(BindPipe),
        NULL, 0,
        NULL);
      if (!fSuccess)
        goto _cleanup;

      fSuccess = _IoctlDevice(
        pDI,
        hPipe,
        IOCTL_USBIO_SET_PIPE_PARAMETERS,
        &PipeParameters, sizeof(PipeParameters),
        NULL, 0,
        NULL);
      if (!fSuccess)
        goto _cleanup;
    }

_cleanup:

    if (!fSuccess && hPipe != INVALID_HANDLE_VALUE)
    {
      DWORD dwLastError = GetLastError();
      CloseHandle(hPipe);
      hPipe = INVALID_HANDLE_VALUE;

      SetLastError(dwLastError);
    }
  }

  if (IS_WINUSB_BACKEND(pDI))
  {
    BOOL  bValue = TRUE;
    const ULONG ulValueLength = sizeof(bValue);
    fSuccess = WinUsb_SetPipePolicy(pDI->wihDevice,
      ucEndpointAddress,
      RAW_IO,
      ulValueLength, &bValue);

    if (fSuccess)
    {
      hPipe = (HANDLE)ucEndpointAddress;
    }
  }

  DBG_LEAVE(hPipe);
  return hPipe;
}

BOOL
_DeviceVendorReq(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  USHORT wIndex)
{
  BOOL fSuccess = FALSE;

  DBG_ENTRY;

  if (IS_USBIO_BACKEND(pDI))
  {
    USBIO_CLASS_OR_VENDOR_REQUEST req;
    ZeroMemory(&req, sizeof(req));
    
    req.Type = RequestTypeVendor;
    req.Recipient = RecipientDevice;
    req.Index = wIndex;

    fSuccess = _IoctlDevice(
      pDI,
      pDI->hDevice,
      IOCTL_USBIO_CLASS_OR_VENDOR_OUT_REQUEST,
      &req, sizeof(req),
      NULL, 0,
      NULL);
  }

  if (IS_WINUSB_BACKEND(pDI))
  {
    WINUSB_SETUP_PACKET sp;
   
    const uint8_t host_to_device = 0x00;
    const uint8_t device_to_host = 0x80;

    const uint8_t type_standard = 0x00;
    const uint8_t type_class = 0x20;
    const uint8_t type_vendor = 0x40;
    const uint8_t type_reserved = 0x60;

    const uint8_t recipient_device = 0x00;
    const uint8_t recipient_interface = 0x01;
    const uint8_t recipient_endpoint = 0x02;
    const uint8_t recipient_other = 0x03;


    sp.RequestType  = host_to_device | type_vendor | recipient_device;
    sp.Request      = 0;
    sp.Value        = 0;
    sp.Index        = wIndex;
    sp.Length       = 0;

    ULONG LengthTransferred = 0;
    fSuccess = WinUsb_ControlTransfer(pDI->wihDevice, 
      sp, 
      NULL, 0, &LengthTransferred, 
      &pDI->Overlapped);

    if (!fSuccess)
    {
      if (GetLastError() == ERROR_IO_PENDING)
      {
        DWORD dwBytesReturned;
        DWORD dwWaitStatus;
        // HANDLE hEvents[2] = { pDI->Overlapped.hEvent, pDI->hBreakEvent };
        HANDLE hEvents[2];
        hEvents[0] = pDI->Overlapped.hEvent;
        hEvents[1] = pDI->hBreakEvent;

        dwWaitStatus = WaitForMultipleObjects(
          pDI->bIOIgnoreBreakEvent ? 1 : 2,
          hEvents,
          FALSE,
          pDI->dwTimeOut);

        // TODO: ??? How to cancel Control transfer ???
        if (dwWaitStatus != (WAIT_OBJECT_0 + 0))
        {
          WinUsb_AbortPipe(pDI->wihDevice, 0x00);
          //CancelIoEx(pDI->hDevice, &pDI->Overlapped);
          //CancelIo(pDI->hDevice);
        }

        fSuccess = WinUsb_GetOverlappedResult(
          pDI->wihDevice,
          &pDI->Overlapped,
          &dwBytesReturned,
          TRUE);

        if (dwWaitStatus == WAIT_TIMEOUT)
          SetLastError(WAIT_TIMEOUT);
      }
    }
  }

  DBG_LEAVE(fSuccess);
  return fSuccess;
}

BOOL
_WaitDeviceForSync(LPTNMLIB_DEVICE_INSTANCE pDI)
{
  BOOL fSuccess;

  DBG_ENTRY;
  /*
    Workaround:
    Device has some time for initialization after configuration request.
    So send VENDOR REQ, device respond after configuration
    process has finished with STALL PID (or SUCCESS, if it is be implemented).
    But now, there is no reason to implement this request, because dispatcher
    for VENDOR REQ and CONFIGURATION REQ in device placed in the same event loop.
  */

  fSuccess = _DeviceVendorReq(pDI, 1);

  if (!fSuccess)
  {
    DWORD dwLastError = GetLastError();
    if (dwLastError == USBIO_ERR_STALL_PID)
      fSuccess = TRUE;
  }
  
  fSuccess = TRUE;

  DBG_LEAVE(fSuccess);
  return fSuccess;
}

BOOL
_AsyncResetDevice(LPTNMLIB_DEVICE_INSTANCE pDI)
{
  BOOL fSuccess;
  DBG_ENTRY;

  {
    fSuccess = _DeviceVendorReq(pDI, 0);
    if (!fSuccess)
      goto _cleanup;

    fSuccess = _WaitDeviceForSync(pDI);
    if (!fSuccess)
      goto _cleanup;

  }

_cleanup:
  DBG_LEAVE(fSuccess);
  return fSuccess;
}

static BOOL
_SetDevicePath(
  LPTNMLIB_DEVICE_INSTANCE pDI, 
  LPCTSTR lpszDevicePath)
{
  DWORD dwDevicePathLength;

  BOOL fSuccess;

  DBG_ENTRY;

  fSuccess = FALSE;

  {
    dwDevicePathLength = (DWORD)_tcslen(lpszDevicePath) + 1;
    pDI->lpszDevicePath = malloc(dwDevicePathLength * sizeof(TCHAR));
    if (pDI->lpszDevicePath == NULL)
      goto _cleanup;

#ifdef TNMLIB_USE_SECURE_CRT
    _tcscpy_s(pDI->lpszDevicePath, dwDevicePathLength, lpszDevicePath);
#else
    _tcsncpy(pDI->lpszDevicePath, lpszDevicePath, dwDevicePathLength);
#endif

    fSuccess = TRUE;

  }

_cleanup:
  DBG_LEAVE(fSuccess);
  return fSuccess;
}

LPTNMLIB_DEVICE_INSTANCE
_CreateDeviceInstanceByPath(LPCTSTR lpszDevicePath, TNMLIB_BACKEND backend)
{
  BOOL fSuccess = FALSE;
  LPTNMLIB_DEVICE_INSTANCE pDI;

  DBG_ENTRY;

  {
    pDI = _AllocDeviceInstance();
    if (pDI == NULL)
      goto _cleanup;

    pDI->Overlapped.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
    if (pDI->Overlapped.hEvent == NULL)
      goto _cleanup;

    pDI->hBreakEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (pDI->hBreakEvent == NULL)
      goto _cleanup;

    pDI->hIdleEvent = CreateEvent(NULL, TRUE, TRUE, NULL);
    if (pDI->hIdleEvent == NULL)
      goto _cleanup;

    pDI->hDevice = _CreateDeviceFileHandle(lpszDevicePath);
    if (pDI->hDevice == INVALID_HANDLE_VALUE)
      goto _cleanup;

    if (backend == TNMLIB_WINUSB_BACKEND)
    {
      fSuccess = WinUsb_Initialize(pDI->hDevice, &pDI->wihDevice);
      if (!fSuccess)
        goto _cleanup;
    }

    fSuccess = _SetDevicePath(pDI, lpszDevicePath);
    if (!fSuccess)
      goto _cleanup;

    fSuccess = _AcquireDevice(pDI);
    if (!fSuccess)
      goto _cleanup;

    fSuccess = _SetDeviceParameters(
      pDI, 
      USBIO_UNCONFIGURE_ON_CLOSE, 
      TNMLIB_DEVICE_IOCTL_TIMEOUT);
    if (!fSuccess)
      goto _cleanup;
    
    fSuccess = _ConfigureDevice(pDI);
    if (!fSuccess)
      goto _cleanup;

    fSuccess = _WaitDeviceForSync(pDI);
    if (!fSuccess)
        goto _cleanup;

    fSuccess = _GetEndpointsMaxTransferSize(pDI);
    if (!fSuccess)
      goto _cleanup;

    fSuccess = FALSE;

    pDI->hOutPipe = _CreateAndBindPipe(
      pDI, 
      TNMLIB_DEVICE_CMDOUT_ENDPOINT, 
      USBIO_SHORT_TRANSFER_OK
      );
    if (pDI->hOutPipe == INVALID_HANDLE_VALUE)
      goto _cleanup;

    pDI->hInPipe = _CreateAndBindPipe(
      pDI, 
      TNMLIB_DEVICE_CMDIN_ENDPOINT, 
      USBIO_SHORT_TRANSFER_OK
      );
    if (pDI->hInPipe == INVALID_HANDLE_VALUE)
      goto _cleanup;

/*
    pDI->hCtlPipe = _CreateAndBindPipe(
      pDI, 
      TNMLIB_DEVICE_CTLIN_ENDPOINT, 
      USBIO_SHORT_TRANSFER_OK
      );
    if (pDI->hCtlPipe == INVALID_HANDLE_VALUE)
      goto _cleanup;
*/

    // _PnP_RegisterHandleNotification(pDI->hDevice);

    fSuccess = TRUE;
  }

_cleanup:
  if (!fSuccess)
  {
    if (pDI != NULL)
    {
      DWORD dwLastError = GetLastError();
      _CloseDeviceInstance(pDI);
      _FreeDeviceInstance(pDI);
      pDI = NULL;
      SetLastError(dwLastError);
    }
  }

  _ASSERTE(_CrtCheckMemory());

  DBG_LEAVE(pDI);
  return pDI;
}

BOOL
_DeleteDeviceInstance(LPTNMLIB_DEVICE_INSTANCE pDI)
{
  BOOL fSuccess;

  DBG_ENTRY;

  fSuccess = _CloseDeviceInstance(pDI);
  _FreeDeviceInstance(pDI);

  DBG_LEAVE(fSuccess);
  return fSuccess;
}
