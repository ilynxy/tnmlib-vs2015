#include "stdafx.h"

#include "tnmdefs.h"
#include "tnmlib.h"
#include "tnmmsg.h"

#include "i_devman.h"
#include "i_devrw.h"

#include "i_tnminfo.h"
#include "i_recrw.h"

#include "i_devcmd.h"

void
_GetCurrentDateTime(LPTNMLIB_DEVICE_TIME pTime, LPTNMLIB_DEVICE_DATE pDate)
{
  SYSTEMTIME  st;
  GetLocalTime(&st);
  if (pTime != NULL)
  {
    pTime->bHour        = (BYTE)(st.wHour);
    pTime->bMinute      = (BYTE)(st.wMinute);
    pTime->bSecond      = (BYTE)(st.wSecond);
    pTime->bMillisecond = (BYTE)(st.wMilliseconds / 10);
  }

  if (pDate != NULL)
  {
    pDate->wYear  = (st.wYear);
    pDate->bMonth = (BYTE)(st.wMonth);
    pDate->bDay   = (BYTE)(st.wDay);
  }
}

/*
BOOL
_TND_OpenDataRecord(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  LPTNMLIB_DEVICE_RECORD_PARAM  pDRP
  )
{
  BOOL fSuccess = FALSE;
  DBG_ENTRY;
  do {

    TNMLIB_DEVICE_RECORD_PARAM  rp;
    if (pDRP == NULL)
    {
      _GetCurrentDateTime(&rp.rsTime, &rp.rsDate);
      rp.wTaskNumber = (0xFFFE); // TODO: Set real value
      rp.wFlyNumber  = 0;

      rp.wDataUnitSerial  = 0; // TODO: Set real value
      rp.wDataUnitVersion = 0; // TODO: Set real value

      rp.dwUReserved = 0;

      pDRP = &rp;
    }

    pDI->cbw.wOperation = TNMLIB_DEVICE_COMMAND_OPEN_PC_RECORD;
    pDI->cbw.dwArgs[0] = pDRP->rsTime.dwUTime;
    pDI->cbw.dwArgs[1] = pDRP->rsDate.dwUDate;
    pDI->cbw.dwArgs[2] = pDRP->dwUTaskFly;
    pDI->cbw.dwArgs[3] = pDRP->dwUSerialVersion;
    pDI->cbw.dwArgs[4] = pDRP->dwUReserved;

    fSuccess = _DeviceCBWAndDataStage(pDI, _CBW_DATASTAGE_VALIDATE, NULL, 0, 0);
    if (!fSuccess)
      break;

    _MarkAllCacheAsInvalid(&pDI->cache);

  } while(FALSE);
  DBG_LEAVE(fSuccess);
  return fSuccess;
}

BOOL
_TND_WriteDataRecord(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  LPTNMLIB_DEVICE_TIME  pTime,
  DWORD   dwBytesToWrite,
  LPVOID  pBuffer
  )
{
  BOOL fSuccess = FALSE;
  DBG_ENTRY;
  do {
    TNMLIB_DEVICE_TIME tm;
    if (pTime == NULL)
    {
      _GetCurrentDateTime(&tm, NULL);
      pTime = &tm;
    }

    pDI->cbw.wOperation = TNMLIB_DEVICE_COMMAND_WRITE_PC_DATA;
    pDI->cbw.dwArgs[0] = dwBytesToWrite;
    pDI->cbw.dwArgs[1] = pTime->dwUTime;

    fSuccess = _DeviceCBWAndDataStage(pDI, _CBW_DATASTAGE_WRITE, 
      pBuffer, dwBytesToWrite, 1);
    if (!fSuccess)
      break;

  } while(FALSE);
  DBG_LEAVE(fSuccess);
  return fSuccess;
}

BOOL
_TND_CloseDataRecord(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  LPTNMLIB_DEVICE_TIME  pTime
  )
{
  BOOL fSuccess = FALSE;
  DBG_ENTRY;
  do {
    TNMLIB_DEVICE_TIME tm;

    if (pTime == NULL)
    {
      _GetCurrentDateTime(&tm, NULL);
      pTime = &tm;
    }

    pDI->cbw.wOperation = TNMLIB_DEVICE_COMMAND_WRITE_PC_DATA;
    pDI->cbw.dwArgs[0] = pTime->dwUTime;

    fSuccess = _DeviceCBWAndDataStage(pDI, _CBW_DATASTAGE_VALIDATE, NULL, 0, 0);
    if (!fSuccess)
      break;

  } while(FALSE);
  DBG_LEAVE(fSuccess);
  return fSuccess;
}
*/
