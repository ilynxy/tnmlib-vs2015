#pragma once

BOOL
_TND_RawReadRecord(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  DWORD   dwIndex,
  DWORD   dwOffsetPages,
  DWORD   dwCountPages,
  LPVOID  pBuffer
  );

BOOL
_TND_RawRWMemory(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  BOOL  bIsWrite,
  DWORD  dwMemoryType,
  DWORD  dwOffsetPages,
  DWORD  dwCountPages,
  LPVOID  pBuffer
  );

BOOL
_TND_ReadRecord(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  DWORD   dwIndex,
  DWORD64 ldwOffsetBytes,
  DWORD64 ldwBytesToRead,
  LPVOID  pBuffer
  );

BOOL
_TND_ReadMemory(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  DWORD   dwMemoryType,
  DWORD64 ldwOffsetBytes,
  DWORD64 ldwBytesToRead,
  LPVOID  pBuffer
  );

BOOL
_TND_WriteMemory(
  LPTNMLIB_DEVICE_INSTANCE pDI,
  DWORD   dwMemoryType,
  DWORD64 ldwOffsetBytes,
  DWORD64 ldwBytesToWrite,
  LPCVOID  pBuffer
  );

BOOL
_TND_DeleteAllRecords(LPTNMLIB_DEVICE_INSTANCE pDI);

BOOL
_TND_VerifyBlock(LPTNMLIB_DEVICE_INSTANCE pDI, DWORD dwBlock);
