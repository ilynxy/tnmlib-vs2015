#include "stdafx.h"

#include <stdio.h>
#include <stdarg.h>
#include <varargs.h>

#include "i_debug.h"

#ifdef _DEBUG_CALLS

#define _DBG_STATIC_BUFFER_SIZE 1024

static CRITICAL_SECTION dbgCS;

void
_DbgInitialize()
{
  InitializeCriticalSection(&dbgCS);
}

void
_DbgFinalize()
{
  DeleteCriticalSection(&dbgCS);
}

void
_DbgPrint(LPCTSTR lpszFormat, ...)
{
  va_list args;

  static TCHAR  pszStaticBuffer[_DBG_STATIC_BUFFER_SIZE];
  LPTSTR  pszDynBuffer = NULL;

  LPTSTR  pszOutBuffer = NULL;
  size_t  ilen;

  va_start(args, lpszFormat);
  ilen = _vsctprintf(lpszFormat, args);

  if (ilen < _DBG_STATIC_BUFFER_SIZE)
  {
    pszOutBuffer = pszStaticBuffer;
  }
  else
  {
    pszDynBuffer = (LPTSTR)malloc((ilen + 1) * sizeof(TCHAR));
    pszOutBuffer = pszDynBuffer;
  }

  if (pszOutBuffer != NULL)
  {
    _vstprintf_p(pszOutBuffer, ilen, lpszFormat, args);
    OutputDebugString(pszOutBuffer);
  }
  else
    OutputDebugString(_T("FATAL ERROR: No memory!"));

  if (pszDynBuffer != NULL)
    free(pszDynBuffer);

}

static int g_uLevel = 0;

void
_DbgFunctionEntry(LPCSTR pszFunction)
{
  DWORD dwPID;
  DWORD dwTID;
  DWORD dwLastError;

  EnterCriticalSection(&dbgCS);

  dwLastError = GetLastError();

  dwPID = GetCurrentProcessId();
  dwTID = GetCurrentThreadId();

  _DbgPrint(_T("PID: %4u, TID: %4u,%*s%S<\n\r"),
    dwPID, dwTID,
    g_uLevel*2, _T(""),
    pszFunction);

  ++ g_uLevel;

  SetLastError(dwLastError);

  LeaveCriticalSection(&dbgCS);
}

void
_DbgFunctionLeave(LPCSTR pszFunction, void* lpParam)
{
  DWORD dwPID;
  DWORD dwTID;
  DWORD dwLastError;

  EnterCriticalSection(&dbgCS);

  dwLastError = GetLastError();

  dwPID = GetCurrentProcessId();
  dwTID = GetCurrentThreadId();

  -- g_uLevel;

  _DbgPrint(_T("PID: %4u, TID: %4u,%*s>%S (0x%p)\n\r"),
    dwPID, dwTID,
    g_uLevel*2, _T(""),
    pszFunction,
    lpParam);

  SetLastError(dwLastError);

  LeaveCriticalSection(&dbgCS);
}

// TODO:
void
_DbgFunctionOutputString(LPCTSTR pszString)
{
  DWORD dwPID;
  DWORD dwTID;
  DWORD dwLastError;

  EnterCriticalSection(&dbgCS);

  dwLastError = GetLastError();

  dwPID = GetCurrentProcessId();
  dwTID = GetCurrentThreadId();

  -- g_uLevel;

  _DbgPrint(_T("PID: %4u, TID: %4u,%*s>%s\n\r"),
    dwPID, dwTID,
    g_uLevel*2, _T(""),
    pszString);

  SetLastError(dwLastError);

  LeaveCriticalSection(&dbgCS);
}

#endif // _DEBUG_CALLS
